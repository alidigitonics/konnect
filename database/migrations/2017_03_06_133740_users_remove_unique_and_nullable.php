<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersRemoveUniqueAndNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::statement('ALTER TABLE `users` MODIFY `username` VARCHAR(150) NULL;');
       DB::statement('ALTER TABLE `users` DROP INDEX `users_username_unique` ;');
       DB::statement('ALTER TABLE `users` DROP INDEX `users_email_unique` ;');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `users` MODIFY `username` VARCHAR(150) NOTNULL;');
        DB::statement('ALTER TABLE `users` ADD INDEX `users_username_unique` UNIQUE (username) ;');
        DB::statement('ALTER TABLE `users` ADD INDEX `users_email_unique` UNIQUE (email) ;');
    }
}
