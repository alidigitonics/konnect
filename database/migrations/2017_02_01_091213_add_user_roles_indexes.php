<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserRolesIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_roles', function (Blueprint $table) {
            
            $table->foreign('role_id', 'user_roles_role_id_foreign')->references('id')->on('roles')->onDelete('CASCADE
')->onUpdate('CASCADE');
            $table->foreign('user_id', 'user_roles_user_id_foreign')->references('id')->on('users')->onDelete('CASCADE
')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_roles', function (Blueprint $table) {
            $table->dropForeign('user_roles_role_id_foreign');
            $table->dropForeign('user_roles_user_id_foreign');
        });
    }
}
