<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserSocialAccountsIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_social_accounts', function (Blueprint $table) {
            
            $table->foreign('user_id', 'user_social_accounts_user_id_foreign')->references('id')->on('users')->onDelete('CASCADE
')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_social_accounts', function (Blueprint $table) {
            $table->dropForeign('user_social_accounts_user_id_foreign');
        });
    }
}
