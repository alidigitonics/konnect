<?php

use Illuminate\Database\Seeder;

use Showpro\Data\Models\Role;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::statement("SET foreign_key_checks=0");
		Role::truncate();
		DB::statement("SET foreign_key_checks=1");

    	$role = new Role;
    	$role->title = 'Admin';
    	$role->save();

    	$role = new Role;
    	$role->title = 'User';
    	$role->save();
    }
}
