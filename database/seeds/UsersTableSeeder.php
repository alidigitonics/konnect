<?php

use Illuminate\Database\Seeder;
use Showpro\Data\Models\User;
use Showpro\Data\Models\Role;
use Showpro\Data\Models\UserRole;

use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement("SET foreign_key_checks=0");
        User::truncate();
        UserRole::truncate();
        
        $user_id = DB::table('users')->insertGetId([ //,
            'username' => 'admin',
            'email' => 'admin@tree.com',
            'password' => Hash::make('tree1234'),
            'fullname' => 'Tree Administrator',
            'describe'  => 'Administrator',
            'signup_via'=>"email",
            'profile_pic'=> '',
            'access_token'=> '',
            'login_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
            'create_password'=>1
            
        ]);

        DB::table('user_roles')->insert([ 
        'user_id' => $user_id,
        'role_id' => Role::ADMINISTRATOR,
        'created_at'=>Carbon::now(),
        'updated_at'=>Carbon::now(),
        ]);

        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            
            $image = $faker->image(config('app.files.users.full_path'), $width = 640, $height = 480);
            $image = pathinfo($image);
            $image_path = $image['filename'].".".$image['extension'];
            
            $user_id = DB::table('users')->insertGetId([ //,
                'username' => $faker->unique()->username,
                'email' => $faker->unique()->email,
                'password' => Hash::make('tree12345'),
                'fullname' => $faker->name,
                'describe'  =>$faker->sentence,
                'signup_via'=>"email",
                'profile_pic'=>$image_path,
                'access_token'=> '',
                'login_at'=>Carbon::now(),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
                'create_password'=>1
                
            ]);
            
            DB::table('user_roles')->insert([ 
                'user_id' => $user_id,
                'role_id' => Role::USER,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
        DB::statement("SET foreign_key_checks=1");

    }
}
