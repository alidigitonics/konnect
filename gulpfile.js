var gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    concat      = require('gulp-concat'),
    ngAnnotate  = require('gulp-ng-annotate'),
    uglify      = require('gulp-uglify'),
    rename      = require('gulp-rename'),
    moment      = require('moment'),
    notify      = require('gulp-notify'),
    sourcemaps      = require('gulp-sourcemaps');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


	var resourcesDir = 'resources/assets/js/';
	var bowerDir = 'bower_components/';
	var nodeDir = 'node_modules/';

gulp.task('combine-vendor', function(){
	var vendorScripts = [
						bowerDir+'jquery/dist/jquery.min.js',
						bowerDir+'angularjs/angular.min.js',
						bowerDir+'angular-ui-router/release/angular-ui-router.min.js',
						bowerDir+'satellizer/dist/satellizer.min.js',
						bowerDir+'angular-local-storage/dist/angular-local-storage.min.js',
						bowerDir+'ng-tags-input/ng-tags-input.min.js',
			            bowerDir+'angular-loading-bar/build/loading-bar.min.js',
			            bowerDir+'ng-file-upload-shim/ng-file-upload-shim.min.js',
			            bowerDir+'ng-file-upload/ng-file-upload.min.js',
					];

	gulp.src(vendorScripts)
		.pipe(concat('vendor.js'))
		.on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(rename({
            extname: ".min.js"
         }))
		.pipe(gulp.dest('public/js'))
		.pipe(notify('Combined JavaScript (' + moment().format('MMM Do h:mm:ss A') + ')'));
});

gulp.task('custom-scripts', function(){

	var customScripts = [
						resourcesDir+'tree/custom/scripts.js',
					];

	gulp.src(customScripts)
		.pipe(sourcemaps.init())
		.pipe(concat('custom-scripts.js'))
        .pipe(rename({
            extname: ".min.js"
         }))
		.pipe(sourcemaps.write('./'))
		.on('error', notify.onError("Error: <%= error.message %>"))
		.pipe(gulp.dest('public/js'))
		.pipe(notify('Custom JavaScript (' + moment().format('MMM Do h:mm:ss A') + ')'));
});

gulp.task('uglify-js',  function() {
	var appScripts = [
			resourcesDir+'tree/tree.js',
			resourcesDir+'tree/factories/*.js',
			resourcesDir+'tree/services/*.js',
			resourcesDir+'tree/directives/*.js',
			resourcesDir+'tree/controllers/*.js'
		];
	gulp.src(appScripts)
		.pipe(concat('app.js'))
		.pipe(ngAnnotate())
		.pipe(sourcemaps.init())
		.on('error', notify.onError("Error: <%= error.message %>"))
		.pipe(uglify( {
						outSourceMap: true
				}
		))
		.pipe(rename({
			extname: ".min.js"
		 }))
		.pipe(sourcemaps.write('./'))
		.on('error', notify.onError("Error: <%= error.message %>"))
		.pipe(gulp.dest('public/js'))
		.pipe(notify('Uglified JavaScript (' + moment().format('MMM Do h:mm:ss A') + ')'));
});
gulp.task('watch',function() {
	var appScripts = [
			resourcesDir+'tree/tree.js',
			resourcesDir+'tree/factories/*.js',
			resourcesDir+'tree/services/*.js',
			resourcesDir+'tree/directives/*.js',
			resourcesDir+'tree/controllers/*.js'
		];
	watch(appScripts, function() {
		gulp.start('uglify-js');
	});

	var customScripts = [
						resourcesDir+'tree/custom/scripts.js',
					];
	watch(customScripts, function() {
		gulp.start('custom-scripts');
	});
});
gulp.task('default', ['watch','uglify-js','custom-scripts','combine-vendor']);
