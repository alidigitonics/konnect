<!DOCTYPE html>
<html ng-app="Showpro">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Showpro</title>
<link rel="shortcut icon" type="image/png" href="{{url('images/favicon.png')}}"/>


<link rel="stylesheet" href="{{url('css/plug.css')}}" media="screen">
<link rel="stylesheet" href="{{url('css/style.css')}}" media="screen">
<link rel="stylesheet" href="{{url('css/theme.css')}}" media="screen">
<script type="text/javascript">
<?php

$appSettings = Showpro\Support\Helper::appSettings();
// dd($appSettings);
?>
var configConstants = <?php echo json_encode($appSettings);?>;
if(window.devicePixelRatio > 1) { document.cookie='HTTP_IS_RETINA=1;path=/'; }
var tokenName = '_front';
</script>
<base href="<?php echo $appSettings['base_href'];?>">
</head>

<body>
 @yield('content')
 <script src="{{url('js/vendor.min.js')}}" type="text/javascript"></script>
 <script src="{{url('js/custom-scripts.min.js')}}" type="text/javascript"></script>
 <script src="{{url('js/app.min.js')}}" type="text/javascript"></script>
</body>
</html>

