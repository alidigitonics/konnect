angular.module('TREE').directive('datetimez',function() {
		return {
				restrict: 'A',
				require : 'ngModel',
				scope: {
					pickerOptions: '=?',
					pickerController: '=?',
					pickerOnChange: '=?'
				},
				link: function(scope, element, attrs, ngModelCtrl) {
					var api = $(element)
							.datepicker(scope.pickerOptions || {});
						api.on('changeDate', function (e){
									ngModelCtrl.$setViewValue(e.date);
									scope.$apply(function (){
										if (scope.pickerOnChange != '') {
											scope.pickerOnChange(e);
										}
									});
							});
					scope.pickerController = api;
				}
		};
});