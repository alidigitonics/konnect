var TREE = angular.module('TREE', ['ui.router', 'satellizer', 'LocalStorageModule', 'ngTagsInput','angular-loading-bar','ngFileUpload']);

TREE.constant('settings', configConstants);



TREE.config(function($stateProvider, $urlRouterProvider, $locationProvider, settings, $authProvider,
	$httpProvider, localStorageServiceProvider, cfpLoadingBarProvider,$qProvider) {

	$authProvider.SatellizerConfig.tokenName += tokenName;

	$qProvider.errorOnUnhandledRejections(false);

	// console.log(settings);
	$authProvider.facebook({
      clientId: settings.facebook_app_id,
      responseType: 'token'
    });

	var skipIfAuthenticated = function ($q, $state, $auth, $timeout) {
					var defer = $q.defer();
					if($auth.isAuthenticated()) {
						$timeout(function () {
							if($authProvider.SatellizerConfig.tokenName == 'token_admin'){
								$state.go('dashboard');
								
							}else if($authProvider.SatellizerConfig.tokenName == 'token_front'){
								$state.go('upload');
							}
						});
						defer.reject(); 
					} else {
						defer.resolve();
					}
					return defer.promise;
				};

	var redirectIfNotAuthenticated = function ($q, $state, $auth, $timeout) {
		var defer = $q.defer();
		if($auth.isAuthenticated()) {
			defer.resolve();
		} else {
			$timeout(function () {
				if ($authProvider.SatellizerConfig.tokenName == 'token_admin') {
					$state.go('main');
				}else if($authProvider.SatellizerConfig.tokenName == 'token_front'){
					$state.go('front-main');

				}
			});
			defer.reject();
		}
		return defer.promise;
	};

	//localStorageServiceProvider.setPrefix(settings.auth.token.prefix);
	cfpLoadingBarProvider.includeSpinner = false;

	$urlRouterProvider
		.otherwise('/admin');

	$stateProvider
		.state('root', {
			abstract: true,
			resolve: {
				redirectIfNotAuthenticated: function ($q, $state, $auth, $timeout) {
					var defer = $q.defer();
					if($auth.isAuthenticated()) {
						defer.resolve();
					} else {
						$timeout(function () {
							$state.go('main');
						});
						defer.reject();
					}
					return defer.promise;
				}
			},
			views: {
				'@': {
					templateUrl: settings.basePath + '/js/views/admin/layouts/main.html',
				},
				'topbar@root': {
					templateUrl: settings.basePath + '/js/views/admin/layouts/top-bar.html',
					controller: 'TopBarController as topBarCtrl'
				}
			},
		})
		.state('main', {
			url: "/admin",
			resolve: {
				skipIfAuthenticated: skipIfAuthenticated
			},
			views : {
				'@': {
					templateUrl: settings.basePath + '/js/views/admin/layouts/main.html',
				},
				'main@main':{
					templateUrl: settings.basePath + "/js/views/admin/login.html",
					controller: 'AuthController as authCtrl'

				}
			}
		})
		.state('front', {
			abstract: true,
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views: {
				'@': {
					templateUrl: settings.basePath + '/js/views/layouts/main.html',
				},
				'topbar@front': {
					templateUrl: settings.basePath + '/js/views/layouts/top-bar.html',
					controller: 'FrontTopBarController as FrontTopBarCtrl'
				}
			},
		})
		.state('front-main', {
			url: "/",
			resolve: {
				skipIfAuthenticated: skipIfAuthenticated
			},
			views : {
				'@': {
					templateUrl: settings.basePath + '/js/views/layouts/main.html',
				},
				'main@front-main':{
					templateUrl: settings.basePath + "/js/views/login.html",
					controller: 'AuthController as authCtrl'

				}
			}
		})
		.state('upload', {
			parent: 'front',
			url: "/upload",
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views : {
				'main@front':{
					templateUrl: settings.basePath + "/js/views/upload.html",
					controller: 'PostController as PostCtrl',
				}
			}
		})
		.state('user-reset-password', {
			url: "/admin/user-reset-password?recover_password_key",
			resolve: {
				skipIfAuthenticated: function ($q, $state, $auth, $timeout) {
					var defer = $q.defer();
					if($auth.isAuthenticated()) {
						$timeout(function () {
							$state.go('dashboard');
						});
						defer.reject(); 
					} else {
						defer.resolve();
					}
					return defer.promise;
				}
			},
					templateUrl: settings.basePath + "/js/views/admin/create-password.html",
					controller: 'AuthController as authCtrl'
			
		})
		.state('dashboard', {
			parent: 'root',
			url: "/admin/dashboard?start_date&end_date",
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views : {
				'main@root':{
					templateUrl: settings.basePath + "/js/views/admin/dashboard.html",
					controller: 'DashboardController as dashboardCtrl',
					params: {
						start_date: {
				            value: '1',
				            squash: true
				        },
				        end_date: {
				            value: '1',
				            squash: true
				        },
					}
				}
			}
		})
		.state('users', {
			parent: 'root',
			url: "/admin/users?page&filter_by_status&filter_by_type&filter_by_name",
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views : {
				'main@root':{
					templateUrl: settings.basePath + "/js/views/admin/users.html",
					controller: 'UserController as UserCtrl',
					params: {
				        page: {
				            value: '1',
				            squash: true
				        },
				        filter_by_status: {
				            value: '',
				            squash: true
				        },
				        filter_by_type: {
				            value: '',
				            squash: true
				        },
				        filter_by_name: {
				            value: '',
				            squash: true
				        }
				    }
				}
			},
		})
		.state('content', {
			parent: 'root',
			url: "/admin/content?page&filter_by_status&filter_by_type&filter_by_title&filter_by_user_id&filter_by_genre",
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views : {
				'main@root':{
					templateUrl: settings.basePath + "/js/views/admin/content.html",
					controller: 'ContentController as ContentCtrl',
					params: {
				        page: {
				            value: '1',
				            squash: true
				        },
				        filter_by_status: {
				            value: '',
				            squash: true
				        },
				        filter_by_type: {
				            value: '',
				            squash: true
				        },
				        filter_by_title: {
				            value: '',
				            squash: true
				        },
				        filter_by_user_id: {
				            value: '',
				            squash: true
				        },
				        filter_by_genre: {
				            value: '',
				            squash: true
				        }
				    }
				}
			},
		})
		.state('genre', {
			parent: 'root',
			url: "/admin/genre?page",
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views : {
				'main@root':{
					templateUrl: settings.basePath + "/js/views/admin/genre.html",
					controller: 'GenreController as GenreCtrl',
					params: {
				        page: {
				            value: '1',
				            squash: true
				        }
				    }
				}
			},
		})

		.state('manage-admin', {
			parent: 'root',
			url: "/admin/manage-admin?page",
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated
			},
			views : {
				'main@root':{
					templateUrl: settings.basePath + "/js/views/admin/manage-admin.html",
					controller: 'AdminController as AdminCtrl',
					params: {
				        page: {
				            value: '1',
				            squash: true
				        }
				    }
				}
			},
		})
		
		.state('oops', {
			url: '/admin/oops',
			controller: 'OopsController as oopsCtrl',
			templateUrl: settings.basePath + "/js/views/oops.html",
		})
		.state('access-denied', {
			url: '/admin/access-denied',
			controller: 'OopsController as oopsCtrl',
			templateUrl: settings.basePath + "/js/views/errors/access-denied.html",
		});
		
	$locationProvider.html5Mode(true);

	$httpProvider.interceptors.push(['$q', '$location', '$rootScope','localStorageService', function($q, $location, $rootScope, localStorageService) {
		return {
			'request': function(config) {
				// request your $rootscope messaging should be here?
				return config;
			},

			'requestError': function(rejection) {
				// request error your $rootscope messagin should be here?
				return $q.reject(rejection);
			},


			'response': function(response) {
				// response your $rootscope messagin should be here?
				return response;
			},

			'responseError': function(rejection) {
				//response error your $rootscope messagin should be here?
				
				if (rejection.status == 401) {
					$rootScope.$broadcast("userState", 'loggedout');	
				}
				return $q.reject(rejection);

			}
		};
	}]);
}).run(['$rootScope', '$state', '$auth', 'MetaInfoService', '$timeout', 'AuthService', 'settings',
	function($rootScope, $state, $auth, MetaInfoService, $timeout, AuthService, settings) {

		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

		});
		$rootScope.$on('userState', function(event, data) {
			if (data == 'loggedout') {
				$timeout(function() {
					AuthService.logout();
				}, 500);
			}
		});
	}
])

angular.module('TREE').directive('paginate',[function(){
	
    return {
        replace: true,
        scope: {
          model: "=",
          nextPage: '=',
          previousPage: '='
        },
        controller: function($scope) {
            $scope.viewNextPage = function () {
            	console.log('here click', $scope.model.next);
                $scope.nextPage($scope.model.next);
            };
            $scope.viewPreviousPage = function () {

                $scope.previousPage($scope.model.previous);
            };
        },
		template:'<div class="col-xs-12 paging-wrapper" ng-show="model.total > 0">\
					<div class="col-md-8 col-sm-6 col-xs-12 padding-0px margin-bottom-10px-xs">\
						<div class="dark-grey paging-info">Showing {{model.to}} to {{model.from}} of {{model.total}} entries</div>\
					</div>\
					<div class="col-md-4 col-sm-6 col-xs-12 text-right padding-0px">\
						<a class="btn btn-success next-prev-btn rt-pad" href=""  ng-class="{disabled: model.previous <= 0 }" ng-click="viewPreviousPage()"><div class="loader"></div> <span>Previous</span></a>\
						<a class="btn btn-success next-prev-btn " ng-class="{disabled: model.next == model.current }" href="" ng-click="viewNextPage()" g-show="model.next > 0"><div class="loader"></div> <span>Next </span></a>\
					</div>\
				</div>',
    }
}]);