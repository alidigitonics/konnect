angular.module('TREE').service('SettingService',['$http','settings',function($http, settings){
    var mcfsettings = this;
    mcfsettings.endPoint = settings.apiUrl+'/setting';

    mcfsettings.list = function(pagination, page, limit, successCallback, errorCallback){
        var params = {
            pagination: (angular.isUndefined(pagination))? 1: pagination,
            page: page || 1,
            limit: limit || 100,
            keyword:'',
        }

        $http.get(mcfsettings.endPoint+'/list',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    mcfsettings.update = function (info, successCallback, errorCallback) {

        $http.put(mcfsettings.endPoint+'/update', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    mcfsettings.resetModal = function (which) {

    }

    return mcfsettings;
}]);
