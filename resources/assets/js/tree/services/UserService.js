angular.module('TREE').service('UserService',['$http','settings',function($http, settings){
    var user = this;
    user.endPoint = settings.apiUrl+'/user';

    user.list = function(pagination, page, limit, keyword,filters,successCallback, errorCallback){
        
        var params = {
            pagination: (angular.isUndefined(pagination))? 1: pagination,
            page: page || 1,
            limit: limit || 100,
            keyword:keyword||'',
            filter_by_name:filters.filter_by_name||'',
            filter_by_type:filters.filter_by_type||'',
            filter_by_status:filters.filter_by_status||'',

        }

        $http.get(user.endPoint+'/list',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    user.setVisibility = function(data,successCallback, errorCallback){
        $http.post(user.endPoint+'/visibility',data).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    // user.view = function(id, successCallback, errorCallback){
    //     var params = {
    //         id: id
    //     }
    //     $http.get(user.endPoint+'/view', {params:params}).then(function(response){
    //         successCallback(response);
    //     },function(error){
    //         errorCallback(error);
    //     });
    // }

    // user.viewByUserId = function(successCallback, errorCallback){

    //     $http.get(user.endPoint+'/get/view').then(function(response){
    //         successCallback(response);
    //     },function(error){
    //         errorCallback(error);
    //     });
    // }

    // user.create = function (info, successCallback, errorCallback) {

    //     $http.post(user.endPoint+'/create', info).then(function(response){
    //         successCallback(response);
    //     },function(error){
    //         errorCallback(error);
    //     });
    // }

    // user.update = function (info, successCallback, errorCallback) {

    //     $http.put(user.endPoint+'/update', info).then(function(response){
    //         successCallback(response);
    //     },function(error){
    //         errorCallback(error);
    //     });
    // }

    // user.updatePassword = function (info, successCallback, errorCallback) {
    //     $http.put(user.endPoint+'/update/password', info).then(function(response){
    //         successCallback(response);
    //     },function(error){
    //         errorCallback(error);
    //     });
    // }

    user.resetModal = function (which) {

    }

    return user;
}]);
