angular.module('TREE').service('GenreService',['$rootScope','$http','settings','$auth','localStorageService',function($rootScope, $http, settings, $auth, localStorageService){
	var genre = this;
	genre.endPoint = settings.apiUrl+'/genre';

	genre.list = function(pagination, page, limit, successCallback, errorCallback){
        var params = {
            pagination: (angular.isUndefined(pagination))? 1: pagination,
            page: page || 1,
            limit: limit || 100,
            // keyword:keyword||'',
            device:'web'
        }

        $http.get(genre.endPoint+'/list',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    genre.delete = function(id, successCallback, errorCallbac){
        var params = {
            id: id
        }
        $http.delete(genre.endPoint+'/delete', {params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    genre.create = function (info, successCallback, errorCallback) {

        $http.post(genre.endPoint+'/create', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    genre.update = function (info, successCallback, errorCallback) {

        $http.put(genre.endPoint+'/update', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    genre.setOrder = function (info, successCallback, errorCallback) {

        $http.put(genre.endPoint+'/order/set', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    
    genre.setVisibility = function (info, successCallback, errorCallback) {

        $http.put(genre.endPoint+'/set/visibility', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    

    return genre;
}]);
