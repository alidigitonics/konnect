angular.module('TREE').service("HelperService", ["localStorageService", "settings", function(localStorageService, settings) {

    var vm = this;
    vm.setEndDate = function(endDate, format) {
        var format = format || 'DD/MM/YYYY';
        if (endDate) {
            endDate = moment(endDate, format);
            localStorageService.set('reportEndDate', endDate);
        }
    }

    vm.getEndDate = function(format) {
        var format = format || 'DD/MM/YYYY';
        var endDate = localStorageService.get('reportEndDate');
        if (endDate) {
            endDate = moment(endDate).format(format);
            return endDate;
        } else {
            return moment().format(format);
        }
    }

    vm.setStartDate = function(startDate, format) {
        var format = format || 'DD/MM/YYYY';
        if (startDate) {
            startDate = moment(startDate,format);
            localStorageService.set('reportStartDate', startDate);
        }
    }

    vm.getStartDate = function(format) {
        var format = format || 'DD/MM/YYYY';
        var startDate = localStorageService.get('reportStartDate');
        if (startDate) {
            startDate = moment(startDate).format(format);
            return startDate;
        } else {
            return moment(settings.program_start_date).format(format);
        }
    }

    // vm.setEndDate = function(endDate, format) {
    //     var format = format || 'DD/MM/YYYY';
    //     if (endDate) {
    //         endDate = moment(endDate,format).format('DD/MM/YYYY');
    //         localStorageService.set('reportEndDate', endDate);
    //     }
    // }

    // vm.getEndDate = function(format) {
    //     var format = format || 'DD/MM/YYYY';
    //     var endDate = localStorageService.get('reportEndDate');
    //     if (endDate) {
    //         endDate = moment(endDate, format).format(format);
    //         return endDate;
    //     } else {
    //         return moment().format(format);
    //     }
    // }

    
}]);
