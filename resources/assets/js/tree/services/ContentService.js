angular.module('TREE').service('ContentService',['$http','settings',function($http, settings){
    var content = this;

    content.endPoint = settings.apiUrl+'/';
    
    content.list = function(pagination, page, limit, keyword,filters,successCallback, errorCallback){
        
        var params = {
            pagination: (angular.isUndefined(pagination))? 1: pagination,
            page: page || 1,
            limit: limit || 100,
            keyword:keyword||'',
            filter_by_title:filters.filter_by_title||'',
            filter_by_type:filters.filter_by_type||'',
            filter_by_status:filters.filter_by_status||'',
            filter_by_user_id:filters.filter_by_user_id||'',
            filter_by_genre_id:filters.filter_by_genre||'',

        }
        
        $http.get(content.endPoint+'post/list',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    

    content.genreList = function(pagination, page, limit, successCallback, errorCallback){
        var params = {
            pagination: (angular.isUndefined(pagination))? 1: pagination,
            page: page || 1,
            limit: limit || 100,
            device:'web'
        }

        $http.get(content.endPoint+'genre/list',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    
    content.delete = function(id, successCallback, errorCallbac){
        var params = {
            id: id
        }
        $http.delete(content.endPoint+'post/delete', {params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    return content;
}]);
