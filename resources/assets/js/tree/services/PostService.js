angular.module('TREE').service('PostService',['$http','settings','localStorageService','Upload',function($http, settings,localStorageService,Upload){
    var post = this;

    post.endPoint = settings.apiUrl+'/';
    
    post.getUserData = function(successCallback,errorCallback) {

        var id   = localStorageService.get('id');
        $http.post(post.endPoint+'user/profile/view',{id:id}).then(function(response){
        
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
   
    return post;
}]);
