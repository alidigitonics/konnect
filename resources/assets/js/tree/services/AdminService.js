angular.module('TREE').service('AdminService',['$rootScope','$http','settings','$auth','localStorageService',function($rootScope, $http, settings, $auth, localStorageService){
	var admin = this;
	admin.endPoint = settings.apiUrl+'/admin';

	admin.scopeName = function(scope) {
		return scope;
	}
	
    admin.isEmail=function(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	admin.isNumberOnly = function (value) {
    	//var er = /^[0-9]*$/;
    	var er = /^\d*\.?\d*$/;
    	return er.test(value);
	};

	admin.minLimit = function(min,value){
    	if(value.length < min) {
      		return false;
     	} else {
      		return true;
     	}
    };

    admin.list = function(pagination, page, limit, successCallback, errorCallback){
        var params = {
            pagination: (angular.isUndefined(pagination))? 1: pagination,
            page: page || 1,
            limit: limit || 100,
            keyword:'',
        }

        $http.get(admin.endPoint+'/list',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }
    admin.delete = function(id, successCallback, errorCallbac){
        var params = {
            id: id
        }
        $http.delete(admin.endPoint+'/delete', {params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    admin.create = function (info, successCallback, errorCallback) {

        $http.post(admin.endPoint+'/create', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    admin.update = function (info, successCallback, errorCallback) {

        $http.put(admin.endPoint+'/update', info).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    admin.resetModal = function (which) {
        
    }

	return admin;
}]);
