angular.module('TREE').service('DashboardService',['$http','settings',function($http, settings){
    var dashboard = this;
    dashboard.endPoint = settings.apiUrl+'/';

    dashboard.dashboard = function(filters,successCallback, errorCallback){
        var params = {
            start_date : filters.start_date,
            end_date :  filters.end_date,
        }
        $http.get(dashboard.endPoint+'dashborad',{params:params}).then(function(response){
            successCallback(response);
        },function(error){
            errorCallback(error);
        });
    }

    return dashboard;
}]);
