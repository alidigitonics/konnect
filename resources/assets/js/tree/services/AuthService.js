angular.module('TREE').service('AuthService',['$rootScope','$http','settings','$auth','localStorageService','UserService','$state',function($rootScope, $http, settings, $auth, localStorageService, UserService, $state){
	var auth = this;
	auth.endPoint = settings.apiUrl+'/user';

	auth.logout = function(){
		// console.log(settings.auth.endpoint.unlink);
		$http.post(settings.apiUrl+'/logout').then(function(response){
			if ($auth.logout()) {
				localStorageService.remove('user_id');
				localStorageService.remove('role_id');
				$state.go('main');
			}
		},function(error){
			if ($auth.logout()) {
				localStorageService.remove('user_id');
				localStorageService.remove('role_id');
				$state.go('main');
			}
		});
	};
	
	auth.frontLogout = function(){
		// console.log(settings.auth.endpoint.unlink);
		$http.post(settings.apiUrl+'/logout').then(function(response){
			if ($auth.logout()) {
				localStorageService.remove('id');
				$state.go('front-main');
			}
		},function(error){
			if ($auth.logout()) {

				$state.go('front-main');
			}
		});
	};
	auth.login = function(user,successCallback,errorCallback){

		$http.post(settings.apiUrl+'/admin/login', user).then(function(response){
          	
			$auth.setToken(response.data.response.data.access_token);
          	successCallback(response);
			
		},function(error) {

        	errorCallback(error);
        });
	};
	auth.forgot = function(info,successCallback,errorCallback){

		$http.put(settings.apiUrl+'/forgot-password', info).then(function(response){
          	
          	successCallback(response);
			
		},function(error) {

        	errorCallback(error);
        });
	};
	auth.reset = function(info,successCallback,errorCallback){

		$http.put(settings.apiUrl+'/reset-password', info).then(function(response){
          	
          	successCallback(response);
			
		},function(error) {

        	errorCallback(error);
        });
	};

	auth.frontLogin = function(frontUser,successCallback,errorCallback){

		$http.post(settings.apiUrl+'/login',
		frontUser).then(function(response){
          	
			$auth.setToken(response.data.response.data.access_token);
			localStorageService.set('id',response.data.response.data.id);
          	successCallback(response);
			
		},function(error) {

        	errorCallback(error);
        });
	};

	auth.scopeName = function(scope) {
		return scope;
	}
	auth.hasScope = function (scope){
		var hasScope = false;
		var payload = $auth.getPayload();
		if (typeof payload.scopes != 'undefined') {
			for (var i = 0; i < payload.scopes.length; i++) {
				if(payload.scopes[i] == auth.scopeName(scope)) {
					hasScope = true;
					break;
				}
			}
		}

		return hasScope;
	}

	auth.getLoggedInUser = function () {
		var user = localStorageService.get('loggedInUser');
		return user;
    }

    auth.fetchLoggedInUser = function (){
		UserService.viewByUserId(function(response){
            if (response.data) {
            	localStorageService.set('loggedInUser', response.data);
            	var data = localStorageService.get('loggedInUser');
            	$rootScope.$broadcast('loggedInUserSuccess', data);
            	return data;
            }
        }, function(error){

        });
    }

    auth.isEmail=function(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	auth.isNumberOnly = function (value) {
    	//var er = /^[0-9]*$/;
    	var er = /^\d*\.?\d*$/;
    	return er.test(value);
	};

	auth.minLimit = function(min,value){
    	if(value.length < min) {
      		return false;
     	} else {
      		return true;
     	}
    };

	return auth;
}]);
