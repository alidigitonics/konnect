angular.module('TREE').factory("MetaInfoService", ["$rootScope", "settings", function($rootScope, settings) {
    var config = {
            title: settings.title || "TREE",
            meta: {
                description: settings.meta.description || "Description"
            }
        };
    function initialize() {
        setTitle(config.title), setMetaDescription(config.meta.description), setBodyClass('login-bg')
    }

    function setTitle(title) {
        $rootScope.title = title || config.title, $rootScope.$broadcast("titleChanged", $rootScope.title)
    }

    function setMetaDescription(description) {
        $rootScope.meta.description = description || config.meta.description, $rootScope.$broadcast("descriptionChanged", $rootScope.meta.description)
    }

    function setBodyClass(bodyClass) {
        $rootScope.bodyClass = bodyClass || 'login-bg', $rootScope.$broadcast("bodyClassChanged", $rootScope.bodyClass)
    }

    return $rootScope.meta = {}, $rootScope.bodyClass = 'login-bg', initialize(), {
        title: function(){
            return $rootScope.title;
        },
        description: function(){
            $rootScope.meta.description;
        },
        updateTitle: function(title){
            $rootScope.title = config.title +' | '+ title || config.title;
        },
        updateDescription: function(description){
            $rootScope.meta.description = description || config.meta.description;
        },
        bodyClass: function(){
            return $rootScope.bodyClass;
        },
        updateBodyClass: function(bodyClass){
            $rootScope.bodyClass = bodyClass || '';
        }
    }

}]);
