angular.module('TREE').controller('DashboardController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService', 'DashboardService', function($scope, $auth, $state, $http, settings, $timeout, AuthService, MetaInfoService, HelperService, DashboardService) {

	var vm  = $scope;
	vm.isLoadedFromApply = false;
    vm.globaldates = {};
    vm.globaldates.startDate = '';
    vm.globaldates.endDate = '';
    
    vm.globaldates.options = {
    	todayBtn:true,
    	format: 'dd/mm/yyyy'
    };
	vm.start_date = '';
	vm.end_date = '';

	vm.setDates = function () {
    	$timeout(function(){
    		vm.globaldates.startDate.datepicker('setDate',HelperService.getStartDate());
    		vm.globaldates.endDate.datepicker('setDate',HelperService.getEndDate());
		},500);
    }

    vm.startDateChange = function (e){
		var a = moment(vm.end_date,'DD/MM/YYYY');
		var b = moment(vm.start_date,'DD/MM/YYYY');
		var diff   =  a.diff(b, 'days');
        if( vm.start_date != "" && vm.end_date !=""){
    	   	if(diff < 0){
    			$('#startDate').parent().parent().addClass('has-error');
    			$('#isApply').prop('disabled', true);
    			

    	    } else {
    			$('#isApply').prop('disabled', false);
    			$('#startDate, #endDate').parent().parent().removeClass('has-error');
    	    }
    		var curDate = new Date();
    		var finalFormattedStartDate = moment(vm.start_date,'DD/MM/YYYY').format('DD/MM/YYYY');
    		var finalFormattedEndDate = moment(vm.end_date,'DD/MM/YYYY').format('DD/MM/YYYY');
    		HelperService.setStartDate(finalFormattedStartDate);
        }
	}

	vm.endDateChange = function (e){
		var a = moment(vm.end_date,'DD/MM/YYYY');
		var b = moment(vm.start_date,'DD/MM/YYYY');
		
        var diff   =  a.diff(b, 'days');
        if( vm.start_date != "" && vm.end_date !=""){
    	   	if(diff < 0){
    			$('#endDate').parent().parent().addClass('has-error');
    			$('#isApply').prop('disabled', true);
    			

    	    } else {
    			$('#isApply').prop('disabled', false);
    			$('#startDate, #endDate').parent().parent().removeClass('has-error');
    	    }

    		var finalFormattedStartDate = moment(vm.start_date,'DD/MM/YYYY').format('DD/MM/YYYY');
    		var finalFormattedEndDate = moment(vm.end_date,'DD/MM/YYYY').format('DD/MM/YYYY');
    		HelperService.setEndDate(finalFormattedEndDate);
        }

	}

	vm.dashboard = function(type){

		$('.dashboard-filters').removeClass('active');
		if(type == "one_month"){
    		$('#one_month').addClass('active');
            $('#startDate, #endDate').val('');
    	} else if(type == "today"){
    		$('#today').addClass('active');
            $('#startDate, #endDate').val('');
    	} else if(type == "overall"){
    		$('#overall').addClass('active');
            $('#startDate, #endDate').val('');
    	}
       	var filters = vm.startEndDates(type);
       	vm.isLoading = true;
        DashboardService.dashboard(filters,function(response){

            vm.isLoading = false;
            vm.dasboardData = "";
            if ( response.data != "" && typeof response.data != "undefined") {
            	
            	vm.data = response.data;
                
                vm.user = vm.data.users;

                vm.content = vm.data.content;
                
                vm.content_action = vm.data.content_actions;
                
            } else {
                vm.dashboardData = [];
            }
        }, function(){
           vm.isLoading = false;
        });

    }
    
    vm.startEndDates = function(type, format){

    	var datesObj = {};
    	var curDate = new Date();
    	var format = format || 'YYYY-MM-DD';
    	
    	// overall 
    	datesObj.start_date = "";
    	datesObj.end_date = "";

    	if( type == "today"){
    		datesObj.start_date = moment(curDate).format(format);
    		datesObj.end_date = moment(curDate).format(format);
    	} else if( type == "date_range"){
    		datesObj.start_date = moment(vm.start_date).format(format);
    		datesObj.end_date = moment(vm.end_date).format(format);
    	} else if(type == "one_month"){
    		var lastMonthDate = moment().subtract(30, 'days').format(format);
    		datesObj.start_date = lastMonthDate;
    		datesObj.end_date = moment(curDate).format(format);
    	}

    	return datesObj;

    }

    vm.dashboard();
}]);
