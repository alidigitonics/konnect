angular.module('TREE').controller('AdminController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService', 'AdminService', function($scope, $auth, $state, $http, settings, $timeout, AuthService, MetaInfoService, HelperService, AdminService) {

	var vm  = $scope;
	vm.isRecordAdding = false;
	vm.isRecordUpdating = false;
	vm.isRecordDeleting = false;
    vm.admins = [];
    vm.isLoading = false;
    vm.page = parseInt($state.params.page, 10) | 1;
    vm.pagination = {};

    vm.newAdmin = {};
    vm.newAdmin.fullname = '';
    vm.newAdmin.email = '';

    vm.loggedInUser = localStorage.getItem('user_id');
    vm.loggedInUserRoleId = localStorage.getItem('role_id');

    vm.list = function(page){
        MetaInfoService.updateTitle('Admin Management');
        $('.pagination').hide();
        vm.isLoading = true;
        var limit = 25;
        AdminService.list(true, page, limit, function(response){
            vm.isLoading = false;
            if (response.data.data.length > 0) {
                vm.admins = response.data.data;
                vm.pagination = response.data.pagination;
                $('.pagination').show();
            } else {
                vm.admins = [];
                vm.pagination = {};
            }
        }, function(){
            vm.isLoading = false;
        });
    }

    vm.delete = function(id){
    	vm.isRecordDeleting = true;
        AdminService.delete(id, function(response){
           $timeout(function () {
                	$("#confirmationPopup").modal("hide");
                    vm.isRecordDeleting = false;
                    vm.$emit('refreshState',{state:'manage-admin', params: {page:vm.page}});
                }, 1000);
        }, function(error){
            vm.isRecordDeleting = false;
        });
    }

    vm.showPopup = function(admin,type){
    	vm.admin = {};
	    vm.admin.id = admin.id;
	    vm.admin.fullname = admin.fullname;
	    vm.admin.email = admin.email;
    	// vm.admin = admin;
    	
    	if(type == 'delete'){
    		$("#confirmationPopup").modal("show");
    	}else{
    		$("#updateAdmin").modal("show");
    	}
    	
    }


    vm.showNext = function(page){
        $('.pagination').hide();
        vm.page = page;
        $state.go('.', {page: vm.page});
    }

    vm.showPrev = function(page){
        $('.pagination').hide();
        vm.page = page;
        if (vm.page > 0) {
            $state.go('.', {page: vm.page});
        }
    }

    vm.create = function(){
        vm.errorMessage = '';
        vm.successMessage = '';
        vm.isRecordAdding = true;
        var data = {
            email : vm.newAdmin.email,
            fullname : vm.newAdmin.fullname
        }

        AdminService.create(data, function(response){
        	if (response.data.response.data) {
                vm.successMessage = 'Admin has been added successfully.';
                $timeout(function () {
                	$("#addAdmin").modal("hide");
                    vm.isRecordAdding = false;
                    
                    vm.$emit('refreshState',{state:'manage-admin', params: {page:vm.page}});
                    // $state.go('manage-admin',{'page':vm.page});
                }, 1000);
            } 

        }, function(error){
            vm.isRecordAdding = false;
            vm.errorMessage = error.data.error.messages[0];
            
            
        });
         
    }

    vm.update = function(){
        var recordInfo = {
            id :vm.admin.id,
            fullname :vm.admin.fullname
        }
        vm.errorMessage = '';
        vm.successMessage = '';
        vm.isRecordUpdating = true;
        AdminService.update(recordInfo, function(response){
        	if (response.data) {
                vm.successMessage = 'Admin has been updated successfully.';
                $timeout(function () {
                    vm.isRecordUpdating = false;
                    $("#updateAdmin").modal("hide");
                    vm.$emit('refreshState',{state:'manage-admin', params: {page:vm.page}});
                }, 1000);

            } 
        }, function(error){
            vm.errorMessage = error.data.error.messages[0];
           	vm.isRecordUpdating = false;

        });
        
    }

    vm.resetModal = function (which) {
        if(which == 'create') {
            $('#newAdminEmail').val('');
            $('#newAdminFullName').val('');
            // $('#addAdmin').modal('show');
        } else if (which == 'update') {
            $('#editAdminFullName').parent().removeClass('has-error');
            $('#editDistrictTitleUr').parent().removeClass('has-error');
            $('.message-div-error, .message-div-success').removeClass('show-message');
            $('#modifyDisricts').modal('show');
        }
    }

    vm.list(vm.page);

    vm.$on('refreshState', function(event, data){
        $state.go(data.state, data.params, {reload: true});
        // $state.go(data.state,true);
    });
}]);
