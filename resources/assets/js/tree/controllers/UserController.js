angular.module('TREE').controller('UserController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService','UserService', function($scope, $auth, $state, $http, settings, $timeout, AuthService, MetaInfoService, HelperService,UserService) {
    var vm  = $scope;
    var state = $state;    
    vm.users = [];
    vm.userSearch = {};

    vm.userSearch.filter_by_status  = {};
    vm.userSearch.filter_by_status.id = '';
    vm.userSearch.filter_by_status.title = 'Filter by status';
    
    vm.userSearch.filter_by_type = {};
    vm.userSearch.filter_by_type.id = '';
    vm.userSearch.filter_by_type.title = 'Filter by sign up type';
    
    vm.userSearch.filter_by_name = '';

    vm.types = [{'id':'', 'title':'Filter by sign up type'},{'id':'facebook', 'title':'Facebook'}, {'id':'email', 'title':'Email'}];
    vm.statuses = [{'id':'', 'title':'Filter by status'},{'id':'0', 'title':'Block'}, {'id':'1', 'title':'Active'}];

    
    vm.isLoading = false;
    vm.page = parseInt($state.params.page, 10);
    vm.pagination = {};
 

    vm.class = "";

    vm.is_visible = "";

    vm.list = function(page){
        
        // filter_by_status
        var filters = {};
        filters.filter_by_status = $state.params.filter_by_status;
        filters.filter_by_type = $state.params.filter_by_type;
        filters.filter_by_name = $state.params.filter_by_name;
        var keyword = $state.params.keyword||'';
        
        MetaInfoService.updateTitle('App Users');
        $('.pagination').hide();
        vm.isLoading = true;
        var limit = 25;
        UserService.list(true, page, limit, keyword,filters,function(response){
            
            if( typeof $state.params.filter_by_status != "undefined"){
                vm.userSearch.filter_by_status.id = $state.params.filter_by_status;
            }
            if( typeof $state.params.filter_by_type != "undefined"){
                vm.userSearch.filter_by_type.id = $state.params.filter_by_type;
            }
            if( typeof $state.params.filter_by_name != "undefined"){
                vm.userSearch.filter_by_name = $state.params.filter_by_name;
            }
            
            vm.isLoading = false;
            if (response.data.response.data.length > 0) {
                vm.users = response.data.response.data;
                vm.pagination = response.data.response.pagination;
                $('.pagination').show();
            } else {
                vm.users = [];
                vm.pagination = {};
            }
        }, function(){
            vm.isLoading = false;
        });


    }

    vm.showNext = function(page){

        $('.pagination').hide();
        vm.page = page;
        $state.go('.', {page: vm.page});
    }

    vm.showPrev = function(page){
        $('.pagination').hide();
        vm.page = page;
        if (vm.page > 0) {
            $state.go('.', {page: vm.page});
        }
    }

    vm.formatDate = function(date, format){
        var format = format || 'DD/MM/YYYY';
        return moment(date).format(format);
    }

    vm.searchUser = function(page){
        console.log(vm.userSearch,vm.userSearch.filter_by_status.id);
        var searchParams = {
            page:1,
            filter_by_status : vm.userSearch.filter_by_status.id,
            filter_by_type : vm.userSearch.filter_by_type.id,
            filter_by_name : vm.userSearch.filter_by_name
        }
        state.go("users",searchParams,{reload:true});
    }

    vm.setUserVisibility = function(id, event){
        vm.show_spinner = true;
        vm.prevent_click = true;
        var element = angular.element(event.target);
        if (element.hasClass('red') === true){
            element.removeClass('red');
            element.addClass('green');
            is_visible = 0;
        } else{
            element.removeClass('green');
            element.addClass('red');
            is_visible = 1;
        }
        var data = {
                id : id,
                is_visible : is_visible
        }
        UserService.setVisibility(data,function(response){
            vm.isLoading = false;
            $("#userBlockConfirmation").modal("hide");
            vm.show_spinner     = false;
            vm.prevent_click    = false;
            if (response.data.data) {
                
            }
        }, function(){
            vm.isLoading = false;
        });
    }

    vm.showBlockConfirmation = function (id, event, visibility){
        console.log(event);
         vm.block_user_id   = id;
         vm.block_event     = event;
         vm.block_visibility     = visibility;
        $('#userBlockConfirmation').modal('show');
    }
    vm.list(vm.page);

     //vm.searchUser();
}]);
