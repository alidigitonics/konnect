angular.module('TREE').controller('ContentController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService','ContentService', function($scope, $auth, $state, $http, settings, $timeout, AuthService, MetaInfoService, HelperService,ContentService) {
    
    var vm  = $scope;
    var state = $state;
    vm.contents = [];
    vm.postSearch = {};

    vm.postSearch.filter_by_genre  = {};
    vm.postSearch.filter_by_genre.id = '';
    vm.postSearch.filter_by_genre.title = 'Filter by Branches';
    
    vm.postSearch.filter_by_type = {};
    vm.postSearch.filter_by_type.id = '';
    vm.postSearch.filter_by_type.title = 'Filter Content';
    vm.genres = [{'id':'', 'title':'Filter branches'}];

    vm.userId = $state.params.filter_by_user_id;
    vm.name = $state.params.name;
    // vm.genres = [];

    vm.postSearch.filter_by_title = '';

    vm.types = [{'id':'', 'title':'Filter by type'},{'id':'photo', 'title':'Photo'}, {'id':'video', 'title':'Video'}];
    //vm.statuses = [{'id':'', 'title':'Filter by status'},{'id':'public', 'title':'Public'}, {'id':'private', 'title':'Private'}];
    
    vm.isLoading = false;
    vm.page = parseInt($state.params.page, 10);
    vm.pagination = {};

    

    vm.list = function(page){
        var filters = {};
        filters.filter_by_genre = $state.params.filter_by_genre;
        filters.filter_by_type = $state.params.filter_by_type;
        filters.filter_by_title = $state.params.filter_by_title;
        filters.filter_by_user_id = $state.params.filter_by_user_id;

        var keyword = $state.params.keyword||'';
        
        MetaInfoService.updateTitle('User Content');
        $('.pagination').hide();
        vm.isLoading = true;
        var limit = 20;
        

        ContentService.list(true, page, limit ,keyword, filters, function(response){
            vm.isLoading = false;
            
            if( typeof $state.params.filter_by_genre != "undefined"){
                vm.postSearch.filter_by_genre.id = $state.params.filter_by_genre;
            }
            if( typeof $state.params.filter_by_type != "undefined"){
                vm.postSearch.filter_by_type.id = $state.params.filter_by_type;
            }
            if( typeof $state.params.filter_by_title != "undefined"){
                vm.postSearch.filter_by_title = $state.params.filter_by_title;
            }

            if (response.data.response.data.length > 0) {
                vm.contents = response.data.response.data;
                vm.creator = response.data.response.creator;
                vm.pagination = response.data.response.pagination;
                $('.pagination').show();
            } else {
                vm.contents = [];
                vm.pagination = {};
            }
        }, function(){
            vm.isLoading = false;
        });


    }

    vm.showNext = function(page){

        $('.pagination').hide();
        vm.page = page;
        $state.go('.', {page: vm.page});
    }

    vm.showPrev = function(page){
        $('.pagination').hide();
        vm.page = page;
        if (vm.page > 0) {
            $state.go('.', {page: vm.page});
        }
    }

    vm.searchPost = function(page){
        
        var searchParams = {
            page:1,
            filter_by_genre : vm.postSearch.filter_by_genre.id,
            filter_by_type : vm.postSearch.filter_by_type.id,
            filter_by_title : vm.postSearch.filter_by_title
        }
        
        state.go("content",searchParams,{reload:true});
    }

    vm.genreList = function(page){
        
        ContentService.genreList(false, '', '', function(response){

            if (response.data.response.data.length > 0) {
                // vm.genres = response.data.response.data;
                for (var i = 0; i < response.data.response.data.length; i++) {
                    vm.genres.push(response.data.response.data[i]);
                }
            } else {
                vm.genres = [];
            }
  
        }, function(){
        
        });
    }

    vm.showPopup = function(contentData){
        
        vm.content = {};
        vm.content.id = contentData.id;
        vm.content.title = contentData.title;
        if(contentData.type == 'photo'){
            vm.content.image_preview = contentData.image_urls['2x'];
        } else {
            vm.content.video_preview = contentData.video_url['hd'];
        }
        vm.content.image_profile = contentData.creator.image_urls['1x'];
        vm.content.views = contentData.post_views;
        vm.content.likes = contentData.post_likes;
        vm.content.comments = contentData.post_comment_count;
        vm.content.time = vm.postTime(contentData.created_at);
        vm.content.privacy = contentData.privacy;
        vm.content.type = contentData.type;
        vm.content.file_type = contentData.file_type;
        /*var date=contentData.created_at.split(' ');
        console.log(date)
        vm.content.date0 = date[0];
        vm.content.date1 = date[1];
        
        jQuery.timeago.settings.strings.prefixAgo = '';
        $(".timeago_check").timeago();
*/
        $("#postModal").modal("show");

    }
   
    vm.delete = function(id){
        
        vm.isRecordDeleting = true;
        vm.show_spinner = true;
        vm.prevent_click = true;
        ContentService.delete(id, function(response){
           $timeout(function () {
                    $("#postModal").modal("hide");
                    $("#disableAccount").modal("hide");
                    vm.isRecordDeleting = false;
                    vm.show_spinner     = false;
                    vm.prevent_click    = false;
                    vm.$emit('refreshState',{state:'content', params: {page:vm.page}});
                }, 1000);
        }, function(error){
            vm.isRecordDeleting = false;
        });
    }

    vm.postTime = function(date, format){
        var format = format || 'DD/MM/YYYY';
        return moment(date).fromNow();
    }

    vm.$on('refreshState', function(event, data){
        $state.go(data.state, data.params, {reload: true});
        // $state.go(data.state,true);
    });

    vm.showRenoveConfirmation = function (){
        $('#disableAccount').modal('show');
    }


    vm.list(vm.page);
    vm.genreList();
}]);
