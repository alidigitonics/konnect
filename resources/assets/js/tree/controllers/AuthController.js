angular.module('TREE').controller('AuthController', ['$scope','$auth','$state','$stateParams', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService', function($scope, $auth, $state, $stateParams, $http, settings, $timeout, AuthService, MetaInfoService, HelperService) {

    //var localStorage;

	$scope.user = {};
	$scope.server = {};
	$scope.user.login_via = 'email';
	$scope.login = function() {
		
        $scope.show_spinner = true;
        AuthService.login($scope.user, function(response){
                        

            if (response.data) {
                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem('user_id', response.data.response.data.id);
                    localStorage.setItem('role_id', response.data.response.data.role_id);

                }

                $scope.user 			= response.data;
        		$scope.text_danger 		= false;
                $scope.text_success 	= true;
                $scope.show_spinner     = false;
                $scope.server.success   = response.data.response.message.pop();
                $state.go('dashboard');
            }
            $scope.loginForm.$setPristine();
            $scope.loginForm.$setUntouched();

        }, function(error){
            
            $scope.text_danger  = true;
            $scope.show_spinner = false;
            $scope.server.error = error.data.error.messages.pop();
            $timeout(function () {
            	$scope.server.error = '';
 			}, 3000);
            
        });
		
	};
	
	
	$scope.info = {};
	$scope.forgot = function() {

		$scope.show_spinner = true;
		AuthService.forgot($scope.info, function(response){
            
            if (response.data) {
                $scope.info                 = response.data;
                $scope.forgot_text_danger   = false;
                $scope.forgot_text_success  = true;
                $scope.show_spinner         = false;
                $scope.server.forgot_success        = response.data.response.messages.pop();
            
            }
            $timeout(function () {
            	$scope.server.forgot_success  = '';
                $scope.server.forgot_error = '';
            }, 5000);
			$scope.forgotForm.$setPristine();
			$scope.forgotForm.$setUntouched();

        }, function(error){
      
        	$scope.forgot_text_danger 	= true;
            $scope.show_spinner         = false;
            $scope.server.forgot_error = error.data.error.messages.pop();
            $timeout(function () {
                $scope.server.forgot_error = '';
 			}, 3000);
            
        });
	};


	$scope.info = {};
	$scope.info.recover_password_key = $stateParams.recover_password_key;
	$scope.reset = function() {

        $scope.show_spinner = true;
		AuthService.reset($scope.info, function(response){
			
            if (response.data) {
                $scope.info 				= response.data;
        		$scope.reset_text_danger 	= false;
                $scope.reset_text_success 	= true;
                $scope.show_spinner         = false;
	            $scope.server.reset_success = 'Your password has been reset successfully.'//response.data.response.messages.pop();
                
                if(response.data.response.role_id[0] == 1 || response.data.response.role_id[0] == 3 || response.data.response.role_id[0] == 4 || response.data.response.role_id[0] == 5){

                    $state.go('main');
                
                }else if(response.data.response.role_id[0] == 2){
                    $state.go('front-main');
                }
            }
			$scope.resetForm.$setPristine();
			$scope.resetForm.$setUntouched();

        }, function(error){
      
        	$scope.reset_text_danger 	= true;
            $scope.show_spinner         = false; 
            $scope.server.reset_error = error.data.error.messages.pop();
            $timeout(function () {
            	$scope.server.reset_error = '';
 			}, 3000);
            
        });
	};


    //Fnont login
    $scope.frontUser = {};
    $scope.frontServer = {};

    $scope.frontLogin = function(type) {
        $scope.frontUser.login_via = type;
        if($scope.frontUser.login_via == 'facebook'){
           $auth.authenticate('facebook')
              .then(function(fbResponse) {
                console.log(fbResponse+'then');
                $scope.frontUser.fb_access_token = fbResponse.access_token;
                AuthService.frontLogin($scope.frontUser, function(response){
                    console.log(response+'success');
                    if (response.data) {
                        $scope.frontUser        = response.data;
                        $scope.text_danger      = false;
                        $scope.text_success     = true;
                        $scope.show_spinner     = false;
                        $scope.frontServer.success   = response.data.response.message.pop();
                        $state.go('upload');
                    }
                    $scope.frontLoginForm.$setPristine();
                    $scope.frontLoginForm.$setUntouched();

                }, function(error){
                    console.log(error+'error')
                    $scope.text_danger  = true;
                    $scope.show_spinner = false;
                    $scope.frontServer.error = error.data.error.messages.pop();
                    $timeout(function () {
                        $scope.frontServer.error = '';
                    }, 3000);
                    
                });
              })
              .catch(function(fbResponse) {
                console.log(fbResponse+'catch');
              });
            // $auth.authenticate('facebook');
        }else {
            $scope.frontUser.login_via = 'email';
            $scope.show_spinner = true;
            AuthService.frontLogin($scope.frontUser, function(response){
                if (response.data) {
                    $scope.frontUser        = response.data;
                    $scope.text_danger      = false;
                    $scope.text_success     = true;
                    $scope.show_spinner     = false;
                    $scope.frontServer.success   = response.data.response.message.pop();
                    $state.go('upload');
                }
                $scope.frontLoginForm.$setPristine();
                $scope.frontLoginForm.$setUntouched();

            }, function(error){
                
                $scope.text_danger  = true;
                $scope.show_spinner = false;
                $scope.frontServer.error = error.data.error.messages.pop();
                $timeout(function () {
                    $scope.frontServer.error = '';
                }, 3000);
                
            });
        }
        
        
        
    };

    

}]);
