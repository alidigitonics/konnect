angular.module('TREE').controller('PostController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService','ContentService','PostService','Upload', function($scope, $auth, $state, $http, settings, $timeout, AuthService, MetaInfoService, HelperService,ContentService,PostService,Upload) {
    
    var vm  = $scope;
    vm.genres = [];
    vm.post = {};
   	vm.server = {};
   	vm.user = {};
		var endPoint = settings.apiUrl+'/';
    vm.post_image_screen 	= true; 
	vm.post_detail_screen 	= false; 
	vm.post_genre_screen 	= false;
	vm.public_active 		= true;
	vm.commentable	 		= true;
	vm.post.privacy	 		= 'public';
	vm.post.is_commentable	= 1;


   	
   	vm.setUserData = function () {
    	
    	PostService.getUserData(function(response){

        	if (response.data.response.data) {
            	vm.user = response.data.response.data;
            }
        }, function(error){

        });
    }

	vm.showDetailScreen = function (){
		vm.show_spinner_screen1 = true;
		vm.post_image_screen 	= false;
	  	vm.post_detail_screen 	= true;
		vm.post_genre_screen 	= false;
	  	/*setTimeout(function(){
			$('.upload-progressbar .fill').css({"width":"100%"});
		}, 1000);*/
	}

	vm.showGenreScreen = function (){
		vm.post_image_screen 	= false;
	 	vm.post_detail_screen 	= false; 
	 	vm.post_genre_screen 	= true;
		vm.progress_bar			= true;
		vm.progress_bar_percentage = '0%';

	}

	vm.publicBtn = function (){
	 	vm.private_active 	= true;
		vm.public_active 		= false;
	}
	vm.privateBtn = function (){
		vm.private_active 	= false;
		vm.public_active 		= true;
	}
	vm.commentableYesBtn = function (){
		
		vm.commentable	= false;
	}
	
	vm.commentableNoBtn = function (){
		vm.commentable	 = true;
	}

	vm.genreList = function(page){
        
        ContentService.genreList(false, '', '', function(response){

            if (response.data.response.data.length > 0) {
                for (var i = 0; i < response.data.response.data.length; i++) {
                    vm.genres.push(response.data.response.data[i]);
                }
            } else {
                vm.genres = [];
            }
        }, function(){
        
        });
    }
	
	vm.postCreate = function(content) {
		
		vm.show_spinner = true;
		vm.prevent_click = true;
		vm.genreArray = [];
    	angular.forEach(vm.genres, function(genre){
      		if (genre.selected){
      			vm.genreArray.push(genre.id);	
      		} 
    	});
    	
    	vm.post.genre 	= vm.genreArray;
		var content 	= vm.myFile;
	
		Upload.upload({
            url: endPoint +'post/create',
            data: {content: content, 'title': vm.post.title,'description': vm.post.description,'privacy': vm.post.privacy,'is_commentable': vm.post.is_commentable,'genre': vm.post.genre},
        }).then(function (response) {
            
            // vm.progress_bar			= false;
            vm.show_spinner			= false;
            
            if (response.data.response.code == 200) {

	            $scope.text_danger 		= false;
	       		$scope.text_success 	= true;
	      		$scope.server.success   = 'Your pots has been posted successfully.';
	            $timeout(function () {
	            	vm.prevent_click 		= false;
	            	$scope.server.success = '';
	            	$state.reload();
					}, 4000);
            }
        }, function (error) {

            $scope.text_danger  = true;
            $scope.show_spinner = false;
            $scope.server.error = error.data.error.messages.pop();
            $timeout(function () {
                $scope.server.error = '';
                $state.reload();
            }, 4000);
            
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.progress_bar_percentage = progressPercentage+'%'; 
            $('.upload-progressbar .fill').css({"width":+progressPercentage+'%'}); 
                
           
        });
	};

	vm.checkboxbtn = function() {
		var length = $scope.genres && $scope.genres.filter(function(el) {
			return el.selected;
		}).length;
		return length > 1;
	}

    vm.genreList();
    vm.setUserData();

}]);
