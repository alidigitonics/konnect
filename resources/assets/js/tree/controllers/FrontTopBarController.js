angular.module('TREE').controller('FrontTopBarController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'UserService', function($scope, $auth, $state, $http, settings, $timeout, AuthService, UserService) {
		var topBarSelf  = $scope;
		topBarSelf.logout = function(){
	        AuthService.frontLogout();
	      
	    };
	    topBarSelf.isActive = function(entity){
	    	var currentState = $state.current.name; 
	        if (entity == 'dashboard') {
	            if (currentState == 'dashboard') {
	                return true;
	            }
	        } else if (entity == 'users') {
	            if (currentState == 'users') {
	                return true;
	            }
	        } else if (entity == 'content') {
	            if (currentState == 'content') {
	                return true;
	            }
	        } else if (entity == 'manage-admin') {
	            if (currentState == 'manage-admin') {
	                return true;
	            }
	        } else if (entity == 'genre') {
	            if (currentState == 'genre') {
	                return true;
	            }
	        }
	        return false;
	    };
}])
