angular.module('TREE').controller('GenreController', ['$scope','$auth','$state', '$http','settings', '$timeout', 'AuthService', 'MetaInfoService', 'HelperService','GenreService', function($scope, $auth, $state, $http, settings, $timeout, AuthService, MetaInfoService, HelperService,GenreService) {

var vm  = $scope;
	vm.isRecordAdding = false;
	vm.isRecordUpdating = false;
	vm.isRecordDeleting = false;
    vm.genres = [];
    vm.isLoading = false;
    vm.page = parseInt($state.params.page, 10) || 1;
    vm.pagination = {};
    vm.newGenre = {};
    vm.newGenre.fullname = '';
    vm.newGenre.email = '';
    vm.checkGenre = [];

    vm.list = function(page){
        MetaInfoService.updateTitle('Branch Management');
        $('.pagination').hide();
        vm.isLoading = true;
        var limit = 10;
        GenreService.list(true, page, limit, function(response){
        	vm.isLoading = false;
            if (response.data.response.data.length > 0) {
                vm.genres = response.data.response.data;
                for (var i = 0; i < vm.genres.length; i++) {
                    vm.checkGenre[i] = vm.genres[i].visibility;
                }
                vm.pagination = response.data.response.pagination;
                $('.pagination').show();
            } else {
                vm.genres = [];
                vm.pagination = {};
            }
        }, function(){
            vm.isLoading = false;
        });
    }

    vm.setOrder = function(id,order){
    	var data = {
            id : id,
            order:order
        }
        GenreService.setOrder(data, function(response){
           vm.$emit('refreshState',{state:'genre', params: {page:vm.page}});
        }, function(error){
            vm.isRecordDeleting = false;
        });
    }

    vm.setVisibility = function(id,visibility,index){
        if(visibility == 1){
            visibility = 0;
        }else{
            visibility = 1;
        }
        var params = {
            id : id,
            visibility:visibility
        }
        GenreService.setVisibility(params, function(response){
            vm.checkGenre[index] = visibility;
           // vm.$emit('refreshState',{state:'genre', params: {page:vm.page}});
        }, function(error){
            vm.checkGenre[index] = visibility;
            vm.isRecordDeleting = false;
        });
    }

    vm.checkVisibility = function(data){
        var value = 'false';
        if(data == 1){
            value = 'true';
        }
    }

    vm.showPopup = function(admin,type){
    	vm.genre = {};
	    vm.genre.id = admin.id;
	    vm.genre.fullname = admin.fullname;
	    vm.genre.email = admin.email;
    	// vm.genre = admin;
    	if(type == 'delete'){
    		$("#confirmationPopup").modal("show");
    	}else{
    		$("#updateAdmin").modal("show");
    	}
    }


    vm.showNext = function(page){
        $('.pagination').hide();
        vm.page = page;
        $state.go('.', {page: vm.page});
    }

    vm.showPrev = function(page){
        $('.pagination').hide();
        vm.page = page;
        if (vm.page > 0) {
            $state.go('.', {page: vm.page});
        }
    }

    vm.create = function(){
        vm.errorMessage = '';
        vm.successMessage = '';
        vm.isRecordAdding = true;
        var data = {
            title : vm.newGenre.title,
        }
        GenreService.create(data, function(response){
        	if (response.data.response) {
                vm.successMessage = 'Branch has been added successfully.';
                $timeout(function () {
                	$("#addGenres").modal("hide");
                    vm.isRecordAdding = false;
                    vm.$emit('refreshState',{state:'genre', params: {page:vm.page}});
                }, 1000);
            } 
        }, function(error){
            vm.isRecordAdding = false;
            vm.errorMessage = error.data.error.messages[0];
            console.log(vm.errorMessage);
        });
    }

    vm.resetModal = function (which) {
        if(which == 'create') {
            $('#newGenreTitle').val('');
        } else if (which == 'update') {
            $('#editAdminFullName').parent().removeClass('has-error');
            $('#editDistrictTitleUr').parent().removeClass('has-error');
            $('.message-div-error, .message-div-success').removeClass('show-message');
            $('#modifyDisricts').modal('show');
        }
    }

    vm.list(vm.page);

    vm.$on('refreshState', function(event, data){
        $state.go(data.state, data.params, {reload: true});
        // $state.go(data.state,true);
    });


}]);
