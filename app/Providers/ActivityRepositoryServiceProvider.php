<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\ActivityRepository;
use konnect\Data\Models\Activity;

class ActivityRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('ActivityRepository', function () {
            return new ActivityRepository(new Activity);
        });
    }
}
