<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\PostActionRepository;
use konnect\Data\Models\PostAction;

class PostActionRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('PostActionRepository', function () {
            return new PostActionRepository(new PostAction);
        });
    }
}
