<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\DashboardRepository;
use konnect\User;
use konnect\Data\Models\Post;
use konnect\Data\Models\PostAction;
use konnect\Data\Models\PostShare;

class DashboardRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DashboardRepository', function(){
            return new DashboardRepository(new User, new Post, new PostAction, new PostShare);
        });
    }
}
