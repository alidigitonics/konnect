<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\HashTagRepository;
use konnect\User;
use konnect\Data\Models\HashTag;


class HashTagRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('HashTagRepository', function () {
            return new HashTagRepository(new HashTag );
        });
    }
}
