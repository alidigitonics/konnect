<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\DeviceTokenRepository;
use konnect\Data\Models\DeviceToken;

class DeviceTokenRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('DeviceTokenRepository', function () {
            return new DeviceTokenRepository(new DeviceToken);
        });
    }
}
