<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\PostHashTagRepository;
use konnect\User;
use konnect\Data\Models\PostHashTag;


class PostHashTagRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PostHashTagRepository', function () {
            return new PostHashTagRepository(new PostHashTag );
        });
    }
}
