<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\PostRepository;
use konnect\Data\Models\Post;
use konnect\Data\Models\Follower;
use konnect\Data\Models\Comment;
use konnect\Data\Models\PostShare;
use konnect\Data\Models\PostAction;
// use konnect\Data\Models\PostGenre;

class PostRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('PostRepository', function () {
            return new PostRepository(new Post,new PostShare,new PostAction,new Comment);
        });
    }
}
