<?php

namespace konnect\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    
    protected $listen = [
        'konnect\Events\SomeEvent' => [
            'konnect\Listeners\EventListener',
        ],'konnect\Events\PostNotificationWasGenerated' => [
            'konnect\Listeners\PostNotificationWasGeneratedConfirmation',
        ],'konnect\Events\FriendRefererNotificationWasGenerated' => [
            'konnect\Listeners\FriendRefererNotificationWasGeneratedConfirmation',
        ],'konnect\Events\FriendNotificationWasGenerated' => [
            'konnect\Listeners\FriendNotificationWasGeneratedConfirmation',
        ],'konnect\Events\PostShareNotificationWasGenerated' => [
            'konnect\Listeners\PostShareNotificationWasGeneratedConfirmation',
        ],'konnect\Events\CommentNotificationWasGenerated' => [
            'konnect\Listeners\CommentNotificationWasGeneratedConfirmation',
        ],'konnect\Events\PasswordWasRecovered' => [
            'konnect\Listeners\PasswordWasRecoveredConfirmation',
        ],'konnect\Events\PasswordWasReset' => [
            'konnect\Listeners\PasswordWasResetConfirmation',
        ],'konnect\Events\LikeNotificationWasGenerated' => [
            'konnect\Listeners\LikeNotificationWasGeneratedConfirmation',
        ],'konnect\Events\SendUserActivation' => [
            'konnect\Listeners\SendUserActivationConfirmation',
        ],'konnect\Events\SendAdminActivation' => [
            'konnect\Listeners\SendAdminActivationConfirmation',
        ],'konnect\Events\InviteWasCreated' => [
            'konnect\Listeners\InviteWasCreatedConfirmation',
        ],'konnect\Events\FollowerRequestNotification' => [
            'konnect\Listeners\FollowerRequestNotificationConfirmation',
        ],'konnect\Events\PubliFollowerNotification' => [
            'konnect\Listeners\PubliFollowerNotificationConfirmation',
        ],'konnect\Events\PrivateFollowerNotification' => [
            'konnect\Listeners\PrivateFollowerNotificationConfirmation',
        ]
    ];
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        // dd($this->listen);
        parent::boot();

        //
    }
}
