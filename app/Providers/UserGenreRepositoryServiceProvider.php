<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\UserGenreRepository;
use konnect\Data\Models\UserGenre;

class UserGenreRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('UserGenreRepository', function () {
            return new UserGenreRepository(new UserGenre);
        });
    }
}
