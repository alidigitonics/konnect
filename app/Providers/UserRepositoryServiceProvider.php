<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\UserRepository;
use konnect\User;
use konnect\Data\Models\UserCode;
use konnect\Data\Models\Role;
use konnect\Data\Models\UserRole;
use konnect\Data\Models\Follower;
use konnect\Data\Models\PostAction;
use konnect\Data\Models\Post;
use konnect\Data\Models\UserSocialAccount;
use konnect\Data\Models\BusinessLocation;

class UserRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('UserRepository', function () {
            return new UserRepository(new User, new BusinessLocation , new UserSocialAccount  );
        });
    }
}
