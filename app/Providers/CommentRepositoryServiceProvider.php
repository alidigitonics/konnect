<?php

namespace konnect\Providers;

use Illuminate\Support\ServiceProvider;
use konnect\Data\Repositories\CommentRepository;
use konnect\User;
use konnect\Data\Models\Comment;


class CommentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CommentRepository', function () {
            return new CommentRepository(new Comment );
        });
    }
}
