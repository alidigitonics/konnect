<?php

namespace konnect\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    const ADMINISTRATOR = 1;
    const USER 	= 2;
    const MEDIA_ADMIN 	= 3;
    const TREE_ADMIN 	= 4;
    const SUPER_ADMIN 	= 5;
}
