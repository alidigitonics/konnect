<?php

namespace konnect\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeviceToken extends Model
{
	protected $table = "device_token";
    use SoftDeletes;
   
}
