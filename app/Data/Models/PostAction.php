<?php

namespace konnect\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostAction extends Model
{
    use SoftDeletes;
    
}
