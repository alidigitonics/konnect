<?php

namespace konnect\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessLocation extends Model
{
	protected $table = "business_location";
    use SoftDeletes;

    public function creator(){
    	$this->belongsTo('\konnect\Data\Models\User', 'user_id');
    }
}
