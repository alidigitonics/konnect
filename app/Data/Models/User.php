<?php

namespace konnect\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    public function business(){
    	$this->hasMany('\konnect\Data\Models\BusinessLocation', 'user_id');
    }
}
