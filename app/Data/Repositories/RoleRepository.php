<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Data\Models\Role;
use konnect\Data\Models\UseRole;

use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Helpers\Helper;
use Storage, Image;

class RoleRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the 
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'role-';

    public function __construct(Role $role,UseRole $user_role) {
        $this->model = $role;
        $this->model_post_genre = $user_role;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = parent::findById($id, $refresh, $details);
        return $data;
    }

    public function create(array $data = [], $role_id = 0)
    {

    }
}