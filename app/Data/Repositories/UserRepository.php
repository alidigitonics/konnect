<?php

namespace konnect\Data\Repositories;

use Illuminate\Support\Facades\Event;
use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Data\Models\BusinessLocation;
use konnect\Data\Models\PostShare;
use konnect\Events\FriendNotificationWasGenerated;
use konnect\Events\FriendRefererNotificationWasGenerated;
use konnect\Data\Models\UserFriend;
use konnect\Data\Models\UserProfileAction;
use konnect\Data\Models\UserSocialAccount;
use Illuminate\Support\Facades\Cache;
use konnect\Events\InviteWasCreated;
use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Support\Helper;
use Storage, Image;
use \App;
use Tymon\JWTAuth\Payload;
use Validator;


class UserRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    public $model_business_location;

    /**
     *
     * This is the prefix of the cache key to which the 
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'user-';
    protected $_cacheTotalKey = 'total-users';
    protected $_cacheRolesKey = 'user-role';

    public function __construct(User $user, BusinessLocation $business_location, UserSocialAccount $userSocialAccount) {
        $this->builder = $user;
        $this->model = $user;
        $this->model_business_location = $business_location;
        $this->model_user_social_account = $userSocialAccount;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            if (!$details) {
                unset($data->password);
                unset($data->deleted_at);
                unset($data->access_token);
            }
            $data->konnection_count = UserFriend::whereRaw("user_id = ".hashid_decode($data->id)." or  friend_id = ".hashid_decode($data->id)."")->count();

            $data->profile_view_count = UserProfileAction::where("profile_id","=",hashid_decode($data->id))->count();
                    
            try{
                $token = JWTAuth::getToken();
                if($token){
                    $tokenUser = JWTAuth::toUser($token);
                    $data->mutual_friends_count = $this->countMutualFriends($tokenUser->id,hashid_decode($data->id)); 
                    $isFriendWithYou = UserFriend::whereRaw("((user_id = ".hashid_decode($data->id)." and friend_id = ".$tokenUser->id.") or (user_id = ".$tokenUser->id." and friend_id = ".hashid_decode($data->id)."))")->first();
                    if($isFriendWithYou){
                        $data->is_friend = $isFriendWithYou->status;
                        $isFriendWithYou->id = hashid_encode($isFriendWithYou->id);
                        $isFriendWithYou->user_id = hashid_encode($isFriendWithYou->user_id);
                        $isFriendWithYou->friend_id = hashid_encode($isFriendWithYou->friend_id);
                        $data->is_friend_data = $isFriendWithYou;
                    }
                }
                
            }catch(Exception $e){

            }

            if($data->user_type == "business"){
                // echo $data->id;
                $businessLocation = BusinessLocation::where("user_id","=",hashid_decode($data->id))->get(); 
                $finalBusinesslocation = [];
                if(count($businessLocation) > 0){
                    foreach ($businessLocation as $key => $value) {
                        $finalBusinesslocation[$key] = new \stdClass;
                        $finalBusinesslocation[$key]->id = hashid_encode($value->id);
                        $finalBusinesslocation[$key]->user_id = hashid_encode($value->user_id);
                        $finalBusinesslocation[$key]->longitude = $value->longitude;
                        $finalBusinesslocation[$key]->lattitude = $value->lattitude;
                        $finalBusinesslocation[$key]->name = $value->name;
                        $finalBusinesslocation[$key]->created_at = $value->created_at;
                        $finalBusinesslocation[$key]->updated_at = $value->updated_at;
                        $finalBusinesslocation[$key]->deleted_at = $value->deleted_at;
                    }
                }
                $data->business_location = $finalBusinesslocation;   
            }
            // dd($data);
            

            if($encode){
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }
            
           

            if($data->created_at){
                $data->join_date = $data->created_at;
            }

            // $socialAccountConnected = $this->model_user_social_account->where('user_id','=', $user_id)->first();
            if(isset($data->profile_pic) && $data->profile_pic != Null){

                if (strpos($data->profile_pic, '.') !== false) {
                    list($file, $extension) = explode('.', $data->profile_pic);
                    $file = config('app.files.users.folder_name')."/".$file;
                    // dd($file);
                    $data->image_urls['1x'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'storage/app/public/files/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'storage/app/public/files/'.$file.'@3x.'.$extension;
                } else {
                    if($data->signup_via == 'facebook' && $socialAccountConnected != Null){
                        $data->image_urls['1x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=180&height=180';
                        $data->image_urls['2x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=180&height=180';
                        $data->image_urls['3x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=360&height=360';
                    }else{
                        $data->image_urls = new \stdClass;
                    }
                }   
            }


            if(isset($data->dream_collage) && $data->dream_collage != Null){

                if (strpos($data->dream_collage, '.') !== false) {
                    list($file, $extension) = explode('.', $data->dream_collage);
                    $file = config('app.files.collage.folder_name')."/".$file;
                    // dd($file);
                    $data->dream_collage_url['1x'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                } else {
                    
                        $data->dream_collage_url = new \stdClass;
                }   
            }
            
            
            
            
            /*if( $data->image_path != null ){
                $data->profile_pic = config('app.files.users.full_path')."/".$data->profile_pic;
            }*/
            
        }

        return $data;
    }

    public function register(array $data = [], $role_id = 0){
        if(isset($data['password']) && !empty($data['password'])){
             $data['password'] = Hash::make($data['password']);
             // $data['create_password'] =  1 ;
        }else{
            $data['password'] = Hash::make(rand(7,5));
            // $data['create_password'] =  0 ;
        }
        if($data['signup_via'] == 'email'){
            $saveImage = $this->crop($data['file']);
            if($saveImage){
                unset($data['file']);
            }    
        }
        

        $deviceData=[];
        if(isset($data['device_token']) && isset($data['udid']) && isset($data['device_type'])){
                $deviceData['token'] = $data['device_token'];
                $deviceData['udid'] = $data['udid'];
                $deviceData['type'] = $data['device_type'];
                $deviceRepo = App::make('DeviceTokenRepository');
                unset($data['device_token'],$data['udid'],$data['device_type']);
        }
        

        if(isset($data["social_network_id"]) && $data["social_network_id"] != ""){
            $socialNetworkId = $data["social_network_id"];
        }
        
        // unset($data["fb_access_token"]);
        unset($data["social_network_id"]);
        // $userDataRole = $this->model->join('user_roles', 'users.id', '=', 'user_roles.user_id')->where('users.email', '=', $data['email'])->first(['user_roles.role_id', 'user_roles.user_id']);
        
        // if( $userDataRole == NULL || $data['signup_via'] == "facebook"){
        if($data['user_type'] == 'business'){
                    $location = $data['business_location'];
                    unset($data['business_location']);
        }
            if ($user = parent::create($data,true,false)) {
                if(isset($location)){
                    // dd($location);
                foreach ($location as $key => $value) {
                    $business_location_data = new BusinessLocation();
                    $business_location_data->name = $value->name;
                    $business_location_data->longitude = $value->longitude;
                    $business_location_data->lattitude = $value->lattitude;
                    $business_location_data->user_id = ($user->id);
                    $business_location_data->created_at = Carbon::now();
                    $business_location_data->save();
                }    
            }
                
                // $business_location_data = $this->model_business_location->insert($location);
                // Cache::forget($this->_cacheRolesKey.$user->id);

                if( isset($socialNetworkId) && $socialNetworkId != "" ){
                    $socialAccountInsatance = $this->model_user_social_account->newInstance();
                    $socialAccountInsatance->user_id = $user->id;
                    $socialAccountInsatance->social_network_id = $socialNetworkId;
                    $socialAccountInsatance->social_network = "facebook";
                    if($socialAccountInsatance->save()){

                        if(count($deviceData) > 0 && is_array($deviceData)){
                                $deviceData['user_id'] = $user->id;
                                $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                        }
                        return $this->update([
                            'id' => $user->id,
                            'login_at' => Carbon::now(),
                            'access_token' => JWTAuth::fromUser($user),
                        ],true,true);
                    }
                } else {
                    if(count($deviceData) > 0 && is_array($deviceData)){
                                $deviceData['user_id'] = $user->id;
                                $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                    }
                    
                }
                    return $this->update([
                            'id' => $user->id,
                            'login_at' => Carbon::now(),
                            'access_token' => JWTAuth::fromUser($user),
                    ],true,true);
                // }
            }
        // } else {

        //     if( $userDataRole->role_id == 1){
        //         $userRole = new UserRole();
        //         $userRole->user_id = $userDataRole->user_id;
        //         $userRole->role_id = Role::USER;
        //     }else{
        //         $userRole = new UserRole();
        //         $userRole->user_id = $userDataRole->user_id;
        //         $userRole->role_id = Role::ADMINISTRATOR;
        //     }
        //     Cache::forget($this->_cacheRolesKey.$userDataRole->user_id);
        //     if( $userRole->save() ){

        //         if($role_id != Role::ADMINISTRATOR){
        //             $genres = Genre::orderby("order","asc")->limit(5)->get(['id']);
        //             $userFavouriteGenre['genre_id']=[];
        //             foreach ($genres as $genreid) {
        //                 $userFavouriteGenre['genre_id'][] = $genreid->id;
        //             }
        //             if(count($userFavouriteGenre['genre_id']) > 0){
        //                 $userFavouriteGenre['user_id'] = $user->id;
        //                 $userFavGenre = $this->userGernreRepo->setFavourite($userFavouriteGenre);
        //             }
                        
        //         }

        //         return $this->update([
        //                         'id' => $userDataRole->user_id,
        //                         'login_at' => Carbon::now(),
        //                         'access_token' => JWTAuth::fromUser($userDataRole),
        //                     ],true,true);
        //     }
            
        // }
        return false;
    }

    public function login($data) {

        $user = $this->findByAttribute('email', $data['email'], false, true, false);
        if ($user != NULL ) {
            // if($user->visibility == 1){

                if (Hash::check($data['password'], $user->password)) {
                    
                    if(isset($data['device_token']) && isset($data['udid']) && isset($data['device_type'])){
                            $deviceData['user_id'] = $user->id;
                            $deviceData['token'] = $data['device_token'];
                            $deviceData['udid'] = $data['udid'];
                            $deviceData['type'] = $data['device_type'];
                            $deviceRepo = App::make('DeviceTokenRepository');
                            $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                    }
                    return $this->update([
                        'id' => $user->id,
                        'login_at' => Carbon::now(),
                        'access_token' => JWTAuth::fromUser($user),
                    ],true,true);

                }
            // }else{
            //     return 'user_block';
            // }
        }
        return false;
    }

    public function hasRole($id, $role) {
        return $this->model_user_role->where('user_id', '=', $id)
                                ->where('role_id', '=', $role)
                                ->count() > 0;
    }

    private function crop($file, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
            $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
            $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        }
        return true;
    }

    public function userSearch($pagination = false,$perPage = 10, $data = []){

        $this->builder = $this->model
        ->where(function($query) use($data) {
            $query->where('username', 'LIKE', "%{$data['keyword']}%");
            $query->orWhere('fullname', 'LIKE', "%{$data['keyword']}%");
        })
        ->where('visibility','=',1)
        ->leftJoin('user_roles',function($joins){
            $joins->on('users.id','=','user_roles.user_id');
        })->where('user_roles.role_id','=', Role::USER)
        ->select('users.id');

        return parent::findByAll($pagination, $perPage);
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){
       $users = $this->builder;
        // if(isset($data['role_id']) && $data['role_id'] == Role::ADMINISTRATOR){
        //     $users = $this->model->join('user_roles','user_roles.user_id','=','users.id')->where('user_roles.role_id','=',Role::ADMINISTRATOR)->select(['users.id as id']);
        // }else{
        //     $users = $this->model->join('user_roles','user_roles.user_id','=','users.id')->where('user_roles.role_id','=',Role::USER)->where('users.id','!=',$data['user_id'])->select(['users.id as id']);
        // }
        if( isset($data['user_id']) && $data['user_id'] != ""){
            $userData = User::find($data['user_id']);
            $users = $users->whereRaw('(users.id != '.$data['user_id'].')');
            if( (isset($data['lattitude']) && $data['lattitude'] != "") && (isset($data['longitude']) && $data['longitude'] != "")) {

                $users = $users->whereRaw('id IN (Select `id`, (3959 * ACOS(COS(RADIANS('.$data["lattitude"].')) * COS(RADIANS(lattitude)) * COS(RADIANS(longitude) - RADIANS('.$data["longitude"].')) + SIN(RADIANS('.$data["lattitude"].')) * SIN(RADIANS(lattitude )))) AS distance FROM `users` WHERE `users`.`deleted_at` IS NULL HAVING `distance` < '.$userData->distance.'');
            }
        }
        if( isset($data['filter_by_name']) && $data['filter_by_name'] != ""){
            $users = $users->whereRaw('(users.username LIKE "%'.$data['filter_by_name'].'%" OR users.username LIKE "%'.$data['filter_by_name'].'%")');
        }
        if( isset($data['filter_by_type']) && $data['filter_by_type'] != ""){
            $users = $users->where('users.user_type', '=', $data['filter_by_type']);
        }
        if( isset($data['filter_by_gender']) && $data['filter_by_gender'] != ""){
            $users = $users->where('users.gender', '=', $data['filter_by_gender']);
        }

        if(isset($data['has_connected'])){
            if($data['has_connected'] == 1){
                $users = $users->whereRaw("(id IN (select friend_id from user_friends where user_id = ".$data['user_id']." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$data['user_id']." and status = 'friend' and deleted_at is NULL))");
            }else{
                $users = $users->whereRaw("(id IN (select friend_id from user_friends where user_id = ".$data['user_id']." and status != 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id != ".$data['user_id']." and status = 'friend' and deleted_at is NULL))");
            }
            
        }
        
        
        $this->builder = $users;
        $order = $this->builder->orderBy("users.created_at","desc");
        $this->builder = $order;
        // dd($this->builder->toSql());
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }

    public function countMutualFriends($currentUserId = '',$userId = ''){
                $currentUserFriends = User::whereRaw("(id IN (select friend_id from user_friends where user_id = ".$currentUserId." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$currentUserId." and status = 'friend' and deleted_at is NULL))")->get(['id']);
                $userFriends = User::whereRaw("(id IN (select friend_id from user_friends where user_id = ".$userId." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$userId." and status = 'friend' and deleted_at is NULL))")->get(['id']);
                if(count($currentUserFriends) > 0){
                    $currUserFrnd = [];
                    foreach ($currentUserFriends as $key => $value) {
                       $currUserFrnd[] = $value->id;
                    }
                }else{
                    return 0;
                }
                if(count($userFriends) > 0){
                    $userFrnd = [];
                    foreach ($userFriends as $key => $value) {
                       $userFrnd[] = $value->id;
                    }
                }else{
                    return 0;   
                }
                $temp = array_intersect($currUserFrnd,$userFrnd);
                return count($temp);
                // dd($currUserFrnd,$userFrnd);
    }

    public function listMutualFriends($pagination = true, $page = '', $input = [] ){
                $currentUserId = $input['user_id'];
                $userId = $input['requested_user_id'];
                $currentUserFriends = User::whereRaw("(id IN (select friend_id from user_friends where user_id = ".$currentUserId." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$currentUserId." and status = 'friend' and deleted_at is NULL))")->get(['id']);
                $userFriends = User::whereRaw("(id IN (select friend_id from user_friends where user_id = ".$userId." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$userId." and status = 'friend' and deleted_at is NULL))")->get(['id']);
                if(count($currentUserFriends) > 0){
                    $currUserFrnd = [];
                    foreach ($currentUserFriends as $key => $value) {
                       $currUserFrnd[] = $value->id;
                    }
                }else{
                    return 0;
                }
                if(count($userFriends) > 0){
                    $userFrnd = [];
                    foreach ($userFriends as $key => $value) {
                       $userFrnd[] = $value->id;
                    }
                }else{
                    return 0;   
                }
                $temp = array_intersect($currUserFrnd,$userFrnd);
                // dd($temp);
                $order = $this->builder->whereIn('id', $temp)->orderBy("users.created_at","desc");
                
                $this->builder = $order;
                $user = parent::findByAll($pagination,10,[],true,true);
        
                if($user != NULL){
                    return $user;
                }else{
                    return NULL;
                }
    }

    public function listMutualTags($pagination = true, $page = '', $input = [] ){
                $currentUserId = $input['user_id'];
                $userId = $input['requested_user_id'];
                $currentUserFriends = User::whereRaw("(id IN (select friend_id from user_friends where user_id = ".$currentUserId." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$currentUserId." and status = 'friend' and deleted_at is NULL))")->get(['id']);
                $userFriends = User::whereRaw("(id IN (select friend_id from user_friends where user_id = ".$userId." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$userId." and status = 'friend' and deleted_at is NULL))")->get(['id']);
                if(count($currentUserFriends) > 0){
                    $currUserFrnd = [];
                    foreach ($currentUserFriends as $key => $value) {
                       $currUserFrnd[] = $value->id;
                    }
                }else{
                    return 0;
                }
                if(count($userFriends) > 0){
                    $userFrnd = [];
                    foreach ($userFriends as $key => $value) {
                       $userFrnd[] = $value->id;
                    }
                }else{
                    return 0;   
                }
                $temp = array_intersect($currUserFrnd,$userFrnd);
                $hashTagRepo = App::make('PostHashTagRepository');
                $hashTags = $hashTagRepo->findByAll($pagination,10, $temp);
                if($hashTags != NULL){
                    return $hashTags;
                }else{
                    return NULL;
                }
    }

    
    public function referFriend($data) {

        $notificationData = [];
        $notificationData['sharer_data'] = User::find($data['share_id']);
        $notificationData['user_data'] = User::find($data['user_id']);
        $notificationData['profiler_data'] = User::find($data['share_id']);
        if (count($notificationData) > 0 ) {
            Event::fire(new FriendNotificationWasGenerated($friendsData)); 
        }
        return false;
    }

       // public function setUserVisibility(array $data = []){
    //     $user = User::find($data['id']);

    //     $user->visibility = $data['is_visible'];

    //     if($user->save()){
    //         if($data['is_visible'] == 0){
                
    //             \DB::table('activities')
    //                 ->where(function($query) use ($data){
    //                     $query->where('activities.actor_id', '=', $data['id'])
    //                     ->orWhere('activities.user_id', '=', $data['id']);
    //                 })
    //                 ->update(['visibility' => 0]);
    //             }

    //         Cache::forget($this->_cacheKey.$data['id']);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // public function connectSocialAccount(array $data){
       
        
    //     $socialNetworkExists = $this->model_user_social_account->where('user_id','=',$data['user_id'])
    //                                     ->where('social_network','=', $data['social_network'])
    //                                     ->where('social_network_id','=', $data['social_network_id'])
    //                                     ->first();
    //     if ($socialNetworkExists == NULL) {
    //         $isAlreadyConnected = $this->model_user_social_account->where('user_id','=',$data['user_id'])->count();
    //         if ($isAlreadyConnected) {
    //             return 'already_connected';
    //         }            
    //         $socialNetwork = $this->model_user_social_account->newInstance();
    //         $socialNetwork->user_id = $data['user_id'];
    //         $socialNetwork->social_network_id = $data['social_network_id'];
    //         $socialNetwork->social_network = $data['social_network'];
    //         if ($socialNetwork->save()) {
    //                  return $this->update([
    //                     'id' => $socialNetwork->user_id,
    //                     'updated_at' => Carbon::now(),
    //                 ],true,true);
    //         } else {
    //             return false;
    //         }
    //     } else {
    //         return 'current_already_connected';
    //     }
    // }

    // public function disconnectSocialAccount(array $data){
    //     $isAlreadyConnected = $this->model_user_social_account->where('user_id','=',$data['user_id'])->count();
    //     if ($isAlreadyConnected < 1) {
    //         return 'not_connected';
    //     }
        
    //     $socialNetworkExists = $this->model_user_social_account->where('user_id','=',$data['user_id'])->delete();
    //      return $this->update([
    //                     'id' => $data['user_id'],
    //                     'updated_at' => Carbon::now(),
    //                     ],true,true);
    // }

    public function addUserDetail(array $data = []){
                    $userDetail = new UserDetail();
                    $userDetail->user_id = $data['user_id'];
                    $userDetail->field_title = $data['field_title'];
                    $userDetail->field_value = $data['field_value'];
                    $userDetail->save();
                    $id = hashid_encode($userDetail->id);
                    $userDetail->primary_id = $id;
                    $userDetail->user_id = hashid_encode($userDetail->user_id);
                    $userDetail->field_title = $userDetail->field_title;
                    $userDetail->field_value = $userDetail->field_value;
                    if($userDetail){
                        return $userDetail;    
                    }else{
                        return false;
                    }
    }

    public function userDetailList($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){
       $users = UserDetail::where('user_id',$data['user_id']);
        
        if( isset($data['filter_by_name']) && $data['filter_by_name'] != ""){
            $users = $users->whereRaw('(field_title LIKE "%'.$data['filter_by_name'].'%")');
        }
        if( isset($data['filter_by_value']) && $data['filter_by_value'] != ""){
            $users = $users->where('field_value', '=', $data['filter_by_value']);
        }
              
        
        $this->builder = $users;
        $order = $this->builder->orderBy("users.created_at","desc");
        $this->builder = $order;
        // dd($this->builder->toSql());
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }

    public function addFriend(array $data = []){
            $friendsData = new UserFriend();
            $friendsData->user_id = $data['user_id'];
            $friendsData->friend_id = $data['friend_id'];
            $friendsData->status = $data['status'];
            if (isset($data['blocked_id'])) {
                $friendsData->blocked_id = $data['blocked_id'];
            }
            $friendsData->save();
            $id = hashid_encode($friendsData->id);
            $friendsData->primary_id = $id;
            $friendsData->user_id = hashid_encode($friendsData->user_id);
            $friendsData->friend_id = hashid_encode($friendsData->friend_id);
            $friendsData->blocked_id = hashid_encode($friendsData->blocked_id);
            $friendsData->user_data = $this->findById($data['user_id']);
            $friendsData->friend_data = $this->findById($data['friend_id']);
            if($data['status'] == 'pending'){
                 Event::fire(new FriendNotificationWasGenerated($friendsData));    
            }
            if($friendsData){
                return $friendsData;    
            }else{
                return false;
            }
    }

    public function updateFriend(array $data = []){
            if($data['status'] == 'cancelled'){
                $friendsData =  UserFriend::where('user_id','=',$data['user_id'])->where('friend_id','=',$data['friend_id'])->delete();
            }else{
                $friendsData =  UserFriend::where('user_id','=',$data['user_id'])->where('friend_id','=',$data['friend_id']);
                if (isset($data['blocked_id'])) {
                    $friendsDataUpdate['blocked_id'] = $data['blocked_id'];
                }
                $friendsDataUpdate['status'] = $data['status'];
                $friendsData->update($friendsDataUpdate);
                $friendsData = $friendsData->first();
                if($friendsData){
                    $id = hashid_encode($friendsData->id);
                    $friendsData->primary_id = $id;
                    $friendsData->user_id = hashid_encode($friendsData->user_id);
                    $friendsData->friend_id = hashid_encode($friendsData->friend_id);
                    $friendsData->blocked_id = hashid_encode($friendsData->blocked_id);
                    $friendsData->user_data = $this->findById($data['user_id']);
                    $friendsData->friend_data = $this->findById($data['friend_id']);
                    if($data['status'] == 'friend'){
                        Event::fire(new FriendNotificationWasGenerated($friendsData));    
                    }
                }
            }
            if($friendsData){
                return $friendsData;    
            }else{
                return false;
            }
    }

    // public function updateFriend(array $data = []){
    //         if($data['status'] == 'cancelled'){
    //             $friendsData =  UserFriend::whereRaw('(user_id = '.$data['user_id'].' and friend_id ='.$data['friend_id'].') or (friend_id ='.$data['user_id'].' and  user_id = '.$data['friend_id'].')')->delete();
    //         }else{
    //             $friendsData =  UserFriend::whereRaw('(user_id = '.$data['user_id'].' and friend_id ='.$data['friend_id'].') or (friend_id ='.$data['user_id'].' and  user_id = '.$data['friend_id'].')');
    //             $friendsData->update(['status'=>$data['status']]);
    //             $friendsData = $friendsData->first();
    //             if($friendsData){
    //                 $id = hashid_encode($friendsData->id);
    //                 $friendsData->primary_id = $id;
    //                 $friendsData->user_id = hashid_encode($friendsData->user_id);
    //                 $friendsData->friend_id = hashid_encode($friendsData->friend_id);
    //                 $friendsData->user_data = $this->findById($data['user_id']);
    //                 $friendsData->friend_data = $this->findById($data['friend_id']);
    //                 if($data['status'] == 'friend'){
    //                     Event::fire(new FriendNotificationWasGenerated($friendsData));    
    //                 }
    //             }
                    
                    
    //             }
    //         if($friendsData){
    //             return $friendsData;    
    //         }else{
    //             return false;
    //         }
    // }

    public function friendList($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){
        if ($data['status'] == 'pending') {
            $users = $this->builder->whereRaw("(id in (select user_id from user_friends where friend_id = ".$data['user_id']." and status = 'pending' and deleted_at is NULL)) and id != ".$data['user_id']."");
        }else{
            $users = $this->builder->whereRaw("(id in (select friend_id from user_friends where user_id = ".$data['user_id']." and status = 'friend' and deleted_at is NULL) or id in (select user_id from user_friends where friend_id = ".$data['user_id']." and status = 'friend' and deleted_at is NULL)) and id != ".$data['user_id']."");    
        }
        
        
        if( isset($data['filter_by_name']) && $data['filter_by_name'] != ""){
            $users = $users->whereRaw('(users.first_name LIKE "%'.$data['filter_by_name'].'%" OR users.last_name LIKE "%'.$data['filter_by_name'].'%")');
        }
        
        $this->builder = $users;
        $order = $this->builder->orderBy("users.first_name","asc");
        $this->builder = $order;
        // dd($this->builder->toSql());
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }

    public function createProfileView(array $data = []) {
        $response = UserProfileAction::create($data);
        return $response;
    }
                    
    public function PostShareList($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){
        
        $users = $this->builder->join("Post_Shares","Post_Shares.user_id","=","users.id")->where("Post_Shares.old_post_id","=",$data['post_id'])->select("users.id");
        
        $this->builder = $users;
        // $order = $this->builder->orderBy("users.first_name","asc");
        // $this->builder = $order;
        // dd($this->builder->toSql(),$data);
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }             

}