<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\Data\Models\Comment;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use \App;
use Illuminate\Support\Facades\Cache;   

class CommentRepository extends AbstractRepository implements RepositoryContract {

 
    public $model;


    protected $_cacheKey = 'comment-';
    protected $_cacheTotalKey = 'total-comment';
    public $_cacheTotalCommentKey = 'total-post-comment';
    

    public function __construct(Comment $comment) {
 
        $this->model    = $comment;
        $this->builder    = $comment;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            $userRepo = App::make('UserRepository');
            $data->user = $userRepo->findById($data->user_id,$refresh, $details, $encode);
            $commentRepliesCount = Comment::where('comment_type','=','reply')->where('comment_id','=',hashid_decode($data->id));
            if ($commentRepliesCount->count() > 0) {
                // dd($commentRepliesCount);
                $commentReplies = $commentRepliesCount->get();    
                foreach ($commentReplies as $key => $value) {
                    $postReplyId = ($value->id);
                    $value->reply_id = hashid_encode($postReplyId);
                    $value->post_id = hashid_encode($value->post_id);
                    $value->user_id = hashid_encode($value->user_id);
                    $value->comment_id = hashid_encode($value->comment_id);
                }
                $data->comment_replies = $commentReplies;
                // dd($data->comment_replies);
            }
            


        }
        if($encode){
            if(isset($data->post_id)){
                $data->post_id = hashid_encode($data->post_id);
            }
            if(isset($data->user_id)){
                $data->user_id = hashid_encode($data->user_id);
            }
                
                
        }

        return $data;
    }

    public function findByAll($pagination = false,$perPage = 10, $data = []){
        $comments = $this->builder;
        if(isset($data['post_id']) && $data['post_id'] != null){
            $comment = $this->builder->where('post_id', '=', $data['post_id']);
            $this->builder = $comment;
        }
        if(isset($data['keyword']) && $data['keyword'] != null){
            $comment = $this->builder->where('comment', 'LIKE', "%{$data['keyword']}%");
            $this->builder = $comment;
        }
        if(isset($data['last_comment_id']) && $data['last_comment_id'] != null){
            $comment = $this->builder->where('id', '<', $data['last_comment_id']);
            $this->builder = $comment;
        }
        if((isset($data['comment_type']) && $data['comment_type'] == 'reply') && (isset($data['comment_id']) && $data['comment_id'] != NULL)){
            $comment = $this->builder->where('comment_type', '=', $data['comment_type'])->where('comment_id', '=', $data['comment_id']);
            $this->builder = $comment;
        }else{
            $comment = $this->builder->where('comment_type', '=', 'simple');
            $this->builder = $comment;
        }
        unset($data);
        $data['data']=[];
        $comments = $comment->orderBy('id', 'DESC')->limit($perPage)->get(['id']);
        // dd($comments);
        if ($comments) {
            foreach ($comments as $comment) {
                $model = $this->findById($comment->id,false,true,true);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        }
        return $data;

        // return parent::findByAll($pagination, $perPage);
    }

    public function getCommentReplies($comment_id=0){
        $comments = $this->builder->where('comment_type', '=', 'reply')->where('comment_id', '=', $comment_id);
        $this->builder = $comments;
        return parent::findByAll(false, 10);
    }

    public function postCommentTotal($id) {
       
        $data = Cache::get($this->_cacheTotalCommentKey.$id);
        
        if ($data == NULL) {
       
            $postCommentCount = $this->model->where('post_id', '=', $id)->count();
         
            if ($postCommentCount != NULL) {

                $data =  $postCommentCount;

                Cache::forever($this->_cacheTotalCommentKey.$id, $data);
            } else {
                return 0;
            }
        }
        
        return $data;
    }
}
