<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\Data\Models\Appointment;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use \App;
use Illuminate\Support\Facades\Cache;   
use Carbon\Carbon;

class AppointmentRepository extends AbstractRepository implements RepositoryContract {

 
    public $model;


    protected $_cacheKey = 'appointment-';
    protected $_cacheTotalKey = 'total-appointment';
    public $_cacheTotalAppointmentKey = 'total-post-appointment';
    

    public function __construct(Appointment $appointment) {
 
        $this->model    = $appointment;
        $this->builder    = $appointment;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            $userRepo = App::make('UserRepository');
            $data->user = $userRepo->findById($data->user_id,$refresh, $details, $encode);

        }
        if($encode){
            if(isset($data->post_id)){
                $data->post_id = hashid_encode($data->post_id);
            }
            if(isset($data->user_id)){
                $data->user_id = hashid_encode($data->user_id);
            }
                
                
            }

        return $data;
    }
    public function findByAll($pagination = false,$perPage = 10, $data = []){
        $appointments = $this->builder;
        if(isset($data['post_id']) && $data['post_id'] != null){
            $appointment = $this->builder->where('post_id', '=', $data['post_id']);
            $this->builder = $appointment;
        }
        if(isset($data['keyword']) && $data['keyword'] != null){
            $appointment = $this->builder->where('appointment', 'LIKE', "%{$data['keyword']}%");
            $this->builder = $appointment;
        }
        if(isset($data['last_appointment_id']) && $data['last_appointment_id'] != null){
            $appointment = $this->builder->where('id', '<', $data['last_appointment_id']);
            $this->builder = $appointment;
        }
        unset($data);
        $data['data']=[];
        $appointments = $appointment->orderBy('id', 'DESC')->limit($perPage)->get(['id']);
        // dd($appointments);
        if ($appointments) {
            foreach ($appointments as $appointment) {
                $model = $this->findById($appointment->id,false,true,true);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        }
        return $data;

        // return parent::findByAll($pagination, $perPage);
    }

    public function postAppointmentTotal($id) {
       
        $data = Cache::get($this->_cacheTotalAppointmentKey.$id);
        
        if ($data == NULL) {
       
            $postAppointmentCount = $this->model->where('post_id', '=', $id)->count();
         
            if ($postAppointmentCount != NULL) {

                $data =  $postAppointmentCount;

                Cache::forever($this->_cacheTotalAppointmentKey.$id, $data);
            } else {
                return 0;
            }
        }
        
        return $data;
    }

    public function create(array $data = [], $role_id = 0){
        
        $input["appointment_name"]= $data["appointment_name"];
        $input["appointment_date"]= $data["appointment_date"];
        $input["appointment_email"]= $data["appointment_email"];
        $input["user_id"]= $data["user_id"];
        $input["client_id"]= $data["client_id"];
        $input["email_user_type"]= $data["email_user_type"];
        $input["appointment_longitude"]= $data["appointment_longitude"];
        $input["appointment_lattitude"]= $data["appointment_lattitude"];
        $input["distance"]= $data["distance"];
        $input["location_address"]= $data["location_address"];
        $input["status"]= 'active';
        $input["created_at"]= Carbon::now();
        
        
    if ($appointment = parent::create($input)) {
            // event(new PostNotificationWasGenerated($appointment));
            return $appointment;
        }else{
            return false;
        }
    }
}
