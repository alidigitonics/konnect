<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\Data\Models\PostHashTag;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use \App;
use Illuminate\Support\Facades\Cache;   

class PostHashTagRepository extends AbstractRepository implements RepositoryContract {

 
    public $model;


    protected $_cacheKey = 'post-hash-tag-';
    protected $_cacheTotalKey = 'total-post-hash-tag';
    public $_cacheTotalPostHashTagKey = 'total-post-post-hash-tag';
    

    public function __construct(PostHashTag $postHashTag) {
 
        $this->model    = $postHashTag;
        $this->builder    = $postHashTag;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            // $userRepo = App::make('UserRepository');
            // $data->user = $userRepo->findById($data->user_id,$refresh, $details, $encode);

        }
        if($encode){
            // if(isset($data->post_id)){
                // $data->post_id = hashid_encode($data->post_id);
            // }
            if(isset($data->user_id)){
                $data->user_id = hashid_encode($data->user_id);
            }
                
                
            }

        return $data;
    }
    public function findByAll($pagination = false,$perPage = 10, $data = []){
        
        $hashTag = NULL;
        if(isset($data) && $data != null){
            $hashTag = $this->builder->whereIn('user_id',$data)->groupBy('hash_tag');
            $this->builder = $hashTag;
            $hashTag = parent::findByAll($pagination,$perPage,[],true,true);
        }
        

        if($hashTag != NULL){
            return $hashTag;
        }else{
            return NULL;
        }
        return $hashTag;

        // return parent::findByAll($pagination, $perPage);
    }

}
