<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Data\Models\PostAction;
use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Helpers\Helper;
use Storage, Image;
use Illuminate\Support\Facades\Cache;

class PostActionRepository extends AbstractRepository implements RepositoryContract {

 
    public $model;


    protected $_cacheKey = 'post-action-';
    protected $_cacheTotalKey = 'total-post-actions';
    protected $_cacheLikeKey = 'total-post-likes';
    protected $_cacheViewKey = 'total-post-views';
    

    public function __construct(PostAction $post_action) {
 
        $this->model = $post_action;
    }

    public function postLikesTotal($id, $type) {
 
        $data = Cache::get($this->_cacheLikeKey.$id);
        
        if ($data == NULL) {
        
            $postLikeCount = $this->model->where('type','=', $type)->where('post_id', '=', $id)->count();
            if ($postLikeCount != NULL) {

                $data =  $postLikeCount;

                Cache::forever($this->_cacheLikeKey.$id, $data);
            } else {
                return 0;
            }
        }
        
        return $data;
    }

    public function postViewsTotal($id, $type) {
 
        $data = Cache::get($this->_cacheViewKey.$id);
        
        if ($data == NULL) {
       
            $postViewCount = $this->model->where('type','=', $type)->where('post_id', '=', $id)->count();
            if ($postViewCount != NULL) {

                $data =  $postViewCount;

                Cache::forever($this->_cacheViewKey.$id, $data);
            } else {
                return 0;
            }
        }
        
        return $data;
    }


    public function create(array $data = []) {
    	
    	if($data['type'] == 'like'){
    		Cache::forget($this->_cacheLikeKey.$data['post_id']);
    	} elseif ($data['type'] == 'view') {
    		Cache::forget($this->_cacheViewKey.$data['post_id']);
    	}
        
        $response = parent::create($data);
        return $response;
    }
    public function update(array $data = []) {
    	
    	if($data['type'] == 'unlike'){
    		Cache::forget($this->_cacheLikeKey.$data['post_id']);
    	} 
        // unset($input['post_id'],$input['user_id']);
        $response = parent::update($data);
        return $response;
    }
}
