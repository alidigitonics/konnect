<?php

namespace konnect\Data\Repositories;

use Illuminate\Support\Facades\Cache;

use konnect\Data\Contracts\RepositoryContract;

use Carbon\Carbon;
use Schema, DB;

abstract class AbstractRepository implements RepositoryContract {

    public $builder;
    /**
     *
     * This method will create a new model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Danish Jamil
     *
     **/

    public function create(array $data = [],$details = false,$encode=true) {

        $newInstance = $this->model->newInstance();
        foreach ($data as $column => $value) {
            $newInstance->{$column} = $value;
        }
        $newInstance->created_at = Carbon::now();
        $newInstance->updated_at = Carbon::now();
        if ($newInstance->save()) {
            Cache::forget($this->_cacheTotalKey);
            return $this->findById($newInstance->id, true,$details,$encode);
        } else {
            return false;
        }
    }

    /**
     *
     * This method will fetch single model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Danish Jamil
     *
     **/
    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = Cache::get($this->_cacheKey.$id);
        
        if ($data == NULL || $refresh == true) {
            $query = $this->model->find($id);
            if ($query != NULL) {

                $data = (object) $query->getAttributes();

                Cache::forever($this->_cacheKey.$id, $data);
            } else {
                return null;
            }
        }
        if($encode){
            $data->id = hashid_encode($data->id);
        }
        return $data;
    }

    public function findByAttribute($attribute, $value, $refresh = false, $details = false, $encode = true, $postView = true) {
        
        $model = $this->model->newInstance()
                        ->where($attribute, '=', $value)->first(['id']);

        if ($model != NULL) {
            $model = $this->findById($model->id, $refresh, $details, $encode,$postView);
        }
        return $model;
    }

    /**
     *
     * This method will fetch all exsiting models
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Danish Jamil
     *
     **/

    public function findByAll($pagination = false, $perPage = 10, array $input = [],$detail = false,$encode = true ,$postView = true, $refresh = false) {
        $ids = $this->builder;
        if ($pagination == true) {

            $ids = $ids->paginate($perPage);
            $models = $ids->items();

        } else {
            $sql = $ids->toSql();
            $binds = $ids->getBindings();
            $models = DB::select($sql, $binds);
        }
        
        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id,$refresh,$detail,$encode,$postView);
                
                if ($model) {

                    unset($model->recover_password_key);
                    unset($model->recover_attempt_at);

                    $data['data'][] = $model;
                }
            }
        }

        if ($pagination == true) {
            $data['pagination'] = [];
            $data['pagination']['total'] = $ids->total();
            $data['pagination']['current'] = $ids->currentPage();
            $data['pagination']['first'] = 1;
            $data['pagination']['last'] = $ids->lastPage();

            if($ids->hasMorePages()) {
                if ( $ids->currentPage() == 1) {
                    $data['pagination']['previous'] = 0;
                } else {
                    $data['pagination']['previous'] = $ids->currentPage()-1;
                }
                $data['pagination']['next'] = $ids->currentPage()+1;
            } else {
                $data['pagination']['previous'] = $ids->currentPage()-1;
                $data['pagination']['next'] =  $ids->lastPage();
            }
            if ($ids->lastPage() > 1) {
                $data['pagination']['pages'] = range(1,$ids->lastPage());
            } else {
                $data['pagination']['pages'] = [1];
            }
            $data['pagination']['from'] = $ids->lastItem();
            $data['pagination']['to'] = $ids->firstItem();
        }
        return $data;
    }

    /**
     *
     * This method will update an existing model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Danish Jamil
     *
     **/
    public function update(array $data = [],$refresh = true,$detail = false,$encode = true) {

        $model = $this->model->find($data['id']);
        if ($model != NULL) {
            foreach ($data as $column => $value) {
                $model->{$column} = $value;
            }
            $model->updated_at = Carbon::now();

            if ($model->save()) {
                return $this->findById($data['id'],$refresh,$detail,$encode);
            }
            return false;
        }
        return NULL;
    }

    /**
     *
     * This method will remove model
     * and will return output back to client as json
     *
     * @access public
     * @return bool
     *
     * @author Danish Jamil
     *
     **/
    public function deleteById($id) {
        $model = $this->model->find($id);
        if($model != NULL) {
            Cache::forget($this->_cacheKey.$id);
            Cache::forget($this->_cacheTotalKey);
            return $model->delete();
        }
        return false;
    }

    /**
     *
     * This method will fetch total models
     * and will return output back to client as json
     *
     * @access public
     * @return integer
     *
     * @author Danish Jamil
     *
     **/
    public function findTotal($refresh=false) {

        $total = Cache::get($this->_cacheTotalKey);
        if ($total == NULL || $refresh == true) {
            $total = $this->model->count();
            Cache::forever($this->_cacheTotalKey, $total);
        }
        return $total;
    }
}
