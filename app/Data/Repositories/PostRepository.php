<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Events\PostNotificationWasGenerated;
use konnect\Events\PostShareNotificationWasGenerated;
use konnect\Events\CommentNotificationWasGenerated;
use konnect\Data\Repositories\UserRepository;
use konnect\Data\Models\Post;
use konnect\Data\Models\PostHashTag;
use konnect\Data\Models\PostFriendTag;
use konnect\Data\Models\Follower;
use konnect\Data\Models\Comment;
use konnect\Data\Models\PostShare;
use konnect\Data\Models\PostAction;
// use konnect\Data\Models\PostGenre;
use konnect\Data\Models\Genre;
use JWTAuth,FFMpeg, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Support\Helper;
use Storage, Image, DB;
use \App;
use Illuminate\Support\Facades\Cache; 


class PostRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    public $model_post_share;

    /**
     *
     * This is the prefix of the cache key to which the 
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'post-';
    protected $_cacheTotalKey = 'total-posts';

    public function __construct(Post $post,PostShare $post_share, PostAction $post_action,Comment $comment) {
        $this->model = $post;
        $this->builder = $post;
        $this->model_post_share = $post_share;
        $this->model_post_action = $post_action;
        $this->model_comment = $comment;
        $this->commentRepo = App::make('CommentRepository');
    }

    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = parent::findById($id, $refresh, $details,$encode);
        
        if($data != NULL){            
            $post_id = hashid_decode($data->id);
            $postActionRepo = App::make('PostActionRepository');
            
            $data->post_likes = $postActionRepo->postLikesTotal($post_id,'like');
            
            $data->post_views = $postActionRepo->postViewsTotal($post_id,'view');
           
            $data->post_comment_count = $this->commentRepo->postCommentTotal($post_id);

            $data->post_share_count = $this->model_post_share->where('old_post_id',$post_id)->count();
            // $genres = Genre::join("post_genres","post_genres.genre_id","=","genres.id")->where("post_genres.post_id","=",$post_id)->get(['genres.id','title','order','visibility','post_id']);
            $hash_tag_data = PostHashTag::where('post_id',$post_id)->get();
            if(count($hash_tag_data) > 0){
                // dd($hash_tag_data);
                $hashTag = [];
                foreach ($hash_tag_data as $key => $value) {
                    $hashTag[$key] = new \stdClass;
                    $hashTag[$key]->id = hashid_encode($value->id);
                    $hashTag[$key]->hash_tag = $value->hash_tag;
                    $hashTag[$key]->user_id = hashid_encode($value->user_id);
                    $hashTag[$key]->post_id = hashid_encode($value->post_id);
                }
                $data->hash_tag_data = $hashTag;    
            }

            if($data->share_to_all == 1){
                // dd($data->old_post_id);
                if(isset($data->old_post_id)){
                    $data->old_post_data = $this->findById($data->old_post_id,false,true,true,true);    
                }
                
            }
            
            $friend_tag_data = PostFriendTag::where('post_id',$post_id)->get();
            if(count($friend_tag_data) > 0){
                $friendTag = [];
                foreach ($friend_tag_data as $key => $value) {
                    $friendTag[$key] = new \stdClass;
                    $friendTag[$key]->id = hashid_encode($value->id);
                    $friendTag[$key]->friend_tag = User::find($value->friend_id);
                    $friendTag[$key]->user_id = hashid_encode($value->user_id);
                    $friendTag[$key]->post_id = hashid_encode($value->post_id);
                }
                $data->friend_tag_data = $friendTag;    
            }
            
            // if(count($hashTagData) > 0){
            //     foreach ($hashTagData as $key => $value) {
            //         # code...
            //     }
            // }

            if($data->type == "photo"){
                if (strpos($data->path, '.') !== false) {
                    list($file, $extension) = explode('.', $data->path);
                    $file = config('app.files.posts.public_relative').$file;
                    $data->image_urls['1x'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'storage/app/public/files/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'storage/app/public/files/'.$file.'@3x.'.$extension;
                    $data->image_urls['thumb'] = config('app.url').'storage/app/public/files/'.$file.'@thumb.'.$extension;
                } else {
                    $data->image_urls = new \stdClass;
                }  
           }else{
                 if (strpos($data->path, '.') !== false) {
                    list($file, $extension) = explode('.', $data->path);
                    $videoThumbnail = config('app.files.posts.public_relative').'thumbnails/'.$file;
                    $file = config('app.files.posts.public_relative').$file;
                    $data->video_url['hd'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                    $data->video_url['sd'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                    $data->video_url['ld'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                    // Thumbnail Image Url
                    $data->image_urls['1x'] = config('app.url').'storage/app/public/files/'.$videoThumbnail.'.png';
                    $data->image_urls['2x'] = config('app.url').'storage/app/public/files/'.$videoThumbnail.'@2x.png';
                    $data->image_urls['3x'] = config('app.url').'storage/app/public/files/'.$videoThumbnail.'@3x.png';
                    $data->image_urls['thumb'] = config('app.url').'storage/app/public/files/'.$videoThumbnail.'@thumb.png';
                } else {
                    $data->video_url = new \stdClass;
                }  
           }
            


            try{
                $token = JWTAuth::getToken();
                if($token){
                    $tokenUser = JWTAuth::toUser($token);
                    $likeExist = $this->model_post_action->where('type','=', 'like')->where('post_id', '=', $post_id)->where('user_id','=', $tokenUser->id)->first();
                    if ($likeExist) {
                        $data->is_like  = true;
                        $data->is_like_data  = $likeExist;
                    }else {
                         $data->is_like = false;
                    }    
                    $viewExist = $this->model_post_action->where('type','=', 'view')->where('post_id', '=', $post_id)->where('user_id','=', $tokenUser->id)->first();
                    if ($viewExist) {
                        $data->is_view  = true;
                    }else {
                        $data->is_view = false;
                    }    
                }
                
            }catch(Exception $e){

            }

           
            $userRepo = App::make('UserRepository');
            $data->creator = $userRepo->findById($data->user_id,false,false,true,false);


            if($encode){
                $data->user_id = hashid_encode($data->user_id);
                // $finalGenre = [];
                // foreach ($genres as $key => $value) {
                //     $finalGenre[$key] = new \stdClass;
                //     $finalGenre[$key]->id = hashid_encode($value->id);
                //     $finalGenre[$key]->title = $value->title;
                //     $finalGenre[$key]->order = $value->order;
                //     $finalGenre[$key]->visibility = $value->visibility;
                //     $finalGenre[$key]->post_id = hashid_encode($value->post_id);
                // }
                // $data->genre = $finalGenre;
            }else{
                // $data->genre = $genres;
            }

            

        }
        
        return $data;
    }

    public function create(array $data = [], $role_id = 0){
            if(isset($data['content']) && $data['content'] != NULL){
                $content = $this->saveImage($data['content']);
                if($content){
                    // dd($content);
                    $data['path'] = $content;
                    if($data['type'] == 'video'){
                        $videoName = $content;
                        list($thumbnailName, $ext) = explode('.', $videoName);
                        // dd($videoName);
                        $video = FFMpeg::open('public/files/'.config('app.files.posts.folder_name').'/'.$videoName)
                            ->getFrameFromSeconds(1)
                            ->export()
                            ->toDisk('public')
                            ->save(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                            $fileThumb = Storage::get(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                            $image = Image::make($fileThumb);
                            $width = $image->width();
                            $height = $image->height();
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'@3x.png', $image->stream());
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'@2x.png', $image->resize($width / 1.5, $height / 1.5)->stream());
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'@thumb.png', $image->fit('510', '240', function($constraint) { $constraint->aspectRatio(); })->stream());
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'.png', $image->resize($width / 3, $height / 3)->stream());

                    }
                   
                }
            }else{
                $data['path'] = "NULL";
                $data['original_name'] = "NULL";
                $data['type'] = "NULL";
                $data['file_type'] = "NULL";
            }
            if(isset($data['hash_tag'])){
                $hashTagData = $data['hash_tag'];
                unset($data['hash_tag']);
            }
            if(isset($data['friend_tag'])){
                $friendTagData = $data['friend_tag'];
                unset($data['friend_tag']);
            }
            if(isset($data['share_to_all']) && $data['share_to_all'] == 1){
                $data['post_id'] = hashid_decode($data['post_id']);
                $postShareId = $data['post_id'];
                $input['old_post_id']         = $data['post_id'];
                unset($data['post_id']);
                $input['share_to_all']         = $data['share_to_all'];
                
            }
            $input['title']         = $data['title'];
            $input['path']          = $data['path'];
            $input['original_name'] = $data['original_name'];
            $input['type']          = $data['type'];
            $input['file_type']     = $data['file_type'];
            $input['description']   = $data['description'];
            $input['user_id']       = $data['user_id'];
            $input['location']       = $data['location'];
            $input['privacy']       = $data['privacy'];
            $input['can_comment']   = $data['is_commentable'];
            $input['post_type']   = $data['post_type'];
            $input['lattitude']   = $data['lattitude'];
            $input['longitude']   = $data['longitude'];
            
            $input['is_blocked']    = 0;
            
            if ($post = parent::create($input)) {
                $postID = hashid_decode($post->id);
                if(isset($data['share_to_all']) && $data['share_to_all'] == 1){
                        $postShare = new PostShare();
                        $postShare->old_post_id = $postShareId;
                        $postShare->post_id = $postID;
                        $postShare->user_id = $data['user_id'];
                        $postShare->share_to_all = $data['share_to_all'];
                        $postShare->save();
                }
                if(isset($friendTagData) && count($friendTagData) > 0){
                    foreach ($friendTagData as $value) {
                        $friendTag = new PostFriendTag();
                        $friendTag->friend_id = hashid_decode($value);
                        $friendTag->post_id = $postID;
                        $friendTag->user_id = $data['user_id'];
                        $friendTag->save();
                    }
                }

                if(isset($hashTagData) && count($hashTagData) > 0){
                    foreach ($hashTagData as $value) {
                        $postGenre = new PostHashTag();
                        $postGenre->hash_tag = $value;
                        $postGenre->post_id = $postID;
                        $postGenre->user_id = $data['user_id'];
                        $postGenre->save();
                    }
                }

                if($post){
                    event(new PostNotificationWasGenerated($post));
                    return $this->findById($postID,false,true,true);
                }else{
                    return false;
                }
            }   
    }

    public function update(array $data = [], $role_id = 0){
            if(isset($data['content']) && $data['content'] != NULL){
                $content = $this->saveImage($data['content']);
                if($content){
                    // dd($content);
                    $data['path'] = $content;
                    if($data['type'] == 'video'){
                        $videoName = $content;
                        list($thumbnailName, $ext) = explode('.', $videoName);
                        // dd($videoName);
                        $video = FFMpeg::open('public/files/'.config('app.files.posts.folder_name').'/'.$videoName)
                            ->getFrameFromSeconds(1)
                            ->export()
                            ->toDisk('public')
                            ->save(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                            $fileThumb = Storage::get(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                            $image = Image::make($fileThumb);
                            $width = $image->width();
                            $height = $image->height();
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'@3x.png', $image->stream());
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'@2x.png', $image->resize($width / 1.5, $height / 1.5)->stream());
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'@thumb.png', $image->fit('510', '240', function($constraint) { $constraint->aspectRatio(); })->stream());
                            $thumbStore = Storage::put(config('app.files.posts.folder_name').'/thumbnails/'.$thumbnailName.'.png', $image->resize($width / 3, $height / 3)->stream());

                    }
                   
                }
            }

            if(isset($data['hash_tag'])){
                $hashTagData = $data['hash_tag'];
                unset($data['hash_tag']);
            }
            if(isset($data['friend_tag'])){
                $friendTagData = $data['friend_tag'];
                unset($data['friend_tag']);
            }
            unset($data['content']);
            if ($post = parent::update($data)) {
                $postID = hashid_decode($post->id);
                
                if(isset($friendTagData) && count($friendTagData) > 0){
                    foreach ($friendTagData as $value) {
                        $friendTag = new PostFriendTag();
                        $friendTag->friend_id = hashid_decode($value);
                        $friendTag->post_id = $postID;
                        $friendTag->user_id = $data['user_id'];
                        $friendTag->save();
                    }
                }

                if(isset($hashTagData) && count($hashTagData) > 0){
                    foreach ($hashTagData as $value) {
                        $postGenre = new PostHashTag();
                        $postGenre->hash_tag = $value;
                        $postGenre->post_id = $postID;
                        $postGenre->user_id = $data['user_id'];
                        $postGenre->save();
                    }
                }

                if($post){
                    event(new PostNotificationWasGenerated($post));
                    return $this->findById($postID,false,true,true);
                }else{
                    return false;
                }
            }   
    }

    private function saveImage($image) {
        $action = $image->store(config('app.files.posts.folder_name'));
        if($action){
            $crop = $this->crop($image);
        }
        return $image->hashName(); //config('app.files.posts.folder_name').'/'.$name.'.'.$ext;
    }

    public function crop($file, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {
        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            $store = Storage::put(config('app.files.posts.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
            $store = Storage::put(config('app.files.posts.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
            $store = Storage::put(config('app.files.posts.folder_name').'/'.$name.'@thumb.'.$ext, $image->fit('510', '240', function($constraint) { $constraint->aspectRatio(); })->stream());
            $store = Storage::put(config('app.files.posts.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        }
        return true;
    }

    public function addComment(array $data = []){
            
        $addComment = new Comment;
        $addComment->post_id = $data['post_id'];
        $addComment->user_id = $data['user_id'];
        $addComment->comment = $data['comment'];
        $addComment->comment_type = $data['comment_type'];
        if ($data['comment_type'] == 'reply') {
            $addComment->comment_id = $data['comment_id'];
        }

        if ( $addComment->save() ) {
            $post = $this->findById($addComment->post_id);
            Cache::forget($this->commentRepo->_cacheTotalCommentKey.$addComment->post_id);    
            if($post != NULL){
                $post->user_commneted = $data['user_id'];
                event(new CommentNotificationWasGenerated($post,$addComment));
                $count = Comment::where('post_id', '=', $data['post_id'])->count();
                $post->post_count = $count;
                $commentRepo = App::make('CommentRepository');
                $current_comment = $commentRepo->findById($addComment->id,false,true,true);
                $post->current_comment = $current_comment;
            }
            return $post;
        }
    }

    public function findAllById($pagination = false,$perPage = 10, $data = [],$encode=false){
        $query = $this->builder->join('post_shares',"posts.id","=","post_shares.post_id")->where('posts.user_id','=',$data['user_id'])->where('post_shares.follower_id','=',$data['follower_id']);
        $this->builder = $query;
        $posts = parent::findByAll($pagination,$perPage);
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function feed($pagination = false,$perPage = 10, $data = [],$encode=false){

        // $follower_id = $data['follower_id']; 
        // $param['view'] = "view";
        // $param['user_id'] = $follower_id;
        // $query = $this->builder->leftJoin('post_actions', function($join) use($param) 
        //                  {
        //                      $join->on('posts.id', '=', 'post_actions.post_id')->where('post_actions.type', '=', $param['view'])->where('post_actions.user_id', '=', $param['user_id']);
        //                  })->whereRaw("(posts.user_id IN (SELECT user_id FROM `followers` WHERE `follower_id` = ".$follower_id.") AND posts.privacy = 'public') 
        //      OR (posts.user_id = ".$follower_id.")
        //       OR (posts.id IN (SELECT post_id FROM `post_shares` WHERE `follower_id` = ".$follower_id."))")->orderBy("post_actions.id","asc")->orderBy("posts.id","desc")->select("posts.id");
        $query = $this->builder->whereRaw("(posts.user_id IN (select friend_id from user_friends where user_id = ".$data['user_id']." and status = 'friend' and deleted_at is NULL) or posts.user_id in (select user_id from user_friends where friend_id = ".$data['user_id']." and status = 'friend' and deleted_at is NULL)) or posts.user_id = ".$data['user_id']."")->orderBy("posts.id","desc")->select("posts.id");
        $this->builder = $query;
        $posts = parent::findByAll($pagination,$perPage,[],true);
        $posts['post_type'] = 'feed';
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function newsFeed($pagination = false,$perPage = 10, $data = [],$encode=false){

        $follower_id = $data['follower_id']; 
        $param['view'] = "view";
        $param['user_id'] = $follower_id;
        $unViewed = $this->builder->leftJoin("user_roles","posts.user_id","=","user_roles.user_id")->whereRaw("(posts.user_id IN (SELECT user_id FROM `followers` WHERE `follower_id` = ".$follower_id." AND STATUS = 'followed') AND privacy = 'public' OR (posts.id IN (SELECT post_id FROM `post_shares` WHERE `follower_id` = ".$follower_id."))) AND posts.id NOT IN (SELECT post_id FROM `post_actions` WHERE user_id = ".$follower_id." AND `type` = 'view') ")->orderByRaw("user_roles.role_id = 3 DESC,posts.id DESC")->limit($perPage)->get(['posts.id']);

        $viewed = $this->builder->Join("post_actions","posts.id","=","post_actions.post_id")->whereRaw("(posts.user_id IN (SELECT user_id FROM `followers` WHERE `follower_id` = ".$follower_id." AND STATUS = 'followed') AND privacy = 'public' OR ( posts.id IN (SELECT post_id FROM `post_shares` WHERE `follower_id` = ".$follower_id."))) AND post_actions.user_id = ".$follower_id." AND post_actions.`type` = 'view'")->orderBy("post_actions.id","DESC")->limit($perPage)->get(['posts.id as id','post_actions.id as post_action_id']);
       
        $data = [];
        if($unViewed){
            foreach ($unViewed as $key => $unview) {
                $unviewedItems = $this->findById($unview->id,false,true,true,true);
                if($unviewedItems != NULL){
                    $data['unviewed'][] = $unviewedItems;    
                }
            }
        }
        if($viewed){
            foreach ($viewed as $key => $view) {
                $viewedItems = $this->findById($view->id,false,true,true,true);
                $viewedItems->post_action_id = hashid_encode($view->post_action_id);
                if($viewedItems != NULL){
                    $data['viewed'][] = $viewedItems;    
                }
            }
        }
        $posts['data'] = $data;
        $posts['post_type'] = 'feed';
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function viewedFeeds($pagination = false,$perPage = 10, $data = [],$encode=false){

        $follower_id = $data['follower_id']; 
        $post_id = $data['last_post_id'];
        $param['view'] = "view";
        $param['user_id'] = $follower_id;
        $feeds = $this->builder->Join("post_actions","posts.id","=","post_actions.post_id")->whereRaw("(posts.user_id IN (SELECT user_id FROM `followers` WHERE `follower_id` = ".$follower_id." AND STATUS = 'followed') AND privacy = 'public' OR ( posts.id IN (SELECT post_id FROM `post_shares` WHERE `follower_id` = ".$follower_id."))) AND post_actions.user_id = ".$follower_id." AND post_actions.`type` = 'view' AND post_actions.`id` < ".$post_id."")->orderBy("post_actions.id","DESC")->limit($perPage)->get(['posts.id as id','post_actions.id as post_action_id']);
        
        $data = [];
        if($feeds){
            foreach ($feeds as $key => $feed) {
                
                    $viewed = $this->findById($feed->id,false,true,true,true);
                    $viewed->post_action_id = hashid_encode($feed->post_action_id);
                    if($viewed != NULL){
                        $data[] = $viewed;    
                    }
            }
        }
        $posts['data'] = $data;
        $posts['post_type'] = 'feed';
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function recentFeeds($pagination = false,$perPage = 10, $data = [],$encode=false){

        $follower_id = $data['follower_id']; 
        $post_id = $data['last_post_id'];
        $param['view'] = "view";
        $param['user_id'] = $follower_id;
        $feeds = $this->builder->whereRaw("(posts.user_id IN (SELECT user_id FROM `followers` WHERE `follower_id` = ".$follower_id." AND STATUS = 'followed') AND privacy = 'public' OR (posts.id IN (SELECT post_id FROM `post_shares` WHERE `follower_id` = ".$follower_id."))) AND posts.id NOT IN (SELECT post_id FROM `post_actions` WHERE user_id = ".$follower_id." AND `type` = 'view') AND posts.id < ".$post_id)->orderBy("posts.id","desc")->limit($perPage)->get(['posts.id']);
        $data = [];
        if($feeds){
            foreach ($feeds as $key => $feed) {
                
                    $viewed = $this->findById($feed->id,false,true,true,true);
                    if($viewed != NULL){
                        $data[] = $viewed;    
                    }
            }
        }
        $posts['data'] = $data;
        $posts['post_type'] = 'feed';
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function postSearch($pagination = false,$perPage = 10, $data = []){

     
        $this->builder = $this->model
        ->where(function($query) use($data) {
            $query->where('title', 'LIKE', "%{$data['keyword']}%");
            $query->orWhere('description', 'LIKE', "%{$data['keyword']}%");
        })
        ->leftJoin('post_shares', function($join) use($data){
            $join->on('posts.id', '=', 'post_shares.post_id')->where('post_shares.user_id','=',$data['user_id']);
        })
        ->where(function($query) {
            $query->where('posts.privacy', '=','public');
            $query->orWhere('post_shares.id','!=', NULL);
        })
        ->select('posts.id');

        if($data['only_favourites']){
            $this->builder = $this->builder->join('post_actions', 'posts.id', '=', 'post_actions.post_id')
                ->where('post_actions.type', '=', 'like')
                ->where('post_actions.user_id', '=', $data['user_id']);
        } 
        
        return parent::findByAll($pagination, $perPage);
    }
    
    public function postSearchByGenre($pagination = false,$perPage = 10, $data = []){
    
        $this->builder = $this->model
        ->join('post_genres', function($join) {
            $join->on('posts.id', '=', 'post_genres.post_id');
        })
        ->join('genres', function($join) use($data){
            $join->on('post_genres.genre_id', '=', 'genres.id');
        })
        ->join('users', function($join) {
            $join->on('posts.user_id', '=', 'users.id');
        })
        ->where('post_genres.genre_id', '=', $data['genre_id'])
        ->where('genres.visibility', '=', $data['visibility'])
        ->where('users.signup_via', '=', $data['signup_via'])
        ->where('users.username', 'LIKE', "%{$data['keyword']}%")
        ->where('users.visibility', '=', 1)
        ->select('posts.id');

        return parent::findByAll($pagination, $perPage);
    }

    public function recentPosts($userid, $limit){
        $recentPosts = $this->model->where('user_id', '=', $userid)->orderBy('created_at', 'desc')->limit($limit)->get(['id']);
        $data=[];
        $i = 0;
        foreach ($recentPosts as $key => $post) {
            $data[$i] = $this->findById($post->id);
            $i++;
        }
        return $data;
    }

    public function userPosts($pagination = false,$perPage = 10, $data = [],$encode=false){
        $query = $this->builder->where('posts.user_id','=',$data['user_id'])->where('posts.post_type','=',$data['post_type']);
        $this->builder = $query;
        $posts = parent::findByAll($pagination,$perPage);
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function findByAll($pagination = false,$perPage = 10, $data = []){

        $creator = [];
        $posts = $this->model;
        // filter by username
        if( isset($data['filter_by_title']) && $data['filter_by_title'] != ""){
            //$posts = $posts->join('users',"posts.user_id","=","users.id")->where('users.fullname','LIKE', "%".$data['filter_by_title']."%");
            $posts = $posts->where('title', 'LIKE', "%".$data['filter_by_title']."%");
        }
        // filter by type
        if( isset($data['filter_by_type']) && $data['filter_by_type'] != ""){
            $posts = $posts->where('type', '=', $data['filter_by_type']);
        }
        // filter by status
        if( isset($data['filter_by_status']) && $data['filter_by_status'] != ""){
            $posts = $posts->where('privacy', '=', $data['filter_by_status']);
        }

        // user related posts
        if( isset($data['filter_by_user_id']) && $data['filter_by_user_id'] != ""){
            $user_id = hashid_decode($data['filter_by_user_id']);
            $posts = $posts->where('user_id', '=', $user_id);
            if(isset($data['device']) && !empty($data['device'])){
                $posts = $posts->whereRaw('privacy = "public" AND (share_to_all = 1 OR share_to_all = 0) AND id NOT IN (SELECT posts.id from posts join post_shares on posts.id = post_shares.post_id where posts.privacy = "public" AND posts.user_id ='.$data['user_id'].' AND posts.share_to_all = 0 group by posts.id)');
            }
            $userRepo = App::make('UserRepository');
            $creator = $userRepo->findById($user_id,false,false,false,false);
        }
        // genre related posts
        if( isset($data['filter_by_genre_id']) && $data['filter_by_genre_id'] != ""){
            // dd(hashid_encode(1039));
            $genre_id = hashid_decode($data['filter_by_genre_id']);
            $posts = $posts->join('post_genres',"posts.id","=","post_genres.post_id")->where('post_genres.genre_id','=', $genre_id)->select('posts.id');

            if(isset($data['device']) && ($data['device'] == 'ios' || $data['device'] == 'android')){
                $posts = $posts->where("posts.privacy","=","public");
                if(isset($data['last_post_id']) && $data['last_post_id'] != NULL){
                    $posts = $posts->where('posts.id', '<', hashid_decode($data['last_post_id']))->orderBy('posts.id', 'desc')->limit(10)->get(['posts.id']);
                        $data = ['data'=>[]];
                        if ($posts) {
                            foreach ($posts as $post) {
                                $model = $this->findById($post->id,false,true,true);
                                
                                if ($model) {

                                    unset($model->recover_password_key);
                                    unset($model->recover_attempt_at);

                                    $data['data'][] = $model;
                                }
                            }
                        }
                }
                return $data;
            }
        }

        $posts = $posts->orderBy('posts.id', 'desc');
        $this->builder = $posts;
        $posts = parent::findByAll($pagination,$perPage,[],true);
        $posts["creator"] = $creator;
        
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function directList($pagination = false,$perPage = 10, $data = []){

        $posts = $this->model;
        // genre related posts
            $posts = $posts->whereRaw('(privacy = "private" AND user_id ='.$data['user_id'].') OR id IN (SELECT posts.id from posts join post_shares on posts.id = post_shares.post_id where posts.privacy = "public" AND posts.user_id ='.$data['user_id'].' AND posts.share_to_all = 0 group by posts.id)')
            ->orderBy('posts.created_at', 'desc');
        
        $this->builder = $posts;
        $posts = parent::findByAll($pagination,$perPage,[],true);
        
        if($posts != NULL){
            return $posts;
        }else{
            return NULL;
        }
    }

    public function postShare(array $data = []){

        $rows = [];
        if(isset($data['share_to_all']) && $data['share_to_all'] == 1){
            if(isset($data['share_except']) && is_array($data['share_except'])){
                foreach ($data['share_except'] as $keyId => $ids) {
                   $data['share_except'][$keyId] = hashid_decode($ids);
                }
                $data['share_with'] = User::join('followers','followers.follower_id','=','users.id')->where('users.visibility', '=', 1)
                    ->where('followers.user_id','=',$data['user_id'])->whereNotIn('users.id', $data['share_except'])->groupBy('users.id')->get(['users.id as id']) ;
            }else{
                $data['share_with'] = User::join('followers','followers.follower_id','=','users.id')->where('users.visibility', '=', 1)
                    ->where('followers.user_id','=',$data['user_id'])->groupBy('users.id')->get(['users.id as id']) ;
            }
                
        }

        
        
        if( isset($data['share_with']) && count($data['share_with']) > 0 ){
            $users=[];
            foreach ($data['share_with'] as $key => $value) {
                if(isset($data['share_to_all']) && $data['share_to_all'] == 1){
                    $followersId = $value->id;
                }else{
                    $followersId = hashid_decode($value);
                }
                $users[]= $followersId;
                $rows[] = array('user_id'=>$data['user_id'], 'post_id'=>$data['post_id'], 'follower_id'=> $followersId, 'share_to_all'=> $data['share_to_all'], 'created_at'=>Carbon::now(),'updated_at'=>Carbon::now());
            }
        }  
        $postShare = false;
        if($rows != NULL){
            $postShare = $this->model_post_share->insert($rows);
            $post = $this->findById($data['post_id']);
            if($post){
                $post->share_user_id = $data['user_id'];
                event(new PostShareNotificationWasGenerated($post,$users));
            }else{
                return false;
            }
            
        }
        return $postShare;
    }

    public function PostFollowerList(array $data = []){

        $sql = 'SELECT 
                users.*
                ,`post_shares`.post_id  
                ,IF ( (IFNULL(`post_shares`.`follower_id`, 0))>0 , 1,0 ) AS `is_shared`
                FROM `users`
                INNER JOIN followers ON followers.`user_id` = users.id
                LEFT JOIN `post_shares`  ON users.id = `post_shares`.follower_id 
                AND `post_shares`.`post_id` = ? 
                AND `post_shares`.`deleted_at` IS NULL 
                WHERE  followers.`follower_id` = ? AND
                `users`.deleted_at IS NULL 
                ORDER BY users.`id` ASC';
        $users  = DB::select($sql, [$data['post_id'],$data['user_id']]);
        foreach ($users as $key => $value) {
            $users[$key]->id = hashid_encode($value->id);
            $users[$key]->post_id = hashid_encode($value->post_id);
            if (strpos($value->profile_pic, '.') !== false) {
                list($file, $extension) = explode('.', $value->profile_pic);
                $file = config('app.files.users.folder_name')."/".$file;
                $value->image_urls['1x'] = Helper::getFileUrl($file.'.'.$extension);
                $value->image_urls['2x'] = Helper::getFileUrl($file.'@2x.'.$extension);
                $value->image_urls['3x'] = Helper::getFileUrl($file.'@3x.'.$extension);
            } else {
                $value->image_urls = new \stdClass;
            }
            unset($users[$key]->password,  $users[$key]->recover_password_key,$users[$key]->recover_attempt_at, $users[$key]->updated_at, $users[$key]->deleted_at, $users[$key]->role_id,$users[$key]->created_at,$users[$key]->access_token);
        }
        if ($users != NULL) {
            return $users;
        } else {
            return NULL;
        }
    }

    public function userFavoritePosts($pagination = false,$perPage = 10, $data = [],$encode=false){

        $query = $this->builder->join("post_actions","post_actions.post_id","=","posts.id")->where("post_actions.user_id","=",$data['id'])->where("post_actions.type","=","like")->orderBy('post_actions.created_at', 'desc')->select(['posts.id']);
        if(isset($data['keyword'])){
            $this->builder = $query;
            $query = $this->builder->where('posts.title', 'LIKE', "%{$data['keyword']}%");
        }

        $this->builder = $query;
        $posts = parent::findByAll($pagination,$perPage);
        if($posts["data"] != NULL){
            return $posts;
        } else{
            return NULL;
        }
    }

    public function thumb() {
        $data = $this->model->where('deleted_at','=',NULL)->get(['path','type']);
        $arr = [];
        if(count($data)){            
            foreach ($data as $key => $value) {
                if($value->type == "photo"){
                    if (strpos($value->path, '.') !== false) {
                        list($file, $extension) = explode('.', $value->path);
                        $file = config('app.files.posts.public_relative').$file;
                    try{

                        $contents = Storage::get($file.'@thumb.'.$extension);
                    
                    }
                    catch(\Exception $e){
                       $thumb = $this->storeThumb($file.'@3x.'.$extension);
                    }
                    }
               }
            }
        }

        return $data;
    }

    public function storeThumb($file) {
     
        $videoName = $file;
        list($thumbnailName, $ext) = explode('@3x.', $videoName);
        $fileThumb = Storage::get($file);
        $image = Image::make($fileThumb);
        $width = $image->width();
        $height = $image->height();
        $thumbStore = Storage::put(config('app.files.posts.full_path/').$thumbnailName.'@thumb.png', $image->resize("510", "240")->stream());
        

        return true;
    }
}
