<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Data\Models\UserCode;
use konnect\Data\Models\Role;
use konnect\Data\Models\UserRole;
use konnect\Data\Models\Follower;
use konnect\Data\Models\Post;
use konnect\Data\Models\PostAction;
use konnect\Data\Models\PostShare;
use konnect\Data\Repositories\PostRepository;
use Illuminate\Support\Facades\Cache;
use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Support\Helper;
use \App;

class DashboardRepository {

	public function __construct(User $user, Post $post, PostAction $postAction, PostShare $postShare){
		
		$this->user_model = $user;
		$this->post_model = $post;
		$this->post_action_model = $postAction;
		$this->post_share_model = $postShare;
	}

	public function dashboard($input) {
		
		$data = [];
		$user = $this->user_model;
		$content = $this->post_model;
		$postActionLike = $this->post_action_model;
		$postActionView = $this->post_action_model;
		$postShare = $this->post_share_model;
		$startDate = '';
		$endDate = '';
		$contentWhere = '';
		$whereClause = '';

    	if (isset($input['start_date']) && $input['start_date'] != '' && isset($input['end_date']) && $input['end_date'] != '') {
			
			$input['start_date'] = Carbon::parse($input['start_date']);//date('Y-m-d', strtotime($input['start_date']));
			$input['end_date']   = Carbon::parse($input['end_date'])->addDay()->subSecond();//date('Y-m-d', strtotime($input['end_date']));

			$startDate           = $input['start_date'];
			$endDate             = $input['end_date'];
			$whereClause = " and date(created_at) between ? and ?";
			$postActionLike = $postActionLike->whereBetween('created_at', [$startDate, $endDate]);
			$postActionView = $postActionView->whereBetween('created_at', [$startDate, $endDate]);
			$postShare = $postShare->whereBetween('created_at', [$startDate, $endDate]);
			$contentWhere = 'where date(up.created_at) between ? and ?';
		}

		// users data
		if($whereClause == ""){
			$sqlTotalUser = "select count(*) as total from users ";
		} else {
			$sqlTotalUser = "select count(*) as total from users where 1=1 ".$whereClause;
		}
		
		$totalUser = \DB::select($sqlTotalUser,[$startDate, $endDate]);
		$data['users']['total_users'] = $totalUser[0]->total;

		$sqlTotalfacebookUser = "select count(*) as total from users where signup_via = 'facebook'  ".$whereClause;
		$totalFacebookUser = \DB::select($sqlTotalfacebookUser,[$startDate, $endDate]);
		$data['users']['facebook_users'] = $totalFacebookUser[0]->total;
		if($data['users']['facebook_users'] != 0 && $data['users']['total_users'] != 0){
			$data['users']['facebook_users_percentage'] = $data['users']['facebook_users'] / $data['users']['total_users'] * 100;
			$data['users']['facebook_users_percentage'] = number_format($data['users']['facebook_users_percentage'],2);
		} else {
			$data['users']['facebook_users_percentage'] = 0;
		}

		$sqlTotalemailUser = "select count(*) as total from users where signup_via = 'email' ".$whereClause;
		$totalEmailUser = \DB::select($sqlTotalemailUser,[$startDate, $endDate]);
		$data['users']['email_users'] = $totalEmailUser[0]->total;
		if($data['users']['email_users'] != 0 && $data['users']['total_users'] != 0){
			$data['users']['email_users_percentage'] = $data['users']['email_users'] / $data['users']['total_users']*100;
			$data['users']['email_users_percentage'] = number_format($data['users']['email_users_percentage'],2);
		} else {
			$data['users']['email_users_percentage'] = 0;
		}
		
		// content data
		if($whereClause == ""){
			$sqlTotalUser = "select count(*) as total from posts ";
		} else {
			$sqlTotalUser = "select count(*) as total from posts where 1=1 ".$whereClause;
		}
		
		$totalContent = \DB::select($sqlTotalUser,[$startDate, $endDate]);
		$data['content']['total_content'] = $totalContent[0]->total;

		$sqlTotalPhotos = "select count(*) as total from posts where type = 'photo'  ".$whereClause;
		$totalPhotos = \DB::select($sqlTotalPhotos,[$startDate, $endDate]);
		$data['content']['photos'] = $totalPhotos[0]->total;
		if($data['content']['photos'] != 0 && $data['content']['total_content'] != 0){
			$data['content']['photos_percentage'] = $data['content']['photos']/$data['content']['total_content'] * 100;
			$data['content']['photos_percentage'] = number_format($data['content']['photos_percentage']);
		} else {
			$data['content']['photos_percentage'] = 0;
		}

		$sqlTotalVideos = "select count(*) as total from posts where type = 'video' ".$whereClause;
		$totalVideos = \DB::select($sqlTotalVideos,[$startDate, $endDate]);
		$data['content']['videos'] = $totalVideos[0]->total;
		if( $data['content']['videos'] != 0 && $data['content']['total_content'] != 0 ){
			$data['content']['videos_percentage'] = $data['content']['videos'] / $data['content']['total_content'] * 100;
			$data['content']['videos_percentage'] = number_format($data['content']['videos_percentage']);
		} else {
			$data['content']['videos_percentage'] = 0;
		}

		$sqlContent = 'SELECT AVG(p.postcount) as post_count FROM (SELECT COUNT(*) AS postcount FROM posts up '.$contentWhere.' GROUP BY up.user_id) p ';
		$postAverage = \DB::select($sqlContent,[$startDate, $endDate]);
		
		$data['content']['avg_content_per_user'] = number_format($postAverage[0]->post_count,2);
		// content actions
		$data['content_actions']['total_likes'] = $postActionLike->where('type', '=', 'like')->count();
		$data['content_actions']['total_views'] = $postActionView->where('type', '=', 'view')->count();
		$data['content_actions']['total_shares'] = $postShare->count();
    	return $data;
    }


}