<?php

namespace konnect\Data\Repositories;

use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Data\Models\Activity;
use konnect\Data\Repositories\UserRepository;
use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Support\Helper;
use Storage, Image, DB;
use \App;

class ActivityRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;

    /**
     *
     * This is the prefix of the cache key to which the 
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'activity-';

    public function __construct(Activity $activity) {
        $this->model = $activity;

    }

    public function getNotificationList($pagination = false,$perPage = 10, $data = []){
        
        // $this->activityViewUpdate($data);
        $activity = $this->model
                    ->where('activities.user_id', '=', $data['user_id'])->where('activities.visibility', '=', 1)
                    ->select(['activities.id', 'activities.user_id','activities.actor_id','activities.action','activities.object','activities.created_at']);

        $activity = $activity->orderBy('activities.created_at', 'desc');
        $this->builder = $activity;
        $activity = parent::findByAll($pagination,$perPage,[],true);
        
        // repository providers
        $userRepo = App::make('UserRepository');
        $postRepo = App::make('PostRepository');
        $commentRepo = App::make('CommentRepository');
        
        $activityNotifications = [];
        $activityNotifications['data'] = [];

        $i = 0;

        if( count($activity) > 0 ){
            $notification = [];
            foreach ($activity["data"] as $key => $activityLog) {
                if( $activityLog->action == "follow_request" ){
                    $notification['user'] = $userRepo->findById($activityLog->actor_id);
                } else {
                    $notification['user'] = $userRepo->findById($activityLog->actor_id);
                    if( $activityLog->action == "comment" ){
                        $notification['post'] = $postRepo->findById($activityLog->object_id);
                        $notification['comment'] = $commentRepo->findById($activityLog->action_id);
                    } else {
                        $notification['post'] = $postRepo->findById($activityLog->object_id);
                    }
                }
                
                $activityLog->user_id = hashid_encode($activityLog->user_id);
                $activityLog->object_id = hashid_encode($activityLog->object_id);
                $activityLog->action_id = hashid_encode($activityLog->action_id);
                $activityLog->actor_id = hashid_encode($activityLog->actor_id);
                $notification['activity'] = $activityLog;
                
                if($notification != ""){
                    $activityNotifications["data"][$i] = $notification;
                    $i++;
                }
                
            }
        }
        $activityNotifications["pagination"] = $activity["pagination"];
        if($activityNotifications != NULL){
            return $activityNotifications;
        }else{
            return NULL;
        }
    }
    
    public function activityNotificationView(array $data = []){

        $updateNotificationView = $this->model->where('id', '=', $data['id'])
                    ->update(['viewed' => 1]);
                    Cache::forget($this->_cacheKey.$data['id']);
        if( $updateNotificationView ){
            return true;
        } else {
            return false;
        }
    }
    public function activityVisibilityUpdate($followerRequest,$type="follow_request"){

        $updatevisibility = $this->model->where('action_id', '=', $followerRequest->id)
                                        ->where('object_id', '=', $followerRequest->id)
                                        ->where('user_id', '=', $followerRequest->user_id)
                                        ->where('actor_id', '=', $followerRequest->follower_id)
                                        ->where('action', '=', $type)
                                        ->update(['visibility' => 0]);
        
        if($updatevisibility){
            return true;
        } else {
            return false;
        }
    }
    
    public function activityCountList(array $data = []){

        $countArr = [];
        $countArr['data'] = []; 
        $countArr['data']['totalCount'] = $this->model->where('viewed', '=', 0)
                                        ->where('user_id', '=', $data['user_id'])
                                        ->where('visibility', '=', 1)
                                        ->count();

         $countArr['data']['postCount'] = $this->model->where('viewed', '=', 0)
                                        ->where('user_id', '=', $data['user_id'])
                                        ->where('visibility', '=', 1)
                                        ->where('action', '=', 'share')
                                        ->count();
        return $countArr;
    }

    public function activityViewUpdate(array $data = []){

        $updateView = $this->model->where('user_id', '=', $data['user_id'])
                                                ->update(['viewed' => 1]);
        
        if( $updateView ){
            return true;
        } else {
            return false;
        }
    }
}
