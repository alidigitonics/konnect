<?php

namespace konnect\Data\Repositories;

use Illuminate\Support\Facades\Event;
use konnect\Data\Contracts\RepositoryContract;
use konnect\User;
use konnect\Data\Models\BusinessLocation;
use konnect\Events\FriendNotificationWasGenerated;
use konnect\Data\Models\UserDetail;
use konnect\Data\Models\HashTag;
use konnect\Data\Models\UserHashTag;
use konnect\Data\Models\UserSocialAccount;
use Illuminate\Support\Facades\Cache;
use konnect\Events\InviteWasCreated;
use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use konnect\Support\Helper;
use Storage, Image;
use \App;
use Tymon\JWTAuth\Payload;
use Validator;


class UserDetailRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    public $model_business_location;

    /**
     *
     * This is the prefix of the cache key to which the 
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'user-detail-';
    protected $_cacheTotalKey = 'total-user-details';
    protected $_cacheRolesKey = 'user-detail-role';

    public function __construct(UserDetail $userDetail) {
        $this->builder = $userDetail;
        $this->model = $userDetail;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            
            if($encode){
                $data->user_id = hashid_encode($data->user_id);
            }else{
                $data->user_id = $data->user_id;
            }
        }

        return $data;
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){

        $users = $this->builder->where('user_id',$data['user_id']);
        
        if( isset($data['filter_by_name']) && $data['filter_by_name'] != ""){
            $users = $users->whereRaw('(field_title LIKE "%'.$data['filter_by_name'].'%")');
        }
        if( isset($data['filter_by_value']) && $data['filter_by_value'] != ""){
            $users = $users->where('field_value', '=', $data['filter_by_value']);
        }
              
        $this->builder = $users;
        $order = $this->builder->orderBy("created_at","desc");
        $this->builder = $order;
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }

    public function addUserDetail(array $data = []){
        
        $userId = $data['user_id'];
        unset($data['user_id']);
        $userDetailData = [];
        foreach ($data as $key => $value) {
            $fieldIdData['field_title'] =  $value['field_title'];
            $fieldIdData['field_value'] =  implode(",",$value['field_value']);
            $fieldIdData['user_id'] =  $userId;
            $userDetailsData = parent::create($fieldIdData,true,false);
            $userDetailDataId = ($userDetailsData->id); 
            foreach ($value['field_value'] as $keyFieldValue => $FieldValue) {
                $HashTagData = HashTag::where('hash_tag','=',$FieldValue)->first();
                if($HashTagData == NULL){
                    $getInsertedIdHashTag = HashTag::insertGetId(['hash_tag'=>$FieldValue]);
                    $hasTagIds = $getInsertedIdHashTag;
                }else{
                    $hasTagIds = $HashTagData->id; 
                }
                $getDataOFUserHashTag['user_id'] = $userId;
                $getDataOFUserHashTag['hash_tag_id'] = $hasTagIds;
                $getDataOFUserHashTag['field_id'] = $userDetailDataId;
                $UserHashTag = UserHashTag::insertGetId($getDataOFUserHashTag);

            }
            $returnData[] = $userDetailsData;
        }
        if (count($returnData) > 0) {
            return $returnData;
        }

        return false;
    }

    public function userDetailList($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){
       $users = UserDetail::where('user_id',$data['user_id']);
        
        if( isset($data['filter_by_name']) && $data['filter_by_name'] != ""){
            $users = $users->whereRaw('(field_title LIKE "%'.$data['filter_by_name'].'%")');
        }
        if( isset($data['filter_by_value']) && $data['filter_by_value'] != ""){
            $users = $users->where('field_value', '=', $data['filter_by_value']);
        }
              
        
        $this->builder = $users;
        $order = $this->builder->orderBy("users.created_at","desc");
        $this->builder = $order;
        // dd($this->builder->toSql());
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }

}