<?php
namespace konnect\Listeners;

use konnect\Events\LikeNotificationWasGenerated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\Activity;
use konnect\Data\Models\PostShare;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class LikeNotificationWasGeneratedConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(LikeNotificationWasGenerated $event)
    {

        $post = $event->post;
        // dd($post);
        // $post->id = hashid_encode($post->id);
        $postLike = $event->postlike;
        if($post->user_id != hashid_encode($postLike->user_id)){
            $user = User::find($postLike->user_id);
            $postTitle = $user->fullname.' like a' .$post->type.' you  shared.';
            if($post->type == "photo"){
                $view = Helper::PHOTO_LIKED;
            }else{
                $view = Helper::VIDEO_LIKED;
            }

            $optionBuiler = new OptionsBuilder();
            $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

            $notificationBuilder = new PayloadNotificationBuilder($postTitle);
            $notificationBuilder->setTitle($postTitle)
                                ->setSound(1)
                                ->setClickAction('MY_KEY_NOTIFICATION')
                                ->setBody($postTitle);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['custom' => [
                                        'view' => $view,
                                        'view_id' => ($post->id),
                                        'type'=>'like'
                        ]]);
             $dataBuilder->setData(['custom' => [
                                        'view' => $view,
                                        'view_id' => ($post->id),
                                        'type'=>'like'
                        ]]);

            $option = $optionBuiler->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            $deviceToken = DeviceToken::where("user_id","=",hashid_decode($post->user_id))->get(['token']);
            if( isset($deviceToken)){
                $token = [];
                foreach ($deviceToken as $key => $value) {
                    $token[] = $value->token;
                }
                
                $activity = new Activity();
                $activity->user_id = hashid_decode($post->user_id); 
                $activity->action  = 'like';
                $activity->object  = $post->type;
                $activity->action_id = hashid_decode($postLike->id);
                $activity->object_id = hashid_decode($post->id);
                $activity->actor_id = $postLike->user_id;
                $activity->save();
                if(count($token) > 0){
                    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                    $success = $downstreamResponse->numberSuccess();
                    return $success;
                }else{
                    return false;
                }

            } else {
                return false;
            }
            
            
        }else{
            return true;
        }
        
    }

    public function queue($queue, $command, $data) {
       $queueName = 'tree_like_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
