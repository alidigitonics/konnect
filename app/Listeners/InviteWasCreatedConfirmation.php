<?php
namespace konnect\Listeners;

use konnect\Events\InviteWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use konnect\Data\Repositories\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use Carbon\Carbon;
use \App;

class InviteWasCreatedConfirmation
{
    public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  InviteWascreated  $event
     * @return void
     */
    public function handle(InviteWasCreated $event)
    {
        $user = $event->user;
        
        $this->mailer->send('emails.user.invitation', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email)->subject('Invitation');
        });
    }
}
