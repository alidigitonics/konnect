<?php
namespace konnect\Listeners;

use konnect\Events\SendAdminActivation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use Carbon\Carbon;

class SendAdminActivationConfirmation
{
    public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  PasswordWasRecovered  $event
     * @return void
     */
    public function handle(SendAdminActivation $event)
    {
        $user = $event->user;
        $date = Carbon::now()->format('F j, Y');
        $user->date = $date;
        $this->mailer->send('emails.user.admin-activate', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email)->subject('Recover Password');
        });
    }
}
