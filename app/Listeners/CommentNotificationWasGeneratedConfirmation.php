<?php
namespace konnect\Listeners;

use konnect\Events\CommentNotificationWasGenerated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\Activity;
use konnect\Data\Models\PostShare;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class CommentNotificationWasGeneratedConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(CommentNotificationWasGenerated $event)
    {
        $post = $event->post;
        if($post->user_id !=  hashid_encode($event->userCommented->user_id)){
            $user = User::find($post->user_commneted);
            $postTitle = $user->fullname.' posted a comment on your '.$post->type.'.';
            if($post->type == "photo"){
                $view = Helper::PHOTO_COMMENT;
            }else{
                $view = Helper::VIDEO_COMMENT;
            }
            $optionBuiler = new OptionsBuilder();
            $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

            $notificationBuilder = new PayloadNotificationBuilder($postTitle);
            $notificationBuilder->setTitle($postTitle)
                                ->setSound(1)
                                ->setClickAction('MY_KEY_NOTIFICATION')
                                ->setBody($postTitle);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['custom' => [
                                        'view' => $view,
                                        'view_id' => $post->id,
                                        'type'=>'comment'
                        ]]);
            $dataBuilder->setData(['custom' => [
                                        'view' => $view,
                                        'view_id' => $post->id,
                                        'type'=>'comment'
                        ]]);

            $option = $optionBuiler->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            $deviceToken = DeviceToken::where("user_id","=",hashid_decode($post->user_id))->get(['token']);
            if( isset($deviceToken)){
                $token = [];
                foreach ($deviceToken as $key => $value) {
                    $token[] = $value->token;
                }
                $activity = new Activity();
                $activity->user_id = hashid_decode($post->user_id); 
                $activity->action  = 'comment';
                $activity->object  =  $post->type;
                $activity->action_id = $event->userCommented->id;
                $activity->object_id = hashid_decode($post->id);
                $activity->actor_id = $event->userCommented->user_id;
                $activity->save();
                if(count($token) > 0){
                    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                    $success = $downstreamResponse->numberSuccess();
                    return $success;
                }else{
                    return false;
                }
                
            } else {
                return false;
            }
            
            
        }else{
            return true;
        }
        
    }

    public function queue($queue, $command, $data) {
       $queueName = 'tree_comment_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
