<?php
namespace konnect\Listeners;

use konnect\Events\SendPostNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\PostShare;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class SendPostNotificationConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(SendPostNotification $event)
    {
        $post = $event->post;
        $post->user_id = hashid_decode($post->user_id);
        $user = User::find($post->user_id);
        $postTitle = $user->fullname.' Posted a '.$post->type;
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);
        if($post->type == "photo"){
            $view = Helper::PHOTO_CREATED;
        }else{
            $view = Helper::VIDEO_CREATED;
        }
        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound(1)
                            ->setClickAction('MY_KEY_NOTIFICATION')
                            ->setBody($postTitle);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($post->post_id),
                                    'type'=>'share'
                    ]]);
        $dataBuilder->setData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($post->post_id),
                                    'type'=>'share'
                    ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        // $deviceToken = DeviceToken::where("user_id","=",$post->user_id)->get(['token']);
        $post->id = hashid_decode($post->id);
        $deviceToken = DeviceToken::join("post_shares","device_token.user_id","=","post_shares.follower_id")->where("post_shares.post_id","=",$post->id)->get(['token']);

        foreach ($deviceToken as $value) {
            $token[] = $value->token;
        }
        if(count($token) > 0){
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $success = $downstreamResponse->numberSuccess();
            return $success;    
        }else{
            return false;
        }
        
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        // $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();
        // $name = $user->username;
        // $this->mailer->send('emails.user.activation', ['user' => $user], function ($m) use ($user,$name) {
        //     $m->to($user->email, $name)->subject('Activation');
        // });
    }
    public function queue($queue, $command, $data) {
       $queueName = 'tree_post_create_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
