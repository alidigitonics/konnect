<?php
namespace konnect\Listeners;

use konnect\Events\PrivateFollowerNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\Activity;
use konnect\Data\Models\PostShare;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PrivateFollowerNotificationConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(PrivateFollowerNotification $event)
    {
        $follower = $event->follower;
        
        $user       = User::find($follower->follower_id);
        $followerTitle = $user->fullname.' has requested to follow you.';
        $view = Helper::FOLLOWER_REQUEST;
        
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($followerTitle);
        $notificationBuilder->setTitle($followerTitle)
                            ->setSound(1)
                            ->setClickAction('MY_KEY_NOTIFICATION')
                            ->setBody($followerTitle);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($follower->id),
                                    'type'=>'follower'
                    ]]);
        $dataBuilder->setData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($follower->id),
                                    'type'=>'follower'
                    ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $deviceToken = DeviceToken::where("user_id","=",$follower->user_id)->get(['token']);
        if( isset($deviceToken)){
            $token = [];
             foreach ($deviceToken as $key => $value) {
                $token[] = $value->token;
            }
            try {
                if($token){
                    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                    $success = $downstreamResponse->numberSuccess();
                    return $success;
                }

            } catch (\Exception $e) {
                return false;                
            }
            return true;
        } else {
            return false;
        }
    }

    public function queue($queue, $command, $data) {
       $queueName = 'tree_follower_private_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
