<?php
namespace konnect\Listeners;

use konnect\Events\PostShareNotificationWasGenerated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\Activity;
use konnect\Data\Models\PostShare;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PostShareNotificationWasGeneratedConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(PostShareNotificationWasGenerated $event)
    {
        $post = $event->post;
        $users = $event->users; 
        $success = false;
        $post->user_id = hashid_decode($post->user_id);
        $user = User::find($post->share_user_id);
        $postUser = User::find($post->user_id);
        $postTitle = $user->fullname." has Shared ".$postUser->fullname."'s ".$post->type." with you. ";
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);
        if($post->type == "photo"){
            $view = Helper::PHOTO_CREATED;
        }else{
            $view = Helper::VIDEO_CREATED;
        }
        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound(1)
                            ->setBody($postTitle)
                            ->setClickAction('MY_KEY_NOTIFICATION');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
                                    'view' => $view,
                                    'view_id' => $post->id,
                                    'type' => 'share',
                    ]]);
        $dataBuilder->setData(['custom' => [
                                    'view' => $view,
                                    'view_id' => $post->id,
                                    'type' => 'share',
                    ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $post->id = hashid_decode($post->id);

        $deviceToken = DeviceToken::whereIn('device_token.user_id', $users)->get(['token','user_id']);
        foreach ($users as $valueUser) {
              # code...
                $activity = new Activity();
                $activity->user_id = $valueUser;
                $activity->action  = 'share';
                $activity->object  = $post->type;
                $activity->action_id = $post->id;
                $activity->object_id = $post->id;
                $activity->actor_id = $post->share_user_id;
                $activity->save();
          }
        if(count($deviceToken) > 0){
            foreach ($deviceToken as $value) {
                $token[] = $value->token;
            }


            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $success = $downstreamResponse->numberSuccess();
        }
        if($success){
           return true;
        }else{
           return false;
        }
       
         
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        // $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();
        // $name = $user->username;
        // $this->mailer->send('emails.user.activation', ['user' => $user], function ($m) use ($user,$name) {
        //     $m->to($user->email, $name)->subject('Activation');
        // });
    }

    public function sendNotificationios($messageData,$servertoken){

        $servertokenArr=[];
       $servertokenArr[]=$servertoken;
   
        $registrationIds = $servertokenArr;//$_GET['device_id']
     
        /*$body['aps'] = array(
          'alert' => array(
              'title' => 'Message',
              'body' => $messageData,
           ),
        'badge'        => 1,
        'vibrate'     => 1,
        'sound'       => 1,
        'largeIcon'   => '',
        'smallIcon'   => '',
        'type'        => 'message'
        );*/
     
        $fields = array
        (
           'registration_ids'      => $registrationIds,  
           'priority'            => 'high',  
           'content_available'     => true,
           'data' => array(
                        'title'         => '',
                        'body'             => $messageData,
                        'click_action'     => 'MY_KEY_NOTIFICATION'
                          
                        ),
           'notification' => array(
                        'title'         =>  '',
                        'body'             => $messageData->alert_message,
                        'click_action'     => 'MY_KEY_NOTIFICATION',
                        'vibrate'         => 1,
                        'sound'           => 1,
                        ),
        );
     
      $headers = array
      (
          'Authorization: key=' . 'AIzaSyD1aC_Rk0c20oFHk3mutBa1uTuBtceIKaE',
          'Content-Type: application/json'
      );
       
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
      $result = curl_exec($ch );
      curl_close( $ch );
      //echo $result;
     
      return $result;
  }

public function queue($queue, $command, $data) {
       $queueName = 'tree_post_create_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
