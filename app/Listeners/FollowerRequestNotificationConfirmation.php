<?php
namespace konnect\Listeners;

use konnect\Events\FollowerRequestNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\Activity;
use konnect\Data\Models\PostShare;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FollowerRequestNotificationConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(FollowerRequestNotification $event)
    {
        $follower = $event->follower;
        $user       = User::find($follower->user_id);
        $followerUser = User::find($follower->follower_id);
        $followerTitle = $user->fullname.' has accepted your request.';
        $view = Helper::FOLLOWER_REQUEST;
        
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($followerTitle);
        $notificationBuilder->setTitle($followerTitle)
                            ->setSound(1)
                            ->setClickAction('MY_KEY_NOTIFICATION')
                            ->setBody($followerTitle);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($user->id),
                                    'user_id' => hashid_encode($followerUser->id),
                                    'type'=>'follower'
                    ]]);
        $dataBuilder->setData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($user->id),
                                    'user_id' => hashid_encode($followerUser->id),
                                    'type'=>'follower'
                    ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $deviceToken = DeviceToken::where("user_id","=",$follower->follower_id)->get(['token']);
        if(!empty($deviceToken)){
            
            $activityVisibility = Activity::where('visibility', '=', 1)->where('user_id', '=', $follower->user_id)->where('actor_id', '=', $follower->follower_id)->where('action', '=', 'follow_request')->update(['visibility' => 0]);
            
            $token = [];
            foreach ($deviceToken as $key => $value) {
                $token[] = $value->token;
            }
            $activity = new Activity();
            $activity->user_id      = $follower->follower_id;
            $activity->action       = 'follow_approved';
            $activity->action_id    = $follower->id;
            $activity->object       = 'follow';
            $activity->object_id    = $follower->id;
            $activity->actor_id     = $follower->user_id;
            $activity->save();

        
            try {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $success = $downstreamResponse->numberSuccess();
                return $success;
            } catch (\Exception $e) {
                return false;
            }
            return true;
        } else {
            return false;
        }
        
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        // $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();
        // $name = $user->username;
        // $this->mailer->send('emails.user.activation', ['user' => $user], function ($m) use ($user,$name) {
        //     $m->to($user->email, $name)->subject('Activation');
        // });
    }

    public function queue($queue, $command, $data) {
       $queueName = 'tree_follower_request_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
