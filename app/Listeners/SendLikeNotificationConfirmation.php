<?php
namespace konnect\Listeners;

use konnect\Events\SendLikeNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Data\Models\PostShare;
use konnect\Support\Helper;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class SendLikeNotificationConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(SendLikeNotification $event)
    {
        $post = $event->post_action;
        $user = User::find($event->userlikedId);
        $postTitle = $user->fullname.' liked on your '.$post->type;
        if($post->type == "photo"){
            $view = Helper::PHOTO_COMMENT;
        }else{
            $view = Helper::VIDEO_COMMENT;
        }
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);
        $notificationBuilder = new PayloadNotificationBuilder($postTitle);
        $notificationBuilder->setTitle($postTitle)
                            ->setSound(1)
                            ->setClickAction('MY_KEY_NOTIFICATION')
                            ->setBody($postTitle);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($post->post_id),
                                    'type'=>'like'
                    ]]);
        $dataBuilder->setData(['custom' => [
                                    'view' => $view,
                                    'view_id' => hashid_encode($post->post_id),
                                    'type'=>'like'
                    ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $deviceToken = DeviceToken::where("user_id","=",$post->user_id)->get(['token']);
        if( isset($deviceToken[0])){
            $token = $deviceToken[0]->token;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $success = $downstreamResponse->numberSuccess();
            return $success;
        } else {
            return false;
        }
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        // $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();
        // $name = $user->username;
        // $this->mailer->send('emails.user.activation', ['user' => $user], function ($m) use ($user,$name) {
        //     $m->to($user->email, $name)->subject('Activation');
        // });
    }

    public function queue($queue, $command, $data) {
       $queueName = 'tree_like_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
