<?php
namespace konnect\Listeners;

use konnect\Events\FriendRefererNotificationWasGenerated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use konnect\Data\Models\DeviceToken;
use konnect\Data\Models\User;
use konnect\Support\Helper;
use konnect\Data\Models\Activity;
use konnect\Data\Models\UserFriend;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FriendRefererNotificationWasGeneratedConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**FriendRefererNotificationWasGenerated
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(FriendRefererNotificationWasGenerated $event)
    {
        $friend = $event->friend;
        $user       = User::find($friend->user_id);
        $friendUser = User::find($friend->friend_id);
        if($friend->status == 'pending'){
            $friendTitle = $user->first_name.' '.$user->last_name.' has sent you a friend request.';
            $deviceToken = DeviceToken::where("user_id","=",$friend->friend_id)->get(['token']);
        }elseif($friend->status == 'friend'){
            $friendTitle = $friendUser->first_name.' '.$friendUser->last_name.' has accepted your friend request.';
            $deviceToken = DeviceToken::where("user_id","=",$friend->user_id)->get(['token']);
        }
        
        $view = Helper::FOLLOWER_REQUEST;
        
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder($friendTitle);
        $notificationBuilder->setTitle($friendTitle)
                            ->setSound('default')
                            ->setClickAction('MY_KEY_NOTIFICATION')->setBadge(1)
                            ->setBody($friendTitle);


        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['custom' => [
                                    'view' => $view,
                                    'view_id' => $friend->primary_id,
                                    'user_id' => hashid_encode($user->id),
                                    'friend_id' => hashid_encode($friendUser->id),
                                    'type'=>'friend'
                    ]]);
        $dataBuilder->setData(['custom' => [
                                    'view' => $view,
                                    'view_id' => $friend->primary_id,
                                    'user_id' => hashid_encode($user->id),
                                    'friend_id' => hashid_encode($friendUser->id),
                                    'type'=>'friend'
                    ]]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        if(!empty($deviceToken)){
            $token = [];
            foreach ($deviceToken as $key => $value) {
                $token[] = $value->token;
            }

            if($friend->status == 'pending'){
                $activity = new Activity();
                $activity->user_id      = $friend->friend_id;
                $activity->action       = 'friend_request';
                $activity->action_id    = $friend->id;
                $activity->object       = 'friend';
                $activity->object_id    = $friend->id;
                $activity->actor_id     = $friend->user_id;
                $activity->save();
            }else{
                $activity = new Activity();
                $activity->user_id      = $friend->user_id;
                $activity->action       = 'friend_approved';
                $activity->action_id    = $friend->id;
                $activity->object       = 'friend';
                $activity->object_id    = $friend->id;
                $activity->actor_id     = $friend->friend_id;
                $activity->save();
            }
            try {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $success = $downstreamResponse->numberSuccess();
                return $success;
            } catch (\Exception $e) {
                return false;
            }
            return true;
        } else {
            return false;
        }
        
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        // $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();
        // $name = $user->username;
        // $this->mailer->send('emails.user.activation', ['user' => $user], function ($m) use ($user,$name) {
        //     $m->to($user->email, $name)->subject('Activation');
        // });
    }

    public function queue($queue, $command, $data) {
       $queueName = 'tree_friend_request_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
