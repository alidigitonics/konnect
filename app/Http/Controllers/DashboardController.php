<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use konnect\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use konnect\Data\Repositories\DashboardRepository;
use konnect\Data\Models\Role;
use konnect\Data\Models\User;
use konnect\Data\Models\Follower;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;

class DashboardController extends Controller {

	private $_repository;

	public function __construct() {
		$this->_repository = app()->make('DashboardRepository');
	}

    /**
     *
     * This method will fetch all analytics for dashboard 
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Bushra Naz
     *
     **/
    public function dashboard(Request $request) {
   
        $input = $request->only('start_date','end_date');
        
        $rules = [
        ];

        $messages = [];

        $validator = Validator::make($input, $rules, $messages);
        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
            
        } else {
            // if validation passes
            $output = $this->_repository->dashboard($input);

        }
        
        return response()->json($output);
    }

    

}