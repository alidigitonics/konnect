<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use konnect\Data\Repositories\PostActionRepository;
use konnect\Data\Repositories\PostRepository;
use konnect\Events\LikeNotificationWasGenerated;
use konnect\Data\Models\PostAction;
use konnect\Data\Models\Post;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Hash, \App;


class PostActionController extends Controller
{
   	private $_repository;

   	public function __construct(PostActionRepository $post_action) {
        $this->_repository = $post_action;
    }

    public function postLike(Request $request) {

        $input = $request->only('post_id','like_type','extra');
        $input['post_id'] = hashid_decode($input['post_id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
        
        $rules = [
                    'post_id'    =>  'required|exists:posts,id,deleted_at,NULL',
                    'user_id'    =>  'required|exists:users,id,visibility,1',
                 ];

        $validator = Validator::make($input,$rules);

        if($validator->fails()) {

            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You are not able to perform this action you may be removed or disabled by admin' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        }
        else {
            $postLikeExist = PostAction::where('post_id', $input['post_id'])->where('user_id',$input['user_id'])->where('type','like')->first();
     
            if($postLikeExist == NULL) {
                
                $input['type'] = 'like';        
                $postLikeCreate = $this->_repository->create($input);
                if($postLikeCreate == NULL) {

                    $code = 400;
                    $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while trying to like this post.']]];

                } else {
                    
                    $postRepo = $postRepo = App::make('PostRepository');
                    $post = $postRepo->findById($postLikeCreate->post_id);
                    $code = 200;
                    $output = ['response' => ['code'=> $code, 'messages' => ['Post like will be saved for the user.'], 'data' => $post]];
                    Event::fire(new LikeNotificationWasGenerated($post,$postLikeCreate));
                }

            } else {
                $input['id'] = $postLikeExist->id;
                $input['type'] = 'like';   
                $postLikeCreate = $this->_repository->update($input);
                if($postLikeCreate == NULL) {

                    $code = 400;
                    $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while trying to like this post.']]];

                } else {
                    $code = 200;
                    $output = ['response' => ['code'=> $code, 'messages' => ['Post like will be saved for the user.'], 'data' => $postLikeCreate]];
                }
            }
        }

        return response()->json($output, $code);
    }

    public function postUnlike(Request $request) {

        $input = $request->only('post_id');
        $input['post_id'] = hashid_decode($input['post_id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
        
        $rules = [
                    'post_id'    =>  'required|exists:posts,id,deleted_at,NULL',
                    'user_id'    =>  'required|exists:users,id,visibility,1',
                 ];

        $validator = Validator::make($input,$rules);

        if($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You are not able to perform this action you may be removed or disabled by admin' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        }
        else {
            $postLikeExist = PostAction::where('post_id', $input['post_id'])->where('user_id',$input['user_id'])->where('type','like')->first();
     
            if($postLikeExist == NULL) {
                $code = 404;
                $output = ['error'=> ['code' => $code, 'messages' => ['User has not liked this post before.']]];
            } else {
                $input['id'] = $postLikeExist->id;
                $input['type'] = 'unlike';
                $postUnlike = $this->_repository->update($input);
                if($postUnlike == NULL) {
                    $code = 400;
                    $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while trying to like this post.']]];
                } else {
                    $postRepo = $postRepo = App::make('PostRepository');
                    $post = $postRepo->findById($postUnlike->post_id);
                    $code = 200;
                    $output = ['response' => ['code'=> $code, 'messages' => ['User Unlike the Post.'], 'data' => $post]];
                }
            }
        }

        return response()->json($output, $code);
    }

    public function addPostView(Request $request) {
        
        $input = $request->only('post_id');
        $input['post_id'] = hashid_decode($input['post_id']);
        
        $postRepo = App::make('PostRepository');
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }

        $rules = [
            'post_id'    =>  'required|exists:posts,id,deleted_at,NULL',
            'user_id'    =>  'required|exists:users,id,visibility,1',
         ];

        $validator = Validator::make($input,$rules);

        if($validator->fails()) {

            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You are not able to perform this action you may be removed or disabled by admin' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }

        } else {

            $input['type'] = 'view';
            $checkPostView = PostAction::where("post_id","=",$input['post_id'])->where("user_id","=",$input['user_id'])->where("type","=","view")->first();
            if($checkPostView == NULL){
                $postView = $this->_repository->create($input);
                if($postView == NULL) { 
                    $code = 406;
                    $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while adding record.']]];
                } else {
                    $code = 200;
                    $output = ['response' => ['code'=> $code, 'messages' => ['Post view saved successfully.']]];
                    $postData['total_views'] = PostAction::where("post_id","=",$input['post_id'])->where("type","=","view")->count();
                    $output['response']['data'] = $postData;
                }
            }else{
                $code = 408;
                $output = ['error' => ['code'=>$code, 'messages' => ['Post Already Viewed.']]];
            }
           
            
            
        }

        return response()->json($output, $code);
    }            
}
