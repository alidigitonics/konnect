<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Socialite;
use konnect\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use konnect\Data\Models\UserDetail;
use konnect\Data\Repositories\UserDetailRepository;
use konnect\Data\Models\User;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;

class UserDetailController extends Controller {
    const PER_PAGE = 10;
    private $_repository;

    public function __construct(UserDetailRepository $userDetail) {
        $this->_repository = $userDetail;
    }

    public function addUserDetail(Request $request) {
           
        $input = $request->all();
        // dd($input);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
        }
        $rules = [
                    // 'field_title' => 'required|array',
                    // 'field_value' => 'required|array',
                    'user_id' => 'required|exists:users,id'
                ];
        $validator = Validator::make( $input, $rules);

        if($validator->fails()){
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];

        } else{
            $user = $this->_repository->addUserDetail($input);
            if($user == false){
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while deleting this user.']]];

            } else {
                $code = 200;
                $output = ['response'=>['code'=>$code,'data'=>$user,'messages'=> ['Successfully Added.']]];
            }
        }

        return response()->json($output, $code);
    }

    public function all(Request $request) {
        
        $input = $request->only('pagination','keyword','filter_by_name','filter_by_value');
         $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = ['user_id' => 'required|exists:users,id'];
        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];

        // if validation passes
        } else {
            $pagination = true;
            if($input['pagination']) {
                $pagination = $input['pagination'];
            }
            $users = $this->_repository->findByAll($pagination, self::PER_PAGE, $input,false,true,true);
             $code = 200;
            $users['code'] = $code;
            $output['response'] = $users;
        }
            
        return response()->json($output);
    }  
}
