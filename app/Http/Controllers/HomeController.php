<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;

use konnect\Http\Requests;
use konnect\Http\Controllers\Controller;

use konnect\Data\Models\User;




class HomeController extends Controller
{
    public function __construct(){
      
    }


    public function showUserResetPassword(Request $request) {
        
        $recover_password_key   =   $request->input('recover_password_key');

        $checkRecoverPasswordKey = User::where('recover_password_key','=',$recover_password_key)->count();

        if($checkRecoverPasswordKey == 0){
             return view('emails.user.url-expire');
        }

        return view('user-reset-password',['recover_password_key' => $recover_password_key]);
    }

    public function showAdmin(Request $request){
        return view('admin.main');
    }

    public function showFront(Request $request){
        return view('main');
    }
}
