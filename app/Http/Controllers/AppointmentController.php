<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use konnect\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
// use konnect\Events\SendAdminActivation;
// use konnect\Events\PasswordWasRecovered;
// use konnect\Events\PasswordWasReset;
use konnect\Data\Repositories\AppointmentRepository;
use konnect\Data\Models\Appointment;
use konnect\Data\Models\User;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AppointmentController extends Controller {
    private $_repository;

    public function __construct(AppointmentRepository $appointment) {
        $this->_repository = $appointment;
    }

    public function create(Request $request) {

        $input = $request->only('appointment_name','appointment_date','appointment_email','email_user_type','appointment_longitude','appointment_lattitude','distance','location_address','status');
            $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'       =>  'required|exists:users,id',
            // 'client_id'       =>  'required|exists:users,id',
            'appointment_name'      =>  'required',
            'appointment_email'      =>  'required|email',
            'email_user_type'      =>  'required|in:client,realtor',
            'appointment_longitude'      =>  'required',
            'appointment_lattitude'      =>  'required',
            'distance'      =>  'required',
            'location_address'      =>  'required',
            'appointment_date'         =>  'required'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
                $userAlredyRegistered = User::where('email', '=', $input['appointment_email'])->first(['id']);
                if($userAlredyRegistered == NULL){
                    $message = 'User with this email not exist in the system.';
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                    
                } else {
                    $input['client_id'] = $userAlredyRegistered->id;
                    $data = $this->_repository->create($input);
                    if($data){
                        $code = 200;
                        $output['response']['data'] = $data;
                        $message = 'Appointment added successfully.';
                    }else{
                        $code = 401;
                        $message = 'An error occurred while registration, please try later';
                    }
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                        
                }
        }
        return response()->json($output, $code);
    }

    public function all(Request $request) {
        $token = JWTAuth::getToken();
        if ($token) {
            $claims = JWTAuth::decode($token);

            if ($claims instanceof Payload && $claims->get('sub')) {
                $input = $request->only('keyword');
                $input['user_id'] = $claims->get('sub');
                $input['role_id'] = Role::ADMINISTRATOR;
                $code = 200;
                $output = $this->_repository->findByAll(true, 10, $input,true,true,false);
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
            }

        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function update(Request $request) {
        $input = $request->only('id', 'fullname');
        $input['id'] = hashid_decode( $input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules =[
                    'user_id'  =>  'required|exists:users,id',
                    'id' => 'required|exists:users,id',
                    'fullname' => 'required'
                ];
        $messages = [
                    'id.exists' => 'Incorrect or Invalid Admin id'
        ];
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
            unset($input['user_id']);
            $user = $this->_repository->update($input,true);
            if ($user == NULL) {
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['Service not found']]];
            } else {
                $code = 200;
                $output = ['response' => [
                                    'code' => $code,
                                    'data'=>$user,
                                ]];
            }
        }
        return response()->json($output, $code);
    }

    public function delete(Request $request) {
        $input = $request->only('id');
        $input['id'] = hashid_decode( $input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules =[
                    'user_id'  =>  'required|exists:users,id',
                    'id' => 'required|exists:users,id'
                    
                ];
        $messages = [
                    'id.exists' => 'Incorrect or Invalid Admin id'
        ];
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
            unset($input['user_id']);
            $user = $this->_repository->deleteById($input['id']);
            if ($user == NULL) {
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['Service not found']]];
            } else {
                $code = 200;
                $output = ['response' => ['code' => $code,'messages' => ['Successfully Delete']]];
            }
        }
        return response()->json($output, $code);
    }

    public function login(Request $request) {

        $input = $request->only('email', 'password');

        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6'
        ];

       

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
                $user = $this->_repository->login($input);
                if ($user === 'user_block') {
                    
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Your account has been block.' ] ] ];
                    
                } elseif($user){
                    $userRole = UserRole::whereRaw("user_id =".hashid_decode($user->id)." and (role_id = ".Role::ADMINISTRATOR." or role_id = ".Role::SUPER_ADMIN.")")->first();
                     if($userRole != Null){
                        $user->role_id = $userRole->role_id;
                            $code = 200;
                            $output = [
                                'response' => [
                                    'code' => $code,
                                    'message'=>['You have been logged in successfully'],
                                ]
                            ];
                            $output['response']['data'] = $user;
                    }else{
                        $code = 404;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'You are not allowed to access' ] ] ];
                    }
                }else {
                    $code = 404;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Invalid email or password' ] ] ];
                }
          

        }

        return response()->json($output, $code);
    }
    
}
