<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use konnect\Data\Repositories\CommentRepository;
use konnect\Data\Models\Comment;
use konnect\Data\Models\UserRole;
use konnect\Data\Models\Post;
use konnect\Data\Models\User;
use konnect\Data\Models\Role;
use Tymon\JWTAuth\Payload;
use konnect\Support\Helper;
use Validator, JWTAuth;
use Illuminate\Support\Facades\Cache; 


class CommentController extends Controller
{
    private $_repository;

    public function __construct(CommentRepository $comment) {
        $this->_repository = $comment;
    }

    public function all(Request $request) {
       
        $input = $request->only('keyword','post_id','last_comment_id','pagination','comment_type','comment_id');
        $input['post_id'] = hashid_decode($input['post_id']);
         
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
        $rules = [
                'user_id'   =>  'required|exists:users,id',
                'post_id'   =>  'required|exists:posts,id,deleted_at,NULL',
        ];
        if(isset($input['last_comment_id'])){
            $input['last_comment_id'] = hashid_decode($input['last_comment_id']);
            $rules['last_comment_id']   = 'required|exists:comments,id';
        }

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $pagination = true;
            if(isset($input['pagination'])){
                $pagination = $input['pagination'];
            }
            $response = $this->_repository->findByAll($pagination, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while fetching data.']]];
            }
        }
       
        return response()->json($output, $code);
    }

    public function delete(Request $request){
        
        $input = $request->only('id');
        
        $input['id'] = hashid_decode($input['id']);

        // user token
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }

        $rules = [
                    'id'        => 'required|exists:comments,id,deleted_at,NULL',
                    'user_id'   =>  'required|exists:users,id'
                 ];

        $messages = [
                    'id.exists'  => 'Commnet id does not exists.',
                    'user_id.exists' => 'Incorrect or invalid user id.'
                    ];    
        $validator = Validator::make($input, $rules,$messages);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            
            $postData = \DB::table('comments')
            ->join('posts', 'comments.post_id', '=', 'posts.id')
            ->where('comments.id', '=', $input['id'])
            ->select('comments.user_id as commnetator', 'posts.user_id as post_owner','posts.id as post_id')
            ->first();
            
            $commnetator = 0;
            $postOwner = 0;

            if( isset($postData)){
                if(isset($postData->commnetator)){
                    $commnetator = $postData->commnetator;
                }
                if(isset($postData->post_owner)){
                    $postOwner = $postData->post_owner;
                }
                if(isset($postData->post_id)){
                    $post_id = $postData->post_id;
                }
            }

            // admin role
            $isAdmin = User::where("id","=",$input['user_id'])->first(['user_type']);
            $role_id = '';
            if($isAdmin!=NULL){
                $role_id = $isAdmin->user_type;
            }
            
            if( $commnetator ==  $input['user_id'] || $postOwner == $input['user_id'] || $role_id == 'admin' ){
                    Cache::forget($this->_repository->_cacheTotalCommentKey.$post_id);
                    $response = $this->_repository->deleteById($input['id'],$post_id);
                    if ($response) {
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Comment deleted successfully." ] ] ];
                    } else {
                        // error occured
                        $code = 406;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while deleting comment.' ] ] ];
                    }
            }else{
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to delete this post.' ] ] ];
            }
                
        } 
        return response()->json($output, $code);
    }

    public function update(Request $request){
        
        $input = $request->only('id','comment','report');
        $input = array_filter($input,'strlen');
        $input['id'] = hashid_decode($input['id']);

        // user token
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }

        $rules = [
                    'id'        => 'required|exists:comments,id,deleted_at,NULL',
                    'user_id'   =>  'required|exists:users,id'
                 ];

        $messages = [
                    'id.exists'  => 'Commnet id does not exists.',
                    'user_id.exists' => 'Incorrect or invalid user id.'
                    ];    
        $validator = Validator::make($input, $rules,$messages);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
        
                $response = $this->_repository->update($input);
                if ($response) {
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Comment updated successfully." ] ] ];
                } else {
                    // error occured
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while deleting comment.' ] ] ];
                }
        
                
        } 
        return response()->json($output, $code);
    }
             
}
