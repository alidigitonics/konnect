<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use konnect\Data\Repositories\UserGenreRepository;
use konnect\Data\Models\UserGenre;
use konnect\Data\Models\User;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth, DB;

class UserGenreController extends Controller
{
    private $_repository;

   	public function __construct(UserGenreRepository $userGenres) {
        $this->_repository = $userGenres;
    }

    public function setFavourite(Request $request){
       	
        $input = $request->only('genre_id');
        
        // decoding ids
        $genresArr = [];
        foreach ($input['genre_id'] as $key => $genre) {
        	$genresArr[] = hashid_decode($genre);
        }
        $input['genre_id'] = $genresArr;
        
        $token = JWTAuth::getToken();
           if($token){
               $claims = JWTAuth::decode($token);
               if($claims instanceof Payload && $claims->get('sub')) {
                   $input['user_id'] = $claims->get('sub');        
               } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
           }
        
        $rules = [
            'genre_id'    =>  'required|exists:genres,id|array|min:1',
            'user_id'=> 'required|exists:users,id'
            ];

        $messages = [
                    'genre_id.exists'  => 'Incorrect or invalid genre id.',
                    'genre_id.min'=> 'Please select minimum 1 genre.',
                    'user_id.exists' => 'Incorrect or invalid user id'
                    ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {

        	/*$userGenresCount	= $this->_repository->model->where('user_id', '=', $input['user_id'])->whereIn('genre_id', $input['genre_id'])->count();

        	if( count($input['genre_id']) != $userGenresCount ){*/
				$response = $this->_repository->setFavourite($input);
				if ($response) {
	            	$code = 200;
	            	$output = [ 'response' => [ 'code' => $code, 'messages' => [ "Genres have been marked as favourite successfully." ] ] ];
	            } else {
	                // error occured
	                $code = 401;
	                $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occured while marking genres as favourite.' ] ] ];
	            }
        	/*} else {
        			// genres already exist
	                $code = 401;
	                $output = ['error' => [ 'code' => $code, 'messages' => [ 'Genres are already marked as favourite.' ] ] ];
        	}*/
  
        } 
        
        return response()->json($output, $code);
    }

    public function userGenreList(Request $request){
        $token = JWTAuth::getToken();
           if($token){
               $claims = JWTAuth::decode($token);
               if($claims instanceof Payload && $claims->get('sub')) {
                   $input['user_id'] = $claims->get('sub');        
               } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
           }
        
        $rules = [
            'user_id'    =>  'required|exists:users,id'
            ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
                $response = $this->_repository->userGenreList($input);
                if ($response) {
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'data' => $response ] ];
                } else {
                    // error occured
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while fetching data.' ] ] ];
                }
            
  
        } 
        return response()->json($output, $code);
    }
}
