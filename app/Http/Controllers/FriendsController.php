<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use konnect\Data\Repositories\FriendsRepository;
use konnect\Data\Models\Post;
use konnect\Data\Models\User;
use konnect\Data\Models\UserFriends;
use konnect\Data\Models\Role;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
class FriendsController extends Controller
{
   	private $_repository;

   	public function __construct(FriendsRepository $genre) {
        $this->_repository = $genre;
    }

    public function create(Request $request){
    	$input = $request->only('title');
        
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'    =>  'required|exists:users,id',
            'title'      =>  'required|unique:genres,title,NULL,deleted_at'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
        	$code = 406;
        	$output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $checkUserRole = $this->findRole($input['user_id']);
            if($checkUserRole != NULL){
                 unset($input['user_id']);
                $response = $this->_repository->create($input);
                if ($response) {
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Friends added successfully." ] ] ];
                    // $output['response']['data'] = $addComment;
                } else {
                    // error occured
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while adding genre.' ] ] ];
                }
            }else{
                $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
            }
            	
    	} 
        
        return response()->json($output, $code);
    }

    public function update(Request $request){
        
        $input = $request->only('title', 'id');
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'    =>  'required|exists:users,id',
            'id'         =>  'required|exists:genres',
            'title'      =>  'required|unique:genres,title,'.$input['id'].',id',
        ];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $checkUserRole = $this->findRole($input['user_id']);
            if($checkUserRole != NULL){
                    unset($input['user_id']);
                    $response = $this->_repository->update($input);
                    if ($response) {
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Friends updated successfully." ], 'data' => $response ] ];
                        // $output['response']['data'] = $addComment;
                    } else {
                        // error occured
                        $code = 406;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while updating genre.' ] ] ];
                    }
                }else{
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
                }
                
        } 
        return response()->json($output, $code);
    }

    public function delete(Request $request){
        
        $input = $request->only('id');
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'    =>  'required|exists:users,id',
            'id'         =>  'required|exists:genres'
            ];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $checkUserRole = $this->findRole($input['user_id']);
            if($checkUserRole != NULL){
            
                    $response = $this->_repository->delete($input);
                    if ($response) {
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Friends deleted successfully." ] ] ];
                        // $output['response']['data'] = $addComment;
                    } else {
                        // error occured
                        $code = 406;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while deleting genre.' ] ] ];
                    }
            }else{
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
            }
                
        } 
        return response()->json($output, $code);
    }

    public function setOrder(Request $request){
        // echo hashid_encode(5);
        $input = $request->only('id','order');
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'    =>  'required|exists:users,id',
            'id'         =>  'required|exists:genres',
            'order'      =>  'required|in:up,down'
            ];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $checkUserRole = $this->findRole($input['user_id']);
           if($checkUserRole != NULL){
                    $response = $this->_repository->setOrder($input);
                    if ($response == "last_id") {
                        $code = 409;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Order  not set successfully." ] ] ];
                        // $output['response']['data'] = $addComment;
                    }elseif($response == "success"){
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Order set successfully." ] ] ];
                    } else {
                        // error occured
                        $code = 406;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while set order of genre.' ] ] ];
                    }
            }else{
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
            }
                
        } 
        return response()->json($output, $code);
    }

    public function setVisibility(Request $request){
        
        $input = $request->only('id','visibility');
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'         =>  'required|exists:users,id',
            'id'         =>  'required|exists:genres',
            'visibility'    =>  'required|in:1,0'
            ];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $checkUserRole = $this->findRole($input['user_id']);
            if($checkUserRole != NULL){
                    unset($input['user_id']);
                    $response = $this->_repository->update($input);
                    if ($response) {
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Visibility set successfully." ] ] ];
                        // $output['response']['data'] = $addComment;
                    } else {
                        // error occured
                        $code = 406;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while set visibility of genre.' ] ] ];
                    }
            }else{
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
            }
                
        } 
        return response()->json($output, $code);
    }

    public function all(Request $request){
        
        $input = $request->only('keyword','limit','pagination','page','device');
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = ['user_id'    =>  'required|exists:users,id'];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
                $pagination = true;
                if(isset($input['pagination'])){
                    $pagination = $input['pagination'];
                }
                $limit = 10;
                if(isset($input['limit'])){
                    $limit = $input['limit'];
                }
                $response = $this->_repository->findByAll($pagination,$limit,$input,true);
                if ($response) {
                    $code = 200;
                    $output = [];
                    $output['response'] = $response;
                    $output['response']['code'] = $code;
                } else {
                    // error occured
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while get data of genre.' ] ] ];
                }
                
        } 
        return response()->json($output, $code);
    }

    public function recentList(Request $request){
        
        $input = $request->only('keyword','pagination','page');
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [

                    'user_id'   =>  'required|exists:users,id',
                ];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {

            $pagination = true;
            if(isset($input['pagination'])){
                $pagination = $input['pagination'];
            }
            $limit = 5;
            if(isset($input['limit'])){
                $limit = $input['limit'];
            }
            
            $response = $this->_repository->recentList($pagination,$limit, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 406;
                $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while fetching data.']]];
            }
        } 
        return response()->json($output, $code);
    }

     public function findRole($user_id=0){
        
       return UserRole::whereRaw("user_id =".$user_id." and (role_id = ".Role::ADMINISTRATOR." or role_id = ".Role::SUPER_ADMIN.")")->first();

    }

}
