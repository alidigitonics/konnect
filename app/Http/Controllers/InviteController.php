<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use konnect\Data\Repositories\InviteRepository;
use konnect\Events\InviteWasCreated;
use konnect\Data\Models\Invite;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Hash, \App;


class InviteController extends Controller
{
   	private $_repository;

   	public function __construct(InviteRepository $invite) {
        $this->_repository = $invite;
    }


    public function create(Request $request) {
        
        $input = $request->only('receiver_id','type','receiver');

        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['sender_id'] = $claims->get('sub');        
            }
        }
        
        $rules = [
                    'sender_id'   =>  'required|exists:users,id',
                    'type'        =>  'required|in:facebook,email',
                 ];

        if($input['type'] == 'email') {
             $rules['receiver'] = 'required|email|unique:users,email';
        }else{ 
             $rules['receiver_id'] = 'required';
        }

        $validator = Validator::make($input,$rules);

        if($validator->fails()) {

            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
        }
        else {

            $inviteCreate = $this->_repository->create($input);
            if($inviteCreate == NULL) {

                $code = 406;
                $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while trying to create a invite.']]];

            } else {
                
                $code = 200;
                $output = ['response' => ['code'=> $code, 'messages' => ['Invite will be created successfully.']]];
                if($inviteCreate->type == 'email'){
                    Event::fire(new InviteWasCreated($inviteCreate));
                }
            }
        }

        return response()->json($output, $code);
    }
          
}
