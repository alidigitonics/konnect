<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use konnect\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use konnect\Events\SendAdminActivation;
use konnect\Events\PasswordWasRecovered;
use konnect\Events\PasswordWasReset;
use konnect\Data\Repositories\UserRepository;
use konnect\Data\Models\Role;
use konnect\Data\Models\UserRole;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AdminController extends Controller {
    private $_repository;

    public function __construct(UserRepository $user) {
        $this->_repository = $user;
    }

    public function create(Request $request) {

        $input = $request->only('email', 'fullname');
            $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        //dd($input);
        $rules = [
            'user_id'       =>  'required|exists:users,id',
            'fullname'      =>  'required',
            //'email'         =>  'required|email|unique:users,email'
            'email'         =>  'required|email'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            if($validator->errors()->get('email')){
                $error = ['It seems like an account with this email '.$input['email'].' already exist.'];
                $output = ['error' => [ 'code' => $code, 'messages' => $error ] ];
            } else {
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
            
        } else {
                $userAlredyRegistered = $this->_repository->model->join('user_roles', 'users.id', '=', 'user_roles.user_id')->where('user_roles.role_id', '=', 1)->where('users.email', '=', $input['email'])->first(['user_roles.role_id']);
                if($userAlredyRegistered == NULL){
                    unset($input['user_id']);
                    $input['username'] = "Admin".Str::random(5);
                    $registerUser = $this->_repository->register($input,Role::ADMINISTRATOR);
                    $message = 'A password reset link has been sent to your attendance account. Please check your inbox.';
                    if($registerUser) {
                    // dd($registerUser);
                    $key = Str::random(60);
                    $updatedUser = $this->_repository->update([
                        'id' => hashid_decode($registerUser->id),
                        'recover_attempt_at' => Carbon::now(),
                        'recover_password_key' =>  $key,
                    ]);
                        if($updatedUser == NULL) {
                            $code = 406;
                            $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while trying to reset your password. Please try again.']]];

                        } else {
                            $code = 200;
                            $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                            $output['response']['data'] = $registerUser;
                            Event::fire(new SendAdminActivation($updatedUser));
                        }
                    } else {
                        $code = 401;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
                    }
                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'User alredy registered with this email address' ] ] ];
                }
        }
        return response()->json($output, $code);
    }

    public function all(Request $request) {
        $token = JWTAuth::getToken();
        if ($token) {
            $claims = JWTAuth::decode($token);

            if ($claims instanceof Payload && $claims->get('sub')) {
                $input = $request->only('keyword');
                $input['user_id'] = $claims->get('sub');
                $input['role_id'] = Role::ADMINISTRATOR;
                $code = 200;
                $output = $this->_repository->findByAll(true, 10, $input,true,true,false);
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
            }

        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function update(Request $request) {
        $input = $request->only('id', 'fullname');
        $input['id'] = hashid_decode( $input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules =[
                    'user_id'  =>  'required|exists:users,id',
                    'id' => 'required|exists:users,id',
                    'fullname' => 'required'
                ];
        $messages = [
                    'id.exists' => 'Incorrect or Invalid Admin id'
        ];
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
            unset($input['user_id']);
            $user = $this->_repository->update($input,true);
            if ($user == NULL) {
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['Service not found']]];
            } else {
                $code = 200;
                $output = ['response' => [
                                    'code' => $code,
                                    'data'=>$user,
                                ]];
            }
        }
        return response()->json($output, $code);
    }

    public function delete(Request $request) {
        $input = $request->only('id');
        $input['id'] = hashid_decode( $input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules =[
                    'user_id'  =>  'required|exists:users,id',
                    'id' => 'required|exists:users,id'
                    
                ];
        $messages = [
                    'id.exists' => 'Incorrect or Invalid Admin id'
        ];
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
            unset($input['user_id']);
            $user = $this->_repository->deleteById($input['id']);
            if ($user == NULL) {
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['Service not found']]];
            } else {
                $code = 200;
                $output = ['response' => ['code' => $code,'messages' => ['Successfully Delete']]];
            }
        }
        return response()->json($output, $code);
    }

    public function login(Request $request) {

        $input = $request->only('email', 'password');

        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6'
        ];

       

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
                $user = $this->_repository->login($input);
                if ($user === 'user_block') {
                    
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Your account has been block.' ] ] ];
                    
                } elseif($user){
                    $userRole = UserRole::whereRaw("user_id =".hashid_decode($user->id)." and (role_id = ".Role::ADMINISTRATOR." or role_id = ".Role::SUPER_ADMIN.")")->first();
                     if($userRole != Null){
                        $user->role_id = $userRole->role_id;
                            $code = 200;
                            $output = [
                                'response' => [
                                    'code' => $code,
                                    'message'=>['You have been logged in successfully'],
                                ]
                            ];
                            $output['response']['data'] = $user;
                    }else{
                        $code = 404;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'You are not allowed to access' ] ] ];
                    }
                }else {
                    $code = 404;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Invalid email or password' ] ] ];
                }
          

        }

        return response()->json($output, $code);
    }
    
}
