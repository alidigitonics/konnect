<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use konnect\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use konnect\Data\Repositories\ActivityRepository;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ActivityController extends Controller {
    private $_repository;

    public function __construct(ActivityRepository $activity) {
        $this->_repository = $activity;
    }

    public function activityNotificationList(Request $request) {
        
        $input = $request->only('pagination','limit', 'keyword');
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }

        $rules = ['user_id'    =>  'required|exists:users,id'];
        $messages = [];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {

            $response = $this->_repository->getNotificationList(true, $input['limit'], $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function activityNotificationView(Request $request) {
        
        $input = $request->only('id');
        
        $input['id'] = hashid_decode($input['id']);
        //$input['id'] = hashid_encode($input['id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }

        $rules = [
                    'user_id'    =>  'required|exists:users,id',
                    'id' => 'required|exists:activities,id'];

        $messages = [];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {

            $response = $this->_repository->activityNotificationView($input);
            if($response){
                $code = 200;
                $output = ['response' => ['code' => $code, 'messages' => ['Notifocation has been viewed.']]];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }


    public function activityCountList(Request $request) {
        
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }

        $rules = [
                    'user_id'    =>  'required|exists:users,id',
                    
                ];
        $messages = [];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {

            $response = $this->_repository->activityCountList($input);
           
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }
}
