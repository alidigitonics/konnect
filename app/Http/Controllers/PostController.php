<?php

namespace konnect\Http\Controllers;

use Illuminate\Http\Request;
use konnect\Data\Repositories\PostRepository;
use konnect\Data\Models\Post;
use konnect\Data\Models\PostHashTag;
use konnect\Data\Models\PostFriendTag;
use konnect\Data\Models\User;
use konnect\Data\Models\Role;
use konnect\Data\Models\UserRole;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth, \App;
class PostController extends Controller
{
    private $_repository;

    public function __construct(PostRepository $post) {
        $this->_repository = $post;
        $userRepo = App::make('UserRepository');
    }

    public function create(Request $request) {
        
        $input = $request->only('title','description','genre','privacy','is_commentable','share_with','share_to_all','share_except','hash_tag','friend_tag','post_type','location','post_id','longitude','lattitude');
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $input['content'] = $request->file('content');

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'location'           =>  'required',
            'title'             =>  'sometimes|max:100',
            'description'       =>  'sometimes|max:200',
            'privacy'           =>  'required|in:public,private',
            'is_commentable'    =>  'required|in:1,0',
            
        ];
        if($input['content'] != NULL){
            $rules['content']  =   'sometimes|mimetypes:image/png,image/jpg,image/jpeg,image/gif,video/avi,video/mp4,video/avi';
        }
        
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            if($input['content'] != NULL){
                $mime = $request->file('content')->getMimeType();
                $extension = $request->file('content')->getClientOriginalExtension();
                if($mime == 'image/png' || $mime == 'image/jpg'|| $mime == 'image/jpeg'||$mime == 'image/gif'){
                    $input['type'] = 'photo';
                }else{
                    $input['type'] = 'video';
                }
                $input['file_type'] = $mime;
                $input['original_name'] = $request->file('content')->getClientOriginalName();
            }
            $response = $this->_repository->create($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating post.']]];
                    }
        }

        return response()->json($output, $code);
    }

    public function update(Request $request) {
        
        $input = $request->only('id','title','description','hash_tag','friend_tag','lattitude','longitude','is_report');
        $input = array_filter($input,'strlen');
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $input['content'] = $request->file('content');

        $rules = [
            'user_id'  =>  'required|exists:users,id,visibility,1',
            'id'       =>  'required|exists:posts,id,deleted_at,NULL'        
        ];
        if($input['content'] != NULL){
            $rules['content']  =   'sometimes|mimetypes:image/png,image/jpg,image/jpeg,image/gif,video/avi,video/mp4,video/avi';
        }
        
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            if(isset($input['hash_tag']) && is_array($input['hash_tag'])){
                $postTagDelete = PostHashTag::where("post_id","=",$input['id'])->delete();
            }
            if(isset($input['friend_tag']) && is_array($input['friend_tag'])){
                $postFriendDelete = PostFriendTag::where("post_id","=",$input['id'])->delete();
            }
            if($input['content'] != NULL){
                $mime = $request->file('content')->getMimeType();
                $extension = $request->file('content')->getClientOriginalExtension();
                if($mime == 'image/png' || $mime == 'image/jpg'|| $mime == 'image/jpeg'||$mime == 'image/gif'){
                    $input['type'] = 'photo';
                }else{
                    $input['type'] = 'video';
                }
                $input['file_type'] = $mime;
                $input['original_name'] = $request->file('content')->getClientOriginalName();
            }
            $response = $this->_repository->update($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating post.']]];
                    }
        }

        return response()->json($output, $code);
    }

    public function all(Request $request) {
        
        $input = $request->only('pagination','limit', 'keyword','filter_by_title','filter_by_type','filter_by_status', 'filter_by_user_id', 'filter_by_genre_id','device','last_post_id');
        //dd($input);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }

        $rules = ['user_id'    =>  'required|exists:users,id,visibility,1'];
        $messages = [];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
           if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {

            $response = $this->_repository->findByAll(true, $input['limit'], $input);
            if($response){
                $code = 200;
                $response['code'] =$code;
                $output = ['response' =>  $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function feed(Request $request) {
        $input = $request->only('keyword');
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
        $rules = [
            'user_id'  =>  'required|exists:users,id,visibility,1',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->feed(true, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function newsFeed(Request $request) {
        $input = $request->only('keyword');
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['follower_id'] = $claims->get('sub');        
            }
        }
        $rules = [
            'follower_id'  =>  'required|exists:users,id,visibility,1',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("follower_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->newsFeed(true, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function viewedFeeds(Request $request) {
        $input = $request->only('keyword','last_post_id');
        $input['last_post_id'] = hashid_decode($input['last_post_id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['follower_id'] = $claims->get('sub');        
            }
        }
        $rules = [
            'follower_id'  =>  'required|exists:users,id,visibility,1',
            'last_post_id' =>  'required|exists:post_actions,id',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("follower_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->viewedFeeds(true, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function recentFeeds(Request $request) {
        $input = $request->only('keyword','last_post_id');
        $input['last_post_id'] = hashid_decode($input['last_post_id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['follower_id'] = $claims->get('sub');        
            }
        }
        $rules = [
            'follower_id'  =>  'required|exists:users,id,visibility,1',
            'last_post_id' =>  'required|exists:posts,id,deleted_at,NULL',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("follower_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->recentFeeds(true, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }



    public function view(Request $request) {

        $input = [
            'id' => $request->id,
        ];
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
        $input['id'] = hashid_decode($input['id']);
        
        $rules = [
                    'id'        => 'required|exists:posts',
                    'user_id'   =>  'required|exists:users,id,visibility,1'
                 ];

        $messages = [
                    'id.exists'  => 'Post id does not exists.',
                    'user_id.exists' => 'Incorrect or invalid user id.'
                    ];
        
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {

            $id = $input['id'];
            $postData = $this->_repository->model->first(['privacy', 'user_id']);

            if( $postData->user_id == $input['user_id'] ){
                $postDetails = $this->_repository->findById($id);
            } else {                
                if( $postData->privacy == "private"){
                    $isShared = $this->_repository->model_post_share->where('user_id', '=', $postData->user_id)->where('follower_id', '=', $input['user_id'])->where('post_id', '=', $id)->first();
                    if( $isShared == NULL ){
                        $code = 404;
                        $output = ['error' => ['code' => $code,'messages' => ['Post is not visible to the user']]];
                        return response()->json($output, $code);
                    } else {
                        $postDetails = $this->_repository->findById($id);
                    }
                } else {
                    $postDetails = $this->_repository->findById($id);
                }
            }

            if ($postDetails == NULL) {
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['Post not found']]];
            } else {
                $code = 200;
                $output = $postDetails;
            }
        }
        return response()->json($output, $code);
    }

    public function addComment(Request $request){

        $input = $request->only('post_id', 'comment','comment_type','comment_id');
        
        if( $input['post_id'] != NULL){
            $input['post_id']= hashid_decode($input['post_id']);
        }
        if( $input['comment_id'] != NULL){
            $input['comment_id']= hashid_decode($input['comment_id']);
        }

        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
        
        $rules = [
            'post_id'    =>  'required|exists:posts,id,deleted_at,NULL',
            'user_id'    =>  'required|exists:users,id,visibility,1',
            'comment'    =>  'required',
            'comment_type' => 'required|in:simple,reply'

        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }elseif(array_key_exists("post_id", $validator->messages()->messages())){
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }else{
                $code = 405;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            
            $postExist = Post::where('id', '=', $input['post_id'])->first(['id', 'can_comment']);
            if( $postExist != NULL){
                if($postExist->can_comment == 1){
                    $addComment = $this->_repository->addComment($input);
                    if ($addComment) {
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "comment added successfully." ] ] ];
                        $output['response']['data'] = $addComment;
                        
                    } else {
                        // error occured
                        $code = 400;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while adding comment.' ] ] ];
                    }
                } else {
                    // can not comment
                    $code = 407;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Post is not available for comment.' ] ] ];
                }
            } else {
                // post not exist
                $code = 409;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'Post does not exists.' ] ] ];
            } 
        }
        return response()->json($output, $code);
    }  

    public function findAllById(Request $request) {
        $input = $request->only('user_id');

        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['follower_id'] = $claims->get('sub');        
            }
        }
        if (!isset($input['follower_id'])) {
            $input['follower_id'] = 0;
        }
        $input['user_id'] = hashid_decode($input['user_id']);
        $rules = [
        'user_id'             =>  'required|exists:users,id|not_in:'.$input['follower_id'],
        'follower_id'          =>  'required|exists:users,id,visibility,1|exists:followers,follower_id',
        ];
        $messages =[
            'user_id.not_in' => "User id and follwer id must not be same ."
        ];
        $validator = Validator::make($input, $rules,$messages);
        if ($validator->fails()) {
            if(array_key_exists("follower_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->findAllById(true, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function postSearch(Request $request) {
       
        $input = $request->only('keyword', 'only_favourites','pagination');
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
    
        $rules = [
                'user_id'   =>  'required|exists:users,id,visibility,1',
                'keyword'   =>  'required|string',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $pagination = true;
            if(isset($input['pagination'])){
                $pagination = $input['pagination'];
            }
            $response = $this->_repository->postSearch($pagination, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while fetching data.']]];
            }
        }
       
        return response()->json($output, $code);
    }

    public function postSearchByGenre(Request $request) {
       
        $input = $request->only('keyword','genre_id','visibility','signup_via');
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }
    
        $rules = [
                'user_id'       => 'required|exists:users,id,visibility,1',
                'genre_id'      => 'required|exists:genres,id',
                'visibility'    => 'required|in:1,0',
                'signup_via'    => 'required|in:facebook,email',
                'keyword'       => 'required|string',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->postSearchByGenre(false, 10, $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while fetching data.']]];
            }
        }
       
        return response()->json($output, $code);
    }

    public function userPosts(Request $request) {
        $input = $request->only('user_id','post_type');

        $input['user_id'] = hashid_decode($input['user_id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['id'] = $claims->get('sub');        
            }
        }
        $rules = [
        'id'                  =>  'required|exists:users,id',
        'user_id'             =>  'required|exists:users,id',
        'post_type'             =>  'required|in:simple,memory',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->userPosts(true, 10, $input);
            
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }  

    public function directList(Request $request) {
        
        $input = $request->only('pagination','limit', 'keyword');
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }

        $rules = ['user_id'    =>  'required|exists:users,id,visibility,1'];
        $messages = [];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $response = $this->_repository->directList(true, $input['limit'], $input);
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];
            }
        }
        // all good so return the token
        return response()->json($output, $code);
    }

    public function delete(Request $request){
        
        $input = $request->only('id');
       
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'user_id'    =>  'required|exists:users,id,visibility,1',
            'id'         =>  'required|exists:posts'
            ];

        $validator = Validator::make($input, $rules);


        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $postExist = Post::where("user_id","=",$input['user_id'])->where("id","=",$input['id'])->first();
            $isAdmin = UserRole::where("user_id","=",$input['user_id'])->first(['role_id']);
            $role_id = '';
            if($isAdmin!=NULL){
                $role_id = $isAdmin->role_id;
            }
            //dd($postExist);
            if( ( isset($postExist) && $postExist != NULL )|| $role_id == Role::ADMINISTRATOR || $role_id == Role::SUPER_ADMIN){
            
                    $response = $this->_repository->deleteById($input['id']);
                    if ($response) {
                        \DB::table('activities')
                        ->where('activities.object_id', '=', $input['id'])
                        ->update(['visibility' => 0]);
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ "Post deleted successfully." ] ] ];
                        // $output['response']['data'] = $addComment;
                    } else {
                        // error occured
                        $code = 406;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while deleting post.' ] ] ];
                    }
            }else{
                $code = 409;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
            }
                
        } 
        return response()->json($output, $code);
    }

    public function postShare(Request $request) {

        $input = $request->only('post_id','share_with','share_to_all','share_except');
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                 if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $input['post_id'] = hashid_decode($input['post_id']);
        $rules = [
            'user_id'       =>  'required|exists:users,id,visibility,1',
            'post_id'       =>  'required|exists:posts,id,deleted_at,NULL',
        ];
        if(isset($input['share_with'])){
            $rules['share_with']        =  'required|array';
        }
        if( isset($input['share_to_all']) ){
            $rules['share_to_all']        =  'required|numeric';
        }
        
        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {

            $response = $this->_repository->postShare($input);
            if ($response) {
               
                $code = 200;
                $output = ['response' => ['code' => $code,'messages'=>['Post has been shared.'],'data' => $response,]];
               
                }else{
                    $code = 409;
                    $output = ['error'=>['code'=>$code,'messages'=>['An error occured while share post.']]];
                }
        }

        return response()->json($output, $code);
    }

    public function PostShareList(Request $request){
        $input = $request->only('post_id');
        $input['post_id'] = hashid_decode($input['post_id']);
        $token = JWTAuth::getToken();
           if($token){
               $claims = JWTAuth::decode($token);
               if($claims instanceof Payload && $claims->get('sub')) {
                   $input['user_id'] = $claims->get('sub');        
               } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
           }
        
        $rules = [
            'user_id'    =>  'required|exists:users,id,visibility,1',
            'post_id'    =>  'required|exists:posts,id,deleted_at,NULL',
            ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            // if(array_key_exists("user_id", $validator->messages()->messages())){
            //     $code = 401;
            //     $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            // }else{
            //     $code = 406;
            //     $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            // }
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
                $userRepo = App::make('UserRepository');
                $response = $userRepo->PostShareList(true,10,$input);
                if ($response) {
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'data' => $response ] ];
                } else {
                    // error occured
                    $code = 407;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while fetching data.' ] ] ];
                }
            
  
        } 
        return response()->json($output, $code);
    }
    
    public function userFavoritePosts(Request $request) {
        $input = $request->only('keyword');
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['id'] = $claims->get('sub');        
            }
        }

        $rules = [
            'id'             =>  'required|exists:users,id,visibility,1',
        ];
 
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if(array_key_exists("id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {

            $response = $this->_repository->userFavoritePosts(true, 10, $input);
            
            if($response){
                $code = 200;
                $response['code'] = $code;
                $output = ['response' => $response];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['No results found.']]];
            }
        }

        return response()->json($output, $code);
    }

    public function thumb(Request $request) {
        $input = [];
        $rules = [];

        $validator = Validator::make($input, $rules);
        
        
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
          
            $response = $this->_repository->thumb();
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating post.']]];
                    }
        }

        return response()->json($output, $code);
    }

}
