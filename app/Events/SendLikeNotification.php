<?php
namespace konnect\Events;

use konnect\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use \StdClass;

class SendLikeNotification extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $post_action;
    public $userlikedId;
    public function __construct(StdClass $post_action,$userlikedId)
    {   
        $this->post_action = $post_action;
        $this->userlikedId = $userlikedId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}