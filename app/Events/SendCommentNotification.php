<?php
namespace konnect\Events;

use konnect\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use \StdClass;

class SendCommentNotification extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $post;
    public $userCommentedId;
    public function __construct(StdClass $post,$userCommentedId)
    {   
        $this->post = $post;
        $this->userCommentedId = $userCommentedId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}