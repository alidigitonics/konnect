<?php
namespace konnect\Events;

use konnect\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use \StdClass;

class CommentNotificationWasGenerated extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $post;
    public $userCommented;

    public function __construct(StdClass $post,$userCommented)
    {   
        $this->post = $post;
        $this->userCommented = $userCommented;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}