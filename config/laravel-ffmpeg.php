<?php

return [
    'default_disk' => 'local',

    'ffmpeg.binaries' => env('FFMPEG_PATH','/usr/local/bin/ffmpeg'),
    'ffmpeg.threads'  => 12,

    'ffprobe.binaries' => env('FFPROBE_PATH','/usr/local/bin/ffprobe'),

    'timeout' => 3600,
];
