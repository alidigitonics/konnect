 <?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/clear-cache',function(){
  Cache::flush();
});
Route::get('/hash/{key}',function(Request $request){
  echo Hash::make($request->key);
});
Route::post('/register', ['uses'=>'UserController@register']);
Route::post('/login', ['uses'=>'UserController@login']);
Route::put('/forgot-password',['as'=>'user.forgot-password','uses'=> 'UserController@forgotPassword']);
Route::put('/reset-password',['as'=>'user.reset-password','uses' => 'UserController@resetPassword']);
Route::post('/user/profile/view', ['uses'=>'UserController@view']);
Route::post('/user/profile/upload', ['uses'=>'UserController@upload']);
Route::get('/user/list', ['uses'=>'UserController@all']);
Route::post('/user/visibility', ['uses'=>'UserController@setVisibility']);
Route::post('/user/profile/update', ['uses'=>'UserController@update']);
Route::get('/user/token/refresh', ['uses'=>'UserController@refreshToken']);
Route::post('/device-token/set', ['uses'=>'DeviceController@setUserTokens']);
Route::post('/device-token/remove', ['uses'=>'DeviceController@removeUserTokens']);
Route::post('/add/friend', ['uses'=>'UserController@addFriend']);
Route::post('/update/friend/status', ['uses'=>'UserController@updateFriend']);
Route::get('/friends/list', ['uses'=>'UserController@friendList']);
Route::post('/add/user/detail', ['uses'=>'UserDetailController@addUserDetail']);
Route::get('/user/detail/list', ['uses'=>'UserDetailController@all']);
Route::get('/mutual/connection/list', ['uses'=>'UserController@listMutualFriends']);
Route::get('/mutual/tags/list', ['uses'=>'UserController@listMutualTags']);
Route::post('/user/add/view', ['uses'=>'UserController@addUserView']);
Route::post('/add/dream/collage', ['uses'=>'UserController@upload']);
Route::post('/refer/friend', ['uses'=>'UserController@referFriend']);

//User Search
Route::get('/user/search',['as'=>'user.search','uses'=> 'UserController@userSearch']);

// Admin
Route::post('/admin/create', ['uses'=>'AdminController@create']);
Route::delete('/admin/delete', ['uses'=>'AdminController@delete']);
Route::get('/admin/list', ['uses'=>'AdminController@all']);
Route::put('/admin/update', ['uses'=>'AdminController@update']);
Route::post('/admin/login', ['uses'=>'AdminController@login']);


// User check
Route::get('/user/check', ['uses'=>'UserController@check']);
Route::post('/user/social/connect', ['uses'=>'UserController@connectSocialAccount']);
Route::post('/user/social/disconnect', ['uses'=>'UserController@disconnectSocialAccount']);

Route::post('/logout', ['uses'=>'UserController@logout']);

// dashboard
Route::get('/dashborad', ['uses'=>'DashboardController@dashboard']);

//User Remove
Route::delete('/remove-user',['as'=>'user.remove','uses'=> 'UserController@remove']);

//post image thumb
Route::post('/create/post/thumb',['as'=>'post-thumb','uses'=> 'PostController@thumb']);

// Appointment
Route::post('/appointment/create', ['uses'=>'AppointmentController@create']);

Route::get('/follower/list', ['uses'=>'UserController@followers']);
Route::get('/user/followers/list', ['uses'=>'UserController@userFollowers']);
Route::get('/follow/list', ['uses'=>'UserController@followList']);
Route::post('/user/visibility', ['uses'=>'UserController@setVisibility']);
Route::put('/user/profile/update', ['uses'=>'UserController@update']);
Route::get('/user/token/refresh', ['uses'=>'UserController@refreshToken']);
Route::post('user/follower/request', ['uses'=>'UserController@followerRequest']);

Route::post('/post/create', ['uses'=>'PostController@create']);
Route::post('/post/update', ['uses'=>'PostController@update']);
Route::post('/post/like', ['as'=>'post.like','uses'=>'PostActionController@postLike']);
Route::post('/post/unlike', ['as'=>'post.unlike','uses'=>'PostActionController@postUnlike']);
Route::post('/device-token/set', ['uses'=>'DeviceController@setUserTokens']);
Route::post('device-token/remove', ['uses'=>'DeviceController@removeUserTokens']);
Route::get('/post/view', ['uses'=>'PostController@view']);
Route::post('/add-comment', ['uses'=>'PostController@addComment']);
Route::get('user/post/list', ['uses'=>'PostController@findAllById']);
Route::get('/post/list', ['uses'=>'PostController@all']);
Route::get('/post/feed', ['uses'=>'PostController@feed']);
Route::get('/post/newsfeed', ['as'=>'post.news-feed','uses'=>'PostController@newsFeed']);
Route::get('/post/view/feed', ['as'=>'post.viewed-feed','uses'=>'PostController@viewedFeeds']);
Route::get('/post/recent/feed', ['as'=>'post.recent-feed','uses'=>'PostController@recentFeeds']);
Route::post('/post/add-view', ['as'=>'post.view','uses'=>'PostActionController@addPostView']);
Route::get('user/posts', ['uses'=>'PostController@userPosts']);
Route::post('/post/follower/list', ['uses'=>'PostController@PostFollowerList']);
Route::get('user/favorite/posts', ['uses'=>'PostController@userFavoritePosts']);
Route::get('/post/share/list', ['uses'=>'PostController@PostShareList']);



// activity
Route::get('/activity/notification/list', ['as'=>'activity.notification.list','uses'=>'ActivityController@activityNotificationList']);
Route::post('/activity/notification/view', ['as'=>'activity.notification.view','uses'=>'ActivityController@activityNotificationView']);
Route::get('/activity/count/list', ['as'=>'activity.count.list','uses'=>'ActivityController@activityCountList']);

//Comments
Route::get('/comment/list',['as'=>'comment.list', 'uses'=>'CommentController@all']);
Route::put('/comment/update',['as'=>'comment.list', 'uses'=>'CommentController@update']);
Route::delete('/comment/delete',['as'=>'comment.delete', 'uses'=>'CommentController@delete']);
