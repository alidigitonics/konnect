<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses'=>'HomeController@showFront']);	
Route::get('/upload', ['uses'=>'HomeController@showFront']);	

Route::get('/user-reset-password', ['as'=>'reset-password','uses'=>'HomeController@showAdmin']);
Route::group(['prefix' => 'admin'],function(){
	
	Route::get('/user-reset-password', ['as'=>'admin-reset-password','uses'=>'HomeController@showAdmin']);
	Route::get('/', ['uses'=>'HomeController@showAdmin']);	
	Route::get('/dashboard', ['uses'=>'HomeController@showAdmin']);	
	Route::get('/users', ['uses'=>'HomeController@showAdmin']);	
	Route::get('/genre', ['uses'=>'HomeController@showAdmin']);	
	Route::get('/manage-admin', ['uses'=>'HomeController@showAdmin']);	
	Route::get('/content', ['uses'=>'HomeController@showAdmin']);	
	Route::get('/forget-password', ['uses'=>'HomeController@showAdmin']);
	Route::get('/oops', ['uses'=>'HomeController@showAdmin']);	
});
Route::post('/logout', ['uses'=>'UserController@logout']);
